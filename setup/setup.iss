#define MyAppName "NetHelp"
#define MyAppVersion "1.2"
#define MyAppPublisher "NetHelp"
#define MyAppURL "http://sourceforge.net/projects/nethelp/"

[Setup]
AppId={{5160BD37-3103-4F66-A55F-49EA085F9DC9}
AppName={#MyAppName}              
AppVerName={#MyAppName}         
AppVersion={#MyAppVersion}

AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}       
AppCopyright=Copyright � 2011 {#MyAppPublisher}

DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
LicenseFile=misc\license.txt
OutputBaseFilename=NetHelpSetup
SetupIconFile=misc\setup_icon.ico
Compression=lzma2/ultra64
SolidCompression=yes

WizardImageBackColor=clWhite
WizardImageFile="misc\setup_left_logo.bmp"
WizardSmallImageFile="misc\setup_mini_logo.bmp"

[Messages]
BeveledLabel={#MyAppName} - v {#MyAppVersion}

[Languages]
Name: "english";  MessagesFile: "compiler:Default.isl"
Name: "french";   MessagesFile: "compiler:Languages\French.isl"

[Components]
Name: Agent;   Description: {cm:Agent};    Types: full; Flags: restart;
Name: Server;  Description: {cm:Server};   Types: full;
Name: Viewer;  Description: {cm:Viewer};   Types: full;

[Files]
; client specific files
Source: "src\client.exe";               DestDir: "{app}"; Flags: ignoreversion; Components:Agent 
Source: "src\tvnserver.exe";            DestDir: "{app}"; Flags: ignoreversion; Components:Agent 
; server specific files 
Source: "src\server.exe";               DestDir: "{app}"; Flags: ignoreversion; Components:Server
; Viewer specific files 
Source: "src\viewer.exe";               DestDir: "{app}"; Flags: ignoreversion; Components:Viewer
; Common files                                                                                            
Source: "src\QtCore4.dll";              DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Server or Agent
Source: "src\QtNetwork4.dll";           DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Server or Agent    
Source: "src\QtGui4.dll";               DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Agent
Source: "src\QtXml4.dll";               DestDir: "{app}"; Flags: ignoreversion; Components:Viewer
Source: "src\imageformats\qjpeg4.dll";  DestDir: "{app}\imageformats"; Flags: ignoreversion; Components:Viewer or Agent
Source: "src\libgcc_s_dw2-1.dll";       DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Server or Agent
Source: "src\mingwm10.dll";             DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Server or Agent    
Source: "src\libeay32.dll";             DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Agent
Source: "src\ssleay32.dll";             DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Agent
Source: "src\libopencv_core231.dll";    DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Agent
Source: "src\libopencv_highgui231.dll"; DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Agent
Source: "src\libstdc++-6.dll";          DestDir: "{app}"; Flags: ignoreversion; Components:Viewer or Agent
;misc files                                                                                    
Source: "misc\utils\srvany.exe";        DestDir: "{app}\misc"; Flags: ignoreversion; Components:Server or Agent
Source: "misc\utils\instsrv.exe";       DestDir: "{app}\misc"; Flags: ignoreversion; Components:Server or Agent
Source: "misc\uninstall.ico";           DestDir: "{app}\misc"; Flags: ignoreversion; Components:Server or Agent  
; temporary files
Source: "misc\utils\AgentOnDesktop.reg"; DestDir: "{app}\misc"; Flags: deleteafterinstall; Components:Agent

[Icons]
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}";  Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"; IconFilename: "{app}\misc\uninstall.ico"
Name: "{group}\{cm:ShortcutViewer}";                Filename: "{app}\viewer.exe"; WorkingDir: "{app}"

[Run]
Filename:"{app}\misc\instsrv.exe"; Parameters:"""NetHelp Agent"" ""{app}\misc\srvany.exe""";  StatusMsg:"Install Agent service...";  Flags:runhidden; Components:Agent
; Set the key after the service is intalled because it would overwrite it
Filename:"{win}\regedit.exe";      Parameters:"/s ""{app}\misc\AgentOnDesktop.reg""";         StatusMsg:"Install Agent service...";  Flags:runhidden; Components:Agent

Filename:"{app}\misc\instsrv.exe"; Parameters:"""NetHelp Server"" ""{app}\misc\srvany.exe"""; StatusMsg:"Install Server service..."; Flags:runhidden; Components:Server 

[UninstallRun]                                           
Filename: "{sys}\net"; Parameters:"stop ""NetHelp Agent""";     StatusMsg:"Stopping Agent service..." ;   Flags: runhidden waituntilterminated;
Filename: "{sys}\net"; Parameters:"stop ""NetHelp Server""";    StatusMsg:"Stopping Server service..." ;  Flags: runhidden waituntilterminated;

Filename:"{app}\misc\instsrv.exe"; Parameters:"""NetHelp Agent"" REMOVE";  StatusMsg:"Uninstall Agent service..."; Flags:runhidden; Components:Agent
Filename:"{app}\misc\instsrv.exe"; Parameters:"""NetHelp Server"" REMOVE"; StatusMsg:"Uninstall Server service..."; Flags:runhidden; Components:Server

[Registry]
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Agent";            Components:Agent; Flags:uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Agent";            ValueType:string; ValueName:"Description";      ValueData:"The NetHelp agent allows a remote viewer to take the control of your desktop when you activate it"; Components:Agent
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Agent\Parameters"; ValueType:string; ValueName:"Application";      ValueData:"{app}\client.exe"; Components:Agent
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Agent\Parameters"; ValueType:string; ValueName:"AppDirectory";     ValueData:"{app}";            Components:Agent

Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Server";           Components:Server; Flags:uninsdeletekey
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Server";            ValueType:string; ValueName:"Description";     ValueData:"The NetHelp server is the central communication point the agent and the viewer connect to."; Components:Server
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Server\Parameters"; ValueType:string; ValueName:"Application";     ValueData:"{app}\server.exe"; Components:Server
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Services\NetHelp Server\Parameters"; ValueType:string; ValueName:"AppDirectory";    ValueData:"{app}";            Components:Server  

[CustomMessages]
;Agent description
english.Agent=The NetHelp Agent allows your computer to be controlled by a remote technician
french.Agent=L'Agent NetHelp permet � un technicien distant de contr�ler votre ordinateur

;Server description
english.Server=The NetHelp server is the central communication point the agent and the viewer connect to. Install it on an Internet-connected computer to provide the remote controle service to your customers.
french.Server=Le serveur NetHelp est le point central de communication pour les agents et les visualiseurs. Intallez-le sur un ordinateur connect� � Internet pour fournir le servie de t�l�maintenance � vos clients.

;Viewer description
english.Viewer=The NetHelp Viewer allows a remote technician to control the computer of a customer.
french.Viewer=Le visualiseur NetHelp permet � un technicien distant de contr�ler l'ordinateur d'un client connect�.

;Shortcut Viewer
english.ShortcutViewer=NetHelp Viewer
french.ShortcutViewer=Visualiseur NetHelp