# -*- coding: utf-8 -*-

import os
import shutil


vtProjectFiles = [
  ('server', 'release', 'server.exe'),
  ('viewer', 'release', 'viewer.exe'),
  ('client', 'release', 'client.exe'),

  ('libs', 'OpenCV', 'bin', 'libopencv_core231.dll'),
  ('libs', 'OpenCV', 'bin', 'libopencv_highgui231.dll'),
  ('libs', 'OpenCV', 'bin', 'libstdc++-6.dll'),

  ('libs', 'QtCore4.dll'),
  ('libs', 'QtNetwork4.dll'),
  ('libs', 'QtGui4.dll'),
  ('libs', 'QtXml4.dll'),
  ('libs', 'imageformats', 'qjpeg4.dll'),
  ('libs', 'libgcc_s_dw2-1.dll'),
  ('libs', 'ssleay32.dll'),
  ('libs', 'libeay32.dll'),
  ('libs', 'mingwm10.dll'),

  ('libs', 'TightVNC', 'tvnserver.exe')
  ]


def importFile(tFilePath):
  sFilePath = os.sep.join(tFilePath)
  sDst = os.path.join('src', tFilePath[-1])

  try:
    shutil.copyfile(sFilePath, sDst)
  except Exception, oEx:
    print 'Error whe copying file %s : %s' % (sFilePath, str(oEx))

if __name__ == '__main__':

  for tProjectFile in vtProjectFiles:
    importFile(tuple(['..'] + list(tProjectFile)))

