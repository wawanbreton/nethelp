#pragma once

#include <QString>

#include "common/argslist.h"
#include "core/httptunnelcommand.h"

typedef struct
{
    HttpTunnelCommand::Enum command{HttpTunnelCommand::None};
    ArgsList arguments;
    bool expectsData{false};
} RequestDesc;
