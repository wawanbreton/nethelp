/*! @file   nethelpclient.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "tcppseudoserver.h"

#include <QTcpSocket>
#include <QDateTime>


TcpPseudoServer::TcpPseudoServer(quint16 actualPort,
                                 quint16 replacementPort,
                                 QObject *parent) :
    QObject(parent),
    _server(new QTcpServer(this)),
    _actualPort(actualPort),
    _replacementPort(replacementPort)
{
    connect(_server, &QTcpServer::newConnection, this, &TcpPseudoServer::onNewConnection);

    if(_server->listen(QHostAddress::LocalHost, _replacementPort))
    {
        qInfo() << "Pseudo-server listening on port" << _actualPort << _replacementPort;
    }
    else
    {
        qWarning() << "Unable to start TCP server on port" << _actualPort
                   << ":" << _server->errorString();
    }
}

void TcpPseudoServer::onNewConnection()
{
    while(_server->hasPendingConnections())
    {
        QTcpSocket *socket = _server->nextPendingConnection();
        connect(socket, &QTcpSocket::readyRead,    this, &TcpPseudoServer::onSocketData);
        connect(socket, &QTcpSocket::disconnected, this, &TcpPseudoServer::onSocketDisconnected);

        int index = _socketIndex++;
        socket->setProperty("index", index);
        _sockets.insert(index, socket);

        emit socketConnected(_actualPort, index);

        qInfo() << "Client connected on port" << _replacementPort << _actualPort << index;
    }
}

void TcpPseudoServer::onSocketData()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if(socket)
    {
        emit clientData(socket->readAll(), _actualPort, socket->property("index").toInt());
    }
    else
    {
        qCritical() << "Sender is not a QTcpSocket" << sender();
    }
}

void TcpPseudoServer::onSocketDisconnected()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if(socket)
    {
        int index = socket->property("index").toInt();
        auto iterator = _sockets.find(index);
        if(iterator != _sockets.end())
        {
            qInfo() << "Client disconnected from port"
                    << _replacementPort << _actualPort << iterator.key();

            _sockets.erase(iterator);
            socket->deleteLater();

            emit socketDisconnected(_actualPort, index);
        }
        else
        {
            qCritical() << "Disconnected socket is not registered ?!" << index;
        }
    }
    else
    {
        qCritical() << "Sender is not a QTcpSocket" << sender();
    }
}

void TcpPseudoServer::onServerData(const QByteArray &data, int socketIndex)
{
    auto iterator = _sockets.find(socketIndex);
    if(iterator != _sockets.end())
    {
        iterator.value()->write(data);
        iterator.value()->flush();
    }
    else
    {
        qWarning() << "Received server data for unregistered socket" << socketIndex;
    }
}
