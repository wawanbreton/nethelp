/*! @file   nethelpclient.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include "core/nethelpendpoint.h"

#include <QNetworkProxy>
#include <QStringList>
#include <QPointer>

#include "common/categorizedfiles.h"
#include "common/clientendpointparams.h"
#include "common/fileinfo.h"
#include "common/stringboolpair.h"

class HttpTunnelClient;
class TcpPseudoServer;
class Path;

/*! @brief Facade class which contains all the methods/signals that should be needed in a
           NetHelp viewer application */
class NetHelpEndPointViewer : public NetHelpEndPoint
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit NetHelpEndPointViewer(const QString &serverUrl,
                                       const QNetworkProxy &proxy,
                                       const QList<QSslError::SslError> &ignoredSslErrors,
                                       QObject *parent = nullptr);


    public slots:
        /*! @brief Sends an order to take the control of the given client
            @param client The identifier of the client to take the control of
            @param password The session unique password */
        void takeControl(const QString &client,
                         const QString &password,
                         const ClientEndPointParams &params);

        /*! @brief Sends an order to release the control of the currently controlled client */
        void releaseControl();

        /*! @brief Sends an order to the client to list the given directory
            @param dirName The absolute path of the directory to be listed
            @note This function returns nothing, but a directoryListed signal will be emitted by the
                  inner object when the informations becomes available
            @sa NetHelpClient::registerDirectoryListListener */
        void listDirectory(const Path &dirName);

        void requestClientParams(const QString &clientId);

        /*! @brief Sends an order to the client to download the given file
            @param filePath The absolute path of the file to be downloaded
            @note This functions returns nothing, but the fileDownloaded signal will be emitted each
                  time new data of the file is available */
        void downloadFile(const Path &filePath);

        /*! @brief Sends an order to request a picture from the remote client webcam
            @note This function returns nothing, the picture will be sent back with the emission
                  of a picture signal when available */
        void requestPicture();

    signals:
        /*! @brief Signal emitted each time the server signals that the
                   clients list has changed
            @param clients The clients list , containing the name of the client and a bool
                           which is true in case the client is currently under control */
        void clientsList(const QList<StringBoolPair> &clients);

        void clientParams(const QString &clientId, const ClientEndPointParams &params);

        /*! @brief Signal emitted when a viewer has tried to take the control of a client
         *  @param success Indicates whether the client remote control was taken, or if an error
         *                 occured (obviously a wrong password) */
        void takeControlResult(bool success);

        void fileData(const Path &filePath, const QByteArray &data, int size);

        /*! @brief Signal emitted when a remote picture has been downloaded
            @param image The downloaded image
            @sa NetHelpClient::requestPicture */
        void picture(const QImage &image);

        void directoryListed(const Path &path, const QList<FileInfo> &files);

    protected:
        /*! @brief Slot called when the inner object indicates that a remote client has approved
         *         (or denied) the take control request */
        void onTakeControlResult(bool success);

    private:
        void onImageData(const QByteArray &data, int size);

        void onServerData(QByteArray data, quint16 port, int socketIndex);

        void onClientData(const QByteArray &data, quint16 port);

        void clearPseudoServers();

    private:
        QList<TcpPseudoServer *> _pseudoServers;
        ClientEndPointParams _controlledClientParams;
        QByteArray _pictureData;
};
