/*! @file   webcamgrabber.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "webcamgrabber.h"

#include <opencv2/imgcodecs/imgcodecs.hpp>


WebcamGrabber::WebcamGrabber(QObject *parent) :
    QThread(parent),
    _capture(nullptr),
    _mutex(),
    _mutex2(),
    _frameBuf()
{
}

WebcamGrabber::~WebcamGrabber()
{
    if(isRunning())
    {
        // Take the lock on the first mutex so that we get the priority when the second one is released
        _mutex2.lock();

        _mutex.lock();
        _capture->release();
        _mutex.unlock();

        _mutex2.unlock();

        wait(5000);
    }
}

void WebcamGrabber::grabImage()
{
    if(isRunning())
    {
        std::vector<int> params;
        params.push_back(cv::IMWRITE_JPEG_QUALITY);
        params.push_back(50);

        // Take the lock on the first mutex so that we get the priority when the second one is released
        _mutex2.lock();

        _mutex.lock();
        _capture->retrieve(_rawFrame);
        _mutex.unlock();

        _mutex2.unlock();

        cv::imencode(".jpg", _rawFrame, _frameBuf, params);

        emit imageGrabbed(QByteArray::fromRawData((const char *)(&_frameBuf.front()),
                                                  _frameBuf.size()));
    }
}

void WebcamGrabber::run()
{
    _mutex.lock();
    _capture = new cv::VideoCapture(0);
    bool running = _capture->isOpened();
    _mutex.unlock();

    while(running)
    {
        // Just block on this mutex to ensure that a waiting gramImage gets the priority
        _mutex2.lock();
        _mutex2.unlock();

        _mutex.lock();
        _capture->grab();
        running = _capture->isOpened();
        _mutex.unlock();
    }

    _mutex.lock();
    delete _capture;
    _capture = nullptr;
    _mutex.unlock();
}
