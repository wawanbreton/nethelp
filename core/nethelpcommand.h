#pragma once

#include "common/argslist.h"
#include "core/httptunnelcommand.h"

typedef struct
{
    HttpTunnelCommand::Enum command{HttpTunnelCommand::None};
    ArgsList arguments{};
    QByteArray data{};
} NetHelpCommand;
