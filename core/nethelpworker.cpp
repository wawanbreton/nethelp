#include "nethelpworker.h"

#include <QMetaObject>
#include <QThread>

#include "core/nethelpendpoint.h"


NetHelpWorker::NetHelpWorker(NetHelpEndPoint *endPoint, QObject *parent) :
    QObject(parent),
    _endPoint(endPoint)
{
    QThread *thread = new QThread(this);

    _endPoint->moveToThread(thread);

    connect(_endPoint, &NetHelpEndPoint::openSessionEnd,   this, &NetHelpWorker::onOpenSessionEnd);
    connect(_endPoint, &NetHelpEndPoint::connectionLost,   this, &NetHelpWorker::connectionLost);
    connect(_endPoint, &NetHelpEndPoint::controlTaken,     this, &NetHelpWorker::onTakeControl);
    connect(_endPoint, &NetHelpEndPoint::controlReleased,  this, &NetHelpWorker::onReleaseControl);

    connect(_endPoint, &NetHelpEndPoint::over,      _endPoint, &NetHelpEndPoint::deleteLater);
    connect(_endPoint, &NetHelpEndPoint::destroyed, thread,    &QThread::quit);
    connect(thread,    &QThread::finished,          thread,    &QThread::deleteLater);
    connect(thread,    &QThread::destroyed,         this,      &NetHelpWorker::over);

    thread->start();
}

void NetHelpWorker::startSession()
{
    QMetaObject::invokeMethod(_endPoint, "startSession");
}

void NetHelpWorker::stopSession()
{
    QMetaObject::invokeMethod(_endPoint, "stopSession");
}

void NetHelpWorker::onOpenSessionEnd(const QString &identifier,
                                     const QString &password,
                                     const QString &errorMsg)
{
    if(errorMsg.isEmpty())
    {
        _sessionId = identifier;
        _sessionPassword = password;
    }

    emit openSessionEnd(identifier, password, errorMsg);
}

void NetHelpWorker::onTakeControl()
{
    _remoteControlTaken = true;

    emit controlTaken();
}

void NetHelpWorker::onReleaseControl()
{
    _remoteControlTaken = false;

    emit controlReleased();
}
