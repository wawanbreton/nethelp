/*! @file   vncpseudoserver.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QObject>

#include <QTcpServer>

/*! @brief This class is a simple proxy TCP server which creates a local socket for an external
           application to connect to (VNC viewer, ssh, ...) */
class TcpPseudoServer : public QObject
{
    Q_OBJECT

    public:
        explicit TcpPseudoServer(quint16 actualPort,
                                 quint16 replacementPort,
                                 QObject *parent = nullptr);

        void onServerData(const QByteArray &data, int socketIndex);

        quint16 getActualPort() const { return _actualPort; }

    signals:
        void socketConnected(quint16 port, int index);

        /*! @brief Signal emitted when the connected client sends some data
            @param data The data sent by the client
            @param port The original TCP port
            @param socketIndex The index of the socket from which data has been received */
        void clientData(const QByteArray &data, quint16 port, int socketIndex);

        void socketDisconnected(quint16 port, int index);

    private:
        /*! @brief Slot called when a new clients connects to the socket */
        void onNewConnection();

        /*! @brief Slot called when some data is available on the client socket */
        void onSocketData();

        void onSocketDisconnected();

    private:
        QTcpServer *_server{nullptr};
        const quint16 _actualPort;
        const quint16 _replacementPort;
        QHash<int, QTcpSocket *> _sockets;
        int _socketIndex{0};
};
