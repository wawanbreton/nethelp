#pragma once

#include "core/abstractcommandreader.h"

#include <QNetworkReply>

class AnswerReader : public AbstractCommandReader
{
    Q_OBJECT

    public:
        explicit AnswerReader(HttpTunnelCommand::Enum sentCommand,
                              int timeout,
                              QObject *parent = nullptr);

        void setReply(QNetworkReply *reply);

        HttpTunnelCommand::Enum getSentCommand() const { return _sentCommand; }

    signals:
        void error(QNetworkReply::NetworkError error, int httpStatusError, bool timeout);

    protected:
        virtual bool parseHeader(QByteArray &buffer,
                                 HttpTunnelCommand::Enum &command,
                                 ArgsList &arguments,
                                 int &contentLength) override;

        virtual void onTimeout() override;

    private:
        void onReplyError(QNetworkReply::NetworkError networkError);

        void onMetaDataChanged();

    private:
        const HttpTunnelCommand::Enum _sentCommand{HttpTunnelCommand::None};
        QNetworkReply *_reply{nullptr};
};
