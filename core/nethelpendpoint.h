/*! @file   nethelpclient.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QObject>

#include <QNetworkProxy>
#include <QSslError>
#include <QStringList>
#include <QPointer>

#include "common/categorizedfiles.h"
#include "common/clientendpointparams.h"
#include "common/stringboolpair.h"


class HttpTunnelClient;
class TcpPseudoServer;
class Path;

/*! @brief Facade class which contains all the methods/signals that should be needed in a
           NetHelp client or viewer application */
class NetHelpEndPoint : public QObject
{
    Q_OBJECT

    public:
        /*! @brief Destructor, which destroys the inner resources */
        virtual ~NetHelpEndPoint();

    public slots:
        /*! @brief Starts the communication session with the server
            @note The function returns no result, but the openSessionEnd signal will be emitted
                  when the connection has been established or if an error occured */
        void startSession();

        /*! @brief Stops the communication session with the server
         *  @note Once this method has been called, wait for the over() signal before destroying
         *        the object so that the communication has time to close properly */
        void stopSession();

    signals:
        /*! @brief Signal emitted when the session opening sequence is over
            @param identifier The identifier affected by the server,
                              or an empty string if an error occured
            @param password The password to be sent to the server to start the remote control
            @param errorMsg The error message if an error occured, or an empty string */
        void openSessionEnd(const QString &identifier,
                            const QString &password,
                            const QString &errorMsg);

        /*! @brief Signal emitted when the connection to the server has been lost
                           without being asked */
        void connectionLost();

        /*! @brief Signal emitted when a viewer has taken the control of the
         *         application (client-side) or when a remote application has been
         *         taken control of (viewer-side) */
        void controlTaken();

        /*! @brief Signal emitted when the viewer has released the control of
                   the application */
        void controlReleased();

        /*! @brief Signal emitted after stopSession() has been called, once all the communication
         *         has been properly shut down */
        void over();

    protected:
        /*! @brief Constructor
            @param type The type of application, which should be "client" or "viewer"
            @param parent The parent container */
        explicit NetHelpEndPoint(const QString &type,
                                 const QString &serverUrl,
                                 const QNetworkProxy &proxy,
                                 const QString &desiredIdentifier,
                                 const QString &desiredPassword,
                                 const QList<QSslError::SslError> &ignoredSslErrors,
                                 const ClientEndPointParams &params,
                                 QObject *parent = nullptr);

        /*! @brief Gets the actual HTTP inner tunnel used for communication */
        HttpTunnelClient *accessClient() { return _client; }

    private:
        /*! @brief Slot called when the inner object indicats that the session opening sequency
                   is over */
        void onOpenSessionEnd(const QString &error);

    private:
        HttpTunnelClient *_client{nullptr};
};
