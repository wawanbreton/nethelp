/*! @file   httptunnelclient.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QIODevice>

#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QNetworkReply>
#include <QUuid>

#include "common/clientendpointparams.h"
#include "common/fileinfo.h"
#include "common/stringboolpair.h"
#include "core/abstracthttptunnel.h"
#include "core/requestdesc.h"

class AnswerReader;

/*! @brief Transparent tunnel which implements a bidirectional stream using only standard HTTP
           requests, inspired from the BOSH protocol */
class HttpTunnelClient : public AbstractHttpTunnel
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param type The type of client to be sent to the server
            @param server The server base URL
            @param proxy The proxy to be used to connect to the Internet
            @param parent The parent container */
        explicit HttpTunnelClient(const QString &type,
                                  const QString &url,
                                  const QNetworkProxy &proxy,
                                  const ClientEndPointParams &params,
                                  const QString &desiredIdentifier,
                                  const QString &desiredPassword = QString(),
                                  const QList<QSslError::SslError> &ignoredSslErrors = QList<QSslError::SslError>(),
                                  QObject *parent = nullptr);

        /*! @brief Gets the server-affected unique identifier
            @return The server-affected unique identifier */
        const QString &getIdentifier() const
        { return _id; }

        /*! @brief Gets the password to be given to start the remote control session
            @return The password to be given to start the remote control session */
        const QString &getPassword() const
        { return _password; }

        /*! @brief Send the first hello message to the server */
        void tryConnect();

        bool isConnected() const { return !_id.isEmpty(); }

        /*! @brief Send the last goodbye message to the server
            @note Please note that the message is sent asychronously, so the object must remain
                  alive for some time after calling this method */
        void disconnect();

        void sendAskClientData(const QString &clientId);

        /*! @brief Sends a take control command
            @param clientId The client to take the control of
            @param password The remote control session password */
        void sendTakeControl(const QString &clientId, const QString &password);

        /*! @brief Sends a list directory command
            @param dirName The absolute path of the directory
            @param infos The list of files informations from the directory */
        void sendListDirectory(const Path &dirName,
                               const QList<FileInfo> &infos=QList<FileInfo>());

        void sendReleaseControl() { sendCommand({HttpTunnelCommand::ReleaseControl}); }

        void sendDownloadFile(const Path &filePath);

        void sendFileData(const Path &filePath, const QByteArray &data, int totalSize);

        void sendAskPicture() { sendCommand({HttpTunnelCommand::GetPicture}); }

        void sendPicture(const QByteArray &data);

        void sendTcpConnected(quint16 port, int socketIndex);

        void sendTcpData(const QByteArray &data, quint16 port, int socketIndex);

        void sendTcpDisconnected(quint16 port, int socketIndex);

        void sendPleaseReleaseControl(const QString user, const QString &clientId);

    signals:
        void openSessionEnd(const QString &error);

        /*! @brief Signal emitted when the connection to the server has been lost
                   without being asked */
        void connectionLost();

        /*! @brief Signal emitted after the goodbye command has been sent and answered */
        void over();

        /*! @brief Signal emitted when a clients list command is received
            @param clients The clients list , containing the name of the client and a bool
                           which is true in case the client is currently under control */
        void clientsList(const QList<StringBoolPair> &clients);

        void clientParams(const QString &clientId, const ClientEndPointParams &params);

        /*! @brief Signal emitted when a viewer has tried to take the control of a client
         *  @param success Indicates whether the client remote control was taken, or if an error
         *                 occured (obviously a wrong password) */
        void takeControlResult(bool success);

        void controlTaken();

        /*! @brief Signal emitted when a control released command is received */
        void controlReleased();

        /*! @brief Signal emitted when a list directory command is received
            @param dirPath The absolute path of the directory
            @param infos The list of files informations from the directory */
        void directoryListed(const Path &dirPath, const QList<FileInfo> &infos);

        void fileData(const Path &filePath, const QByteArray &data, int size);

        /*! @brief Signal emitted when a download file command is received
            @param filePath The path of the file to be downloaded */
        void downloadFile(const Path &filePath);

        void pictureAsked();

        void pictureData(const QByteArray &data, int size);

        void tcpConnected(quint16 port, int socketIndex);

        void tcpData(QByteArray data, quint16 port, int socketIndex);

        void tcpDisconnected(quint16 port, int socketIndex);

        void pleaseReleaseControl(const QString &user, const QString &clientId);

    protected:
        virtual bool sendCommandImpl(const NetHelpCommand &command) override;

    private:
        void onSslErrors(QNetworkReply *reply, const QList<QSslError> & errors);

        /*! @brief This method starts listening for command by sending an
                   asynchronously blocking HTTP request */
        void listenForCommand();

        /*! @brief Method called when a fatal error occurs on an HTTP request */
        void fatalRequestError();

        void onAnswerHeaderParsed(const RequestDesc &desc);

        void onAnswerError(QNetworkReply::NetworkError networkError,
                           int httpStatusCode,
                           bool timeout);

        void onClientsListData(const QByteArray &data);

        void onClientParamsData(const QByteArray &data);

        void onDirectoryListData(const QByteArray &data);

        void onFileData(const QByteArray &data);

        void onPictureData(const QByteArray &data);

        void onTcpData(const QByteArray &data);

        void onListenHeaderParsed(const RequestDesc &desc);

        void onListenError(QNetworkReply::NetworkError networkError,
                           int httpStatusCode,
                           bool timeout);

        void onCommandFinished();

        void treatAnswer(AnswerReader *reader, const RequestDesc &desc);

        void sendTcpCommand(HttpTunnelCommand::Enum command,
                            quint16 port,
                            int socketIndex,
                            const QByteArray &data = QByteArray());

    private:
        /*! @brief Generates a random string to be appended at each HTTP request to ensure that no
                   proxy will send back HTTP data from a cache
            @return A random string */
        static QString random()
        { return QString::number(qrand()); }

        static QNetworkRequest createRequest(const QUrl &url);

    private:
        const QString _type;
        const QString _url;
        const ClientEndPointParams _params;
        const QString _desiredIdentifier;
        const QString _desiredPassword;
        const QList<QSslError::SslError> _ignoredSslErrors;
        const QNetworkProxy _proxy;
        QString _id;
        QString _password;
        QList<QByteArray> _dataToSend;
        bool _sendingCommand{false};
        QNetworkAccessManager *_networkManagerListen{nullptr};
        QNetworkAccessManager *_networkManagerCommands{nullptr};
};
