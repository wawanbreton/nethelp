/*! @file   nethelpclient.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include "core/nethelpendpoint.h"

#include <QNetworkProxy>
#include <QStringList>
#include <QPointer>

#include "common/categorizedfiles.h"
#include "common/clientendpointparams.h"
#include "common/stringboolpair.h"

class HttpTunnelClient;
class TcpPseudoServer;
class TcpPseudoClient;
class Path;

/*! @brief Facade class which contains all the methods/signals that should be needed in a
           NetHelp client application */
class NetHelpEndPointClient : public NetHelpEndPoint
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit NetHelpEndPointClient(const QString &serverUrl,
                                       const QNetworkProxy &proxy,
                                       const QString &desiredIdentifier,
                                       const QString &desiredPassword,
                                       const QList<QSslError::SslError> &ignoredSslErrors,
                                       const ClientEndPointParams &params,
                                       QObject *parent = nullptr);

    private:
        void onDownloadFile(const Path &filePath);

        /*! @brief Slot called when the inner object receives a
                   directory listing command
            @param dirPath The absolute path of the directory to be listed */
        void onListDirectory(const Path &dirPath);

        #ifdef NETHELP_CORE_CAMERA
        /*! @brief Slot called when the inner object has grabbed a local webcam image
            @param data The grabbed image as JPEG data */
        void onImageGrabbed(const QByteArray &data);
        #endif

        #ifdef NETHELP_CORE_TCP
        void onTcpConnected(quint16 port, int socketIndex);

        void onClientData(const QByteArray &data, quint16 port, int socketIndex);

        void onTcpDisconnected(quint16 port, int socketIndex);
        #endif

    private:
        #ifdef NETHELP_CORE_TCP
        QList<TcpPseudoClient *> _pseudoClients;
        #endif
        const ClientEndPointParams _params;
};
