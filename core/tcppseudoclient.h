/*! @file   vncproxy.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QObject>

#include <QTcpSocket>
#include <QDateTime>

/*! @brief Simple TCP proxy which connects to a local server in order to forward the incoming
           data from the client */
class TcpPseudoClient : public QObject
{
    Q_OBJECT

    public:
        explicit TcpPseudoClient(quint16 port, int socketIndex, QObject *parent = nullptr);

        /*! @brief Slot to be called when some data is to be forwarded to the server
            @param data The data to be forwarded to the server */
        void onClientData(const QByteArray &data);

        quint16 getPort() const { return _port; }

        int getSocketIndex() const { return _socketIndex; }

    signals:
        /*! @brief Signal emitted when some data have been read from the server
            @param data The data read from the server
            @param port The original incoming data port */
        void serverData(const QByteArray &data, quint16 port, int socketIndex);

    private:
        /*! @brief Slot called when the server socket has been connected */
        void onSocketConnected();

        /*! @brief Slot called when an error occurs on the server socket
            @param socketError The error which just occured */
        void onSocketError(QAbstractSocket::SocketError socketError);

        /*! @brief Slot called when some data is available from the server */
        void onReadyRead();

        /*! @brief Slot possibly called many times to connect to the VNC local server */
        void connectSocket() { _socket->connectToHost("127.0.0.1", _port); }

    private:
        const quint16 _port;
        const int _socketIndex;
        QTcpSocket *_socket{nullptr};
        QDateTime _timeStart;
        QByteArray _startBuffer;
};
