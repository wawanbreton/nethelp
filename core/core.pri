QT *= network

NETHELP_CORE = $${NETHELP_ROOT}/core

contains(NETHELP_CORE_PACKAGES, camera) {
    DEFINES += NETHELP_CORE_CAMERA

    unix {
      INCLUDEPATH += /usr/local/include/opencv4
      LIBS += -L/usr/local/lib
      LIBS += -lopencv_videoio
      LIBS += -lopencv_imgcodecs
      LIBS += -lopencv_core
    }

    win32 {
      LIBS += "-L../libs/OpenCV/bin"
      LIBS += "-L../libs"
      LIBS += -lopencv_highgui231
      LIBS += -lopencv_core231
    }

    SOURCES *= $${NETHELP_CORE}/webcamgrabber.cpp
    HEADERS *= $${NETHELP_CORE}/webcamgrabber.h
}

contains(NETHELP_CORE_PACKAGES, tcp) {
    DEFINES += NETHELP_CORE_TCP
}

contains(NETHELP_CORE_PACKAGES, endpoint-client) {
    NETHELP_CORE_PACKAGES *= endpoint

    SOURCES *= $${NETHELP_CORE}/nethelpendpointclient.cpp
    SOURCES *= $${NETHELP_CORE}/nethelpworkerclient.cpp

    HEADERS *= $${NETHELP_CORE}/nethelpendpointclient.h
    HEADERS *= $${NETHELP_CORE}/nethelpworkerclient.h

    contains(NETHELP_CORE_PACKAGES, tcp) {
        SOURCES *= $${NETHELP_CORE}/tcppseudoclient.cpp
        HEADERS *= $${NETHELP_CORE}/tcppseudoclient.h
    }
}

contains(NETHELP_CORE_PACKAGES, endpoint-viewer) {
    NETHELP_CORE_PACKAGES *= endpoint

    SOURCES *= $${NETHELP_CORE}/nethelpendpointviewer.cpp
    SOURCES *= $${NETHELP_CORE}/nethelpworkerviewer.cpp

    HEADERS *= $${NETHELP_CORE}/nethelpendpointviewer.h
    HEADERS *= $${NETHELP_CORE}/nethelpworkerviewer.h

    contains(NETHELP_CORE_PACKAGES, tcp) {
        SOURCES *= $${NETHELP_CORE}/tcppseudoserver.cpp
        HEADERS *= $${NETHELP_CORE}/tcppseudoserver.h
    }
}

contains(NETHELP_CORE_PACKAGES, endpoint) {
    SOURCES *= $${NETHELP_COMMON}/clientendpointparams.cpp
    SOURCES *= $${NETHELP_CORE}/abstracthttptunnel.cpp
    SOURCES *= $${NETHELP_CORE}/nethelpendpoint.cpp
    SOURCES *= $${NETHELP_CORE}/nethelpworker.cpp

    HEADERS *= $${NETHELP_COMMON}/clientendpointparams.h
    HEADERS *= $${NETHELP_CORE}/abstracthttptunnel.h
    HEADERS *= $${NETHELP_CORE}/nethelpendpoint.h
    HEADERS *= $${NETHELP_CORE}/nethelpworker.h
}

SOURCES *= $${NETHELP_COMMON}/httpstatuscode.cpp
SOURCES *= $${NETHELP_CORE}/abstractcommandreader.cpp
SOURCES *= $${NETHELP_CORE}/answerreader.cpp
SOURCES *= $${NETHELP_CORE}/httptunnelclient.cpp
SOURCES *= $${NETHELP_ROOT}/common/categorizedfiles.cpp
SOURCES *= $${NETHELP_ROOT}/common/path.cpp

HEADERS *= $${NETHELP_COMMON}/argslist.h
HEADERS *= $${NETHELP_COMMON}/constants.h
HEADERS *= $${NETHELP_COMMON}/httpstatuscode.h
HEADERS *= $${NETHELP_CORE}/abstractcommandreader.h
HEADERS *= $${NETHELP_CORE}/answerreader.h
HEADERS *= $${NETHELP_CORE}/httptunnelclient.h
HEADERS *= $${NETHELP_CORE}/httptunnelcommand.h
HEADERS *= $${NETHELP_ROOT}/common/categorizedfiles.h
HEADERS *= $${NETHELP_ROOT}/common/fileinfo.h
HEADERS *= $${NETHELP_ROOT}/common/path.h
HEADERS *= $${NETHELP_ROOT}/common/stringboolpair.h
