#include "nethelpworkerviewer.h"

#include "core/nethelpendpointviewer.h"


NetHelpWorkerViewer::NetHelpWorkerViewer(const QString &serverUrl,
                                         const QNetworkProxy &proxy,
                                         const QList<QSslError::SslError> &ignoredSslErrors,
                                         QObject *parent) :
    NetHelpWorker(new NetHelpEndPointViewer(serverUrl, proxy, ignoredSslErrors), parent)
{
    qRegisterMetaType<QList<StringBoolPair>>();
    qRegisterMetaType<QList<ClientEndPointParams>>();
    qRegisterMetaType<QList<FileInfo>>();

    NetHelpEndPointViewer *endPoint = qobject_cast<NetHelpEndPointViewer *>(accessEndPoint());

    connect(endPoint, &NetHelpEndPointViewer::clientsList,
            this,     &NetHelpWorkerViewer::clientsList);
    connect(endPoint, &NetHelpEndPointViewer::clientParams,
            this,     &NetHelpWorkerViewer::clientParams);
    connect(endPoint, &NetHelpEndPointViewer::takeControlResult,
            this,     &NetHelpWorkerViewer::takeControlResult);
    connect(endPoint, &NetHelpEndPointViewer::fileData,
            this,     &NetHelpWorkerViewer::fileData);
    connect(endPoint, &NetHelpEndPointViewer::picture,
            this,     &NetHelpWorkerViewer::picture);
    connect(endPoint, &NetHelpEndPointViewer::directoryListed,
            this,     &NetHelpWorkerViewer::directoryListed);
}

void NetHelpWorkerViewer::takeControl(const QString &client,
                                      const QString &password,
                                      const ClientEndPointParams &params)
{
    QMetaObject::invokeMethod(accessEndPoint(),
                              "takeControl",
                              Q_ARG(QString, client),
                              Q_ARG(QString, password),
                              Q_ARG(ClientEndPointParams, params));
}

void NetHelpWorkerViewer::releaseControl()
{
    QMetaObject::invokeMethod(accessEndPoint(), "releaseControl");
}

void NetHelpWorkerViewer::listDirectory(const Path &dirName)
{
    QMetaObject::invokeMethod(accessEndPoint(), "listDirectory", Q_ARG(Path, dirName));
}

void NetHelpWorkerViewer::requestClientParams(const QString &clientId)
{
    QMetaObject::invokeMethod(accessEndPoint(), "requestClientParams", Q_ARG(QString, clientId));
}

void NetHelpWorkerViewer::downloadFile(const Path &fileName)
{
    QMetaObject::invokeMethod(accessEndPoint(), "downloadFile", Q_ARG(Path, fileName));
}

void NetHelpWorkerViewer::requestPicture()
{
    QMetaObject::invokeMethod(accessEndPoint(), "requestPicture");
}
