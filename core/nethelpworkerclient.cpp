#include "nethelpworkerclient.h"

#include "core/nethelpendpointclient.h"


NetHelpWorkerClient::NetHelpWorkerClient(const QString &serverUrl,
                                         const QNetworkProxy &proxy,
                                         const QString &desiredIdentifier,
                                         const QString &desiredPassword,
                                         const QList<QSslError::SslError> &ignoredSslErrors,
                                         const ClientEndPointParams &params,
                                         QObject *parent) :
    NetHelpWorker(new NetHelpEndPointClient(serverUrl,
                                            proxy,
                                            desiredIdentifier,
                                            desiredPassword,
                                            ignoredSslErrors,
                                            params),
                  parent)
{
}
