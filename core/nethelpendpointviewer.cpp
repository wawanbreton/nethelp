/*! @file   nethelpclient.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelpendpointviewer.h"

#include <QImage>

#include "core/httptunnelclient.h"
#include "core/tcppseudoserver.h"


NetHelpEndPointViewer::NetHelpEndPointViewer(const QString &serverUrl,
                                             const QNetworkProxy &proxy,
                                             const QList<QSslError::SslError> &ignoredSslErrors,
                                             QObject *parent) :
    NetHelpEndPoint("viewer",
                    serverUrl,
                    proxy,
                    QString(),
                    QString(),
                    ignoredSslErrors,
                    ClientEndPointParams(),
                    parent)
{
    connect(this, &NetHelpEndPointViewer::controlReleased,
            this, &NetHelpEndPointViewer::clearPseudoServers);

    connect(accessClient(), &HttpTunnelClient::clientsList,
            this,           &NetHelpEndPointViewer::clientsList);
    connect(accessClient(), &HttpTunnelClient::takeControlResult,
            this,           &NetHelpEndPointViewer::onTakeControlResult);
    connect(accessClient(), &HttpTunnelClient::clientParams,
            this,           &NetHelpEndPointViewer::clientParams);
    connect(accessClient(), &HttpTunnelClient::fileData,
            this,           &NetHelpEndPointViewer::fileData);
    connect(accessClient(), &HttpTunnelClient::pictureData,
            this,           &NetHelpEndPointViewer::onImageData);
    connect(accessClient(), &HttpTunnelClient::tcpData,
            this,           &NetHelpEndPointViewer::onServerData);
    connect(accessClient(), &HttpTunnelClient::directoryListed,
            this,           &NetHelpEndPointViewer::directoryListed);
}

void NetHelpEndPointViewer::takeControl(const QString &client,
                                        const QString &password,
                                        const ClientEndPointParams &params)
{
    _controlledClientParams = params;
    accessClient()->sendTakeControl(client, password);
}

void NetHelpEndPointViewer::releaseControl()
{
    accessClient()->sendReleaseControl();
}

void NetHelpEndPointViewer::requestClientParams(const QString &clientId)
{
    accessClient()->sendAskClientData(clientId);
}

void NetHelpEndPointViewer::downloadFile(const Path &filePath)
{
    accessClient()->sendDownloadFile(filePath);
}

void NetHelpEndPointViewer::listDirectory(const Path &dirName)
{
    accessClient()->sendListDirectory(dirName);
}

void NetHelpEndPointViewer::requestPicture()
{
    accessClient()->sendAskPicture();
}

void NetHelpEndPointViewer::onTakeControlResult(bool success)
{
    if(success)
    {
        for(const MappedTcpPort &tcpPort : _controlledClientParams.tcpPorts)
        {
            TcpPseudoServer *pseudoServer = new TcpPseudoServer(tcpPort.actualPort,
                                                                tcpPort.replacementPort,
                                                                this);

            connect(pseudoServer,   &TcpPseudoServer::socketConnected,
                    accessClient(), &HttpTunnelClient::sendTcpConnected);
            connect(pseudoServer,   &TcpPseudoServer::clientData,
                    accessClient(), &HttpTunnelClient::sendTcpData);
            connect(pseudoServer,   &TcpPseudoServer::socketDisconnected,
                    accessClient(), &HttpTunnelClient::sendTcpDisconnected);

            _pseudoServers.append(pseudoServer);
        }
    }

    emit takeControlResult(success);
}

void NetHelpEndPointViewer::onImageData(const QByteArray &data, int size)
{
    _pictureData.append(data);
    if(_pictureData.size() >= size)
    {
        emit picture(QImage::fromData(_pictureData, "JPG"));
        _pictureData.clear();
    }
}

void NetHelpEndPointViewer::onServerData(QByteArray data, quint16 port, int socketIndex)
{
    for(TcpPseudoServer *pseudoServer : _pseudoServers)
    {
        if(pseudoServer->getActualPort() == port)
        {
            pseudoServer->onServerData(data, socketIndex);
            return;
        }
    }
}

void NetHelpEndPointViewer::clearPseudoServers()
{
    for(TcpPseudoServer *pseudoServer : _pseudoServers)
    {
        pseudoServer->deleteLater();
    }
    _pseudoServers.clear();
}
