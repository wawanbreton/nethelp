#include "answerreader.h"

#include <QUrlQuery>

#include "common/httpstatuscode.h"


AnswerReader::AnswerReader(HttpTunnelCommand::Enum sentCommand,
                           int timeout,
                           QObject *parent) :
    AbstractCommandReader(timeout, parent),
    _sentCommand(sentCommand)
{

}

void AnswerReader::setReply(QNetworkReply *reply)
{
    setDevice(reply);
    _reply = reply;

    connect(_reply, qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error),
            this,   &AnswerReader::onReplyError);
    connect(_reply, &QNetworkReply::metaDataChanged,
            this,   &AnswerReader::onMetaDataChanged);
    connect(_reply, &QNetworkReply::finished,
            this,   &AnswerReader::onTransferFinished);
}

bool AnswerReader::parseHeader(QByteArray &buffer,
                               HttpTunnelCommand::Enum &command,
                               ArgsList &arguments,
                               int &contentLength)
{
    contentLength = _reply->header(QNetworkRequest::ContentLengthHeader).toInt();

    int headerEndIndex = buffer.indexOf("\r\n");
    if(headerEndIndex >= 0)
    {
        // Header is complete
        QString url = QString::fromUtf8(buffer.mid(0, headerEndIndex));
        buffer = buffer.mid(headerEndIndex + 2);
        contentLength -= headerEndIndex + 2;

        QUrlQuery query(url);
        QList<QPair<QString, QString>> items = query.queryItems(QUrl::FullyEncoded);

        parseUrl(url, command, arguments);
        return true;
    }

    return false;
}

void AnswerReader::onTimeout()
{
    emit error(QNetworkReply::NoError, -1, true);
    onFatalError();
}

void AnswerReader::onReplyError(QNetworkReply::NetworkError networkError)
{
    emit error(networkError, -1, false);
    onFatalError();
}

void AnswerReader::onMetaDataChanged()
{
    if(_reply)
    {
        int httpStatusCode = _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(httpStatusCode != HttpStatusCode::Ok)
        {
            emit error(QNetworkReply::NoError, httpStatusCode, false);
            onFatalError();
        }
        else
        {
            int contentLength = _reply->header(QNetworkRequest::ContentLengthHeader).toInt();
            if(contentLength == 0)
            {
                onTransferFinished();
            }
        }
    }
    else
    {
        qCritical() << "There is no reply ?!";
    }
}
