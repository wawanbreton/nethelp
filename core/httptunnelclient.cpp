/*! @file   httptunnelclient.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "httptunnelclient.h"

#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkProxy>
#include <QNetworkRequest>
#include <QSslError>
#include <QTimer>
#include <QUrl>
#include <QUuid>

#include "common/constants.h"
#include "common/httpstatuscode.h"
#include "core/answerreader.h"


HttpTunnelClient::HttpTunnelClient(const QString &type,
                                   const QString &url,
                                   const QNetworkProxy &proxy,
                                   const ClientEndPointParams &params,
                                   const QString &desiredIdentifier,
                                   const QString &desiredPassword,
                                   const QList<QSslError::SslError> &ignoredSslErrors,
                                   QObject *parent) :
    AbstractHttpTunnel(parent),
    _type(type),
    _url(url),
    _params(params),
    _desiredIdentifier(QString(desiredIdentifier).replace('/', '_')),
    _desiredPassword(desiredPassword),
    _ignoredSslErrors(ignoredSslErrors),
    _proxy(proxy),
    _networkManagerListen(new QNetworkAccessManager(this)),
    _networkManagerCommands(new QNetworkAccessManager(this))
{
    _networkManagerListen->setProxy(_proxy);
    connect(_networkManagerListen, &QNetworkAccessManager::sslErrors,
            this,                  &HttpTunnelClient::onSslErrors);

    _networkManagerCommands->setProxy(_proxy);
    connect(_networkManagerCommands, &QNetworkAccessManager::sslErrors,
            this,                    &HttpTunnelClient::onSslErrors);
}

bool HttpTunnelClient::sendCommandImpl(const NetHelpCommand &command)
{
    if(_sendingCommand)
    {
        return false;
    }

    qDebug() << "Sending  command" << command.command << command.arguments << command.data.size();

    ArgsList arguments = command.arguments;
    if(!_id.isEmpty())
    {
        // Once we have an ID, all requests shall be identified
        arguments.insert("identifier", _id);
    }

    QUrl url(makeUrl(_url, command.command, arguments));
    QNetworkReply *reply = _networkManagerCommands->post(createRequest(url), command.data);

    AnswerReader *reader = new AnswerReader(command.command, Constants::requestTimeout, this);
    connect(reader, &AnswerReader::headerParsed, this,   &HttpTunnelClient::onAnswerHeaderParsed);
    connect(reader, &AnswerReader::error,        this,   &HttpTunnelClient::onAnswerError);
    connect(reader, &AnswerReader::finished,     this,   &HttpTunnelClient::onCommandFinished);
    connect(reader, &AnswerReader::finished,     reader, &AnswerReader::deleteLater);
    connect(reader, &AnswerReader::finished,     reply,  &QNetworkReply::deleteLater);
    reader->setReply(reply);

    _sendingCommand = true;

    return true;
}

void HttpTunnelClient::listenForCommand()
{
    qDebug() << "Sending listen";

    QUrl url(makeUrl(_url, HttpTunnelCommand::Listen, {{"identifier", _id}}));
    QNetworkReply *reply = _networkManagerListen->get(createRequest(url));

    AnswerReader *reader = new AnswerReader(HttpTunnelCommand::Listen,
                                            Constants::serverListenAnswerTimeout,
                                            this);
    connect(reader, &AnswerReader::headerParsed, this,   &HttpTunnelClient::onListenHeaderParsed);
    connect(reader, &AnswerReader::error,        this,   &HttpTunnelClient::onListenError);
    connect(reader, &AnswerReader::finished,     reader, &AnswerReader::deleteLater);
    connect(reader, &AnswerReader::finished,     reply,  &QNetworkReply::deleteLater);
    reader->setReply(reply);
}

void HttpTunnelClient::tryConnect()
{
    ArgsList args;
    if(!_desiredIdentifier.isEmpty())
    {
        args.insert("desiredIdentifier", _desiredIdentifier);
    }
    if(!_desiredPassword.isEmpty())
    {
        args.insert("desiredPassword", _desiredPassword);
    }
    if(_type == "viewer")
    {
        args.insert("listClients", "true");
    }
    if(_type == "client")
    {
        args.insert("controllable", "true");
    }

    QByteArray paramsData;
    if(!_params.information.isEmpty() ||
       !_params.iconData.isEmpty() ||
       !_params.guiDescription.isEmpty() ||
       !_params.interestingFiles.isEmpty() ||
       !_params.tcpPorts.isEmpty())
    {
        QJsonDocument doc;
        doc.setObject(_params.save());
        paramsData = doc.toJson();
    }

    sendCommand({HttpTunnelCommand::Hello, args, paramsData});
}

void HttpTunnelClient::disconnect()
{
    sendCommand({HttpTunnelCommand::Goodbye});
}

void HttpTunnelClient::sendAskClientData(const QString &clientId)
{
    ArgsList arguments;
    arguments.insert("clientId", clientId);
    sendCommand({HttpTunnelCommand::ClientData, arguments});
}

void HttpTunnelClient::sendTakeControl(const QString &clientId, const QString &password)
{
    ArgsList arguments;
    arguments.insert("clientId", clientId);
    arguments.insert("password", password);
    sendCommand({HttpTunnelCommand::TakeControl, arguments});
}

void HttpTunnelClient::sendListDirectory(const Path &dirName, const QList<FileInfo> &infos)
{
    ArgsList arguments;
    arguments.insert("dirPath", dirName.toNormalized().getPath());

    QJsonArray arrayFileInfos;
    for(const FileInfo &info : infos)
    {
        QJsonObject objectFileInfo;

        objectFileInfo.insert("name", info.name);


        if(info.isFile)
        {
            objectFileInfo.insert("type", "file");
            objectFileInfo.insert("size", info.size);
        }
        else
        {
            objectFileInfo.insert("type", "directory");
        }

        arrayFileInfos.append(objectFileInfo);
    }

    sendCommand({HttpTunnelCommand::ListDirectory,
                 arguments,
                 QJsonDocument(arrayFileInfos).toJson()});
}

void HttpTunnelClient::sendDownloadFile(const Path &filePath)
{
    ArgsList arguments;
    arguments.insert("filePath", filePath.toNormalized().getPath());
    sendCommand({HttpTunnelCommand::DownloadFile, arguments});
}

void HttpTunnelClient::sendFileData(const Path &filePath, const QByteArray &data, int totalSize)
{
    ArgsList arguments;
    arguments.insert("filePath", filePath.toNormalized().getPath());
    arguments.insert("size", QString::number(totalSize));
    sendCommand({HttpTunnelCommand::FileData, arguments, data});
}

void HttpTunnelClient::sendPicture(const QByteArray &data)
{
    ArgsList arguments;
    arguments.insert("size", QString::number(data.size()));
    sendCommand({HttpTunnelCommand::PictureData, arguments, data});
}

void HttpTunnelClient::sendTcpCommand(HttpTunnelCommand::Enum command,
                                      quint16 port,
                                      int socketIndex,
                                      const QByteArray &data)
{
    ArgsList arguments;
    arguments.insert("port", QString::number(port));
    arguments.insert("socketIndex", QString::number(socketIndex));
    sendCommand({command, arguments, data});
}

void HttpTunnelClient::sendTcpConnected(quint16 port, int socketIndex)
{
    sendTcpCommand(HttpTunnelCommand::TcpConnected, port, socketIndex);
}

void HttpTunnelClient::sendTcpData(const QByteArray &data, quint16 port, int socketIndex)
{
    sendTcpCommand(HttpTunnelCommand::TcpData, port, socketIndex, data);
}

void HttpTunnelClient::sendTcpDisconnected(quint16 port, int socketIndex)
{
    sendTcpCommand(HttpTunnelCommand::TcpDisconnected, port, socketIndex);
}

void HttpTunnelClient::sendPleaseReleaseControl(const QString user, const QString &clientId)
{
    ArgsList arguments;
    arguments.insert("user", user);
    arguments.insert("clientId", clientId);
    sendCommand({HttpTunnelCommand::PleaseReleaseControl, arguments});
}

void HttpTunnelClient::fatalRequestError()
{
    emit connectionLost();
    emit over();
}

void HttpTunnelClient::onAnswerHeaderParsed(const RequestDesc &desc)
{
    qDebug() << "Header parsed for command" << desc.command << desc.arguments;

    AnswerReader *reader = qobject_cast<AnswerReader *>(sender());
    if(reader)
    {
        treatAnswer(reader, desc);
    }
    else
    {
        qCritical() << "Sender is not an AnswerReader" << sender();
    }
}

void HttpTunnelClient::onAnswerError(QNetworkReply::NetworkError networkError,
                                     int httpStatusCode,
                                     bool timeout)
{
    QString command;
    AnswerReader *reader = qobject_cast<AnswerReader *>(sender());
    if(reader)
    {
        command = HttpTunnelCommand::toString(reader->getSentCommand());

        bool fatal = httpStatusCode != HttpStatusCode::NoContent &&
                     httpStatusCode != HttpStatusCode::NotFound;

        qWarning() << "Error for command" << command
                   << networkError << httpStatusCode << timeout << fatal;

        if(reader->getSentCommand() == HttpTunnelCommand::Hello)
        {
            QString userMessage;
            if(networkError != QNetworkReply::NoError)
            {
                userMessage = tr("Network error (code %1)").arg(networkError);
            }
            else if(httpStatusCode != -1)
            {
                userMessage = tr("Server HTTP error (code %1)").arg(httpStatusCode);
            }
            else
            {
                userMessage = tr("Request timeout");
            }

            emit openSessionEnd(userMessage);
            emit over();
        }
        else if(fatal)
        {
            fatalRequestError();
        }
    }
    else
    {
        qCritical() << "Sender is not an AnswerReader ?!" << sender();
    }

}

void HttpTunnelClient::treatAnswer(AnswerReader *reader, const RequestDesc &desc)
{
    qDebug() << "Received command" << desc.command << desc.arguments << desc.expectsData;

    switch(desc.command)
    {
        case HttpTunnelCommand::None:
        {
            qWarning() << "Unrecognized request type" << desc.command;
            break;
        }
        case HttpTunnelCommand::Hello:
        {
            if(reader->getSentCommand() == HttpTunnelCommand::Hello)
            {
                _id = desc.arguments.value("identifier");
                _password = desc.arguments.value("password");

                if(desc.expectsData)
                {
                    reader->setSendDataWhenComplete(true);
                    connect(reader, &AbstractCommandReader::dataAvailable,
                            this,   &HttpTunnelClient::onClientsListData);
                }

                listenForCommand();
                emit openSessionEnd(QString());
            }
            else
            {
                qWarning() << "Unexpected hello command received from server :"
                           << reader->getSentCommand();
                emit openSessionEnd(tr("Unexpected server answer"));
                emit over();
            }

            break;
        }
        case HttpTunnelCommand::Goodbye:
        {
            if(reader->getSentCommand() == HttpTunnelCommand::Goodbye)
            {
                emit over();
            }
            else
            {
                qWarning() << "Unexpected goodbye command received from server :"
                           << reader->getSentCommand();
            }
            break;
        }
        case HttpTunnelCommand::ClientData:
        {
            reader->setSendDataWhenComplete(true);
            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &HttpTunnelClient::onClientParamsData);
            break;
        }
        case HttpTunnelCommand::ClientsList:
        {
            reader->setSendDataWhenComplete(true);
            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &HttpTunnelClient::onClientsListData);
            break;
        }
        case HttpTunnelCommand::TakeControl:
        {
            emit takeControlResult(desc.arguments.value("success") == "true");
            break;
        }
        case HttpTunnelCommand::ControlTaken:
        {
            emit controlTaken();
            break;
        }
        case HttpTunnelCommand::ControlReleased:
        {
            emit controlReleased();
            break;
        }
        case HttpTunnelCommand::ListDirectory:
        {
            reader->setSendDataWhenComplete(true);
            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &HttpTunnelClient::onDirectoryListData);
            break;
        }
        case HttpTunnelCommand::DownloadFile:
        {
            emit downloadFile(Path(desc.arguments.value("filePath"), true));
            break;
        }
        case HttpTunnelCommand::FileData:
        {
            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &HttpTunnelClient::onFileData);
            break;
        }
        case HttpTunnelCommand::GetPicture:
        {
            emit pictureAsked();
            break;
        }
        case HttpTunnelCommand::PictureData:
        {
            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &HttpTunnelClient::onPictureData);
            break;
        }
        case HttpTunnelCommand::TcpConnected:
        {
            emit tcpConnected(desc.arguments.value("port").toUShort(),
                              desc.arguments.value("socketIndex").toInt());
            break;
        }
        case HttpTunnelCommand::TcpData:
        {
            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &HttpTunnelClient::onTcpData);
            break;
        }
        case HttpTunnelCommand::TcpDisconnected:
        {
            emit tcpDisconnected(desc.arguments.value("port").toUShort(),
                                 desc.arguments.value("socketIndex").toInt());
            break;
        }
        case HttpTunnelCommand::PleaseReleaseControl:
        {
            emit pleaseReleaseControl(desc.arguments.value("user"),
                                      desc.arguments.value("clientId"));
            break;
        }
        case HttpTunnelCommand::Listen:
        case HttpTunnelCommand::ReleaseControl:
        {
            qWarning() << "Received unexpected command from server" << desc.command;
            fatalRequestError();
            break;
        }
    }
}

void HttpTunnelClient::onClientsListData(const QByteArray &data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray clientsArray = doc.array();

    QList<StringBoolPair> clients;

    for(const QJsonValue &clientValue : clientsArray)
    {
        QJsonObject clientObject = clientValue.toObject();
        clients.append({clientObject["identifier"].toString(),
                        clientObject["controlled"].toBool()});
    }

    emit clientsList(clients);
}

void HttpTunnelClient::onClientParamsData(const QByteArray &data)
{
    AbstractCommandReader *reader = qobject_cast<AbstractCommandReader *>(sender());
    if(reader)
    {
        ClientEndPointParams params;
        params.load(QJsonDocument::fromJson(data).object());
        emit clientParams(reader->getRequestDesc().arguments.value("clientId"), params);
    }
    else
    {
        qCritical() << "Sender is not an AbstractCommandReader";
    }
}

void HttpTunnelClient::onDirectoryListData(const QByteArray &data)
{
    AbstractCommandReader *reader = qobject_cast<AbstractCommandReader *>(sender());
    if(reader)
    {
        Path dirPath(reader->getRequestDesc().arguments.value("dirPath"), true);

        QList<FileInfo> infos;
        for(const QJsonValue &valueInfo : QJsonDocument::fromJson(data).array())
        {
            QJsonObject objectInfo = valueInfo.toObject();

            FileInfo info;
            info.name = objectInfo["name"].toString();
            info.isFile = objectInfo["type"].toString() == "file";
            if(info.isFile)
            {
                info.size = objectInfo["size"].toInt();
            }

            infos << info;
        }

        emit directoryListed(dirPath, infos);
    }
    else
    {
        qCritical() << "Sender is not an AbstractCommandReader";
    }
}

void HttpTunnelClient::onFileData(const QByteArray &data)
{
    AbstractCommandReader *reader = qobject_cast<AbstractCommandReader *>(sender());
    if(reader)
    {
        Path filePath(reader->getRequestDesc().arguments.value("filePath"), true);
        int fileSize = reader->getRequestDesc().arguments.value("size").toInt();

        emit fileData(filePath, data, fileSize);
    }
    else
    {
        qCritical() << "Sender is not an AbstractCommandReader";
    }
}

void HttpTunnelClient::onPictureData(const QByteArray &data)
{
    AbstractCommandReader *reader = qobject_cast<AbstractCommandReader *>(sender());
    if(reader)
    {
        emit pictureData(data, reader->getRequestDesc().arguments.value("size").toInt());
    }
    else
    {
        qCritical() << "Sender is not an AbstractCommandReader";
    }
}

void HttpTunnelClient::onTcpData(const QByteArray &data)
{
    AbstractCommandReader *reader = qobject_cast<AbstractCommandReader *>(sender());
    if(reader)
    {
        emit tcpData(data,
                     reader->getRequestDesc().arguments.value("port").toUShort(),
                     reader->getRequestDesc().arguments.value("socketIndex").toInt());
    }
    else
    {
        qCritical() << "Sender is not an AbstractCommandReader";
    }
}

void HttpTunnelClient::onListenHeaderParsed(const RequestDesc &desc)
{
    qDebug() << "Listen answer received" << desc.command << desc.arguments;

    AnswerReader *reader = qobject_cast<AnswerReader *>(sender());
    if(reader)
    {
        #warning use a DataAggregator so that we can restart listening right now
        // This is an answer from the server with relevant data : treat it and re-listen ASAP
        connect(reader, &AnswerReader::finished, this, &HttpTunnelClient::listenForCommand);
        treatAnswer(reader, desc);
    }
    else
    {
        qCritical() << "Sender is not an AnswerReader";
    }
}

void HttpTunnelClient::onListenError(QNetworkReply::NetworkError networkError,
                                     int httpStatusCode,
                                     bool timeout)
{
    qDebug() << "Listen error" << networkError << httpStatusCode << timeout;

    if(networkError != QNetworkReply::NoError)
    {
        // Communication error somehow
        qWarning() << "Listen network error" << networkError;
        fatalRequestError();
    }
    else if(timeout)
    {
        // Server didn't answer within given time, consider it as fatal error
        qWarning() << "Listen timeout";
        fatalRequestError();
    }
    else if(httpStatusCode == HttpStatusCode::NoContent)
    {
        // This is an answer from the server, politely indicating that it has
        // no data yet : re-listen ASAP
        AnswerReader *reader = qobject_cast<AnswerReader *>(sender());
        if(reader)
        {
            connect(reader, &AnswerReader::finished, this, &HttpTunnelClient::listenForCommand);
        }
        else
        {
            qCritical() << "Sender is not an AnswerReader";
        }
    }
    else
    {
        // Protocol error, maybe a proxy caught our request and dropped it
        qWarning() << "Listen protocol error" << httpStatusCode;
        fatalRequestError();
    }
}

void HttpTunnelClient::onCommandFinished()
{
    AnswerReader *reader = qobject_cast<AnswerReader *>(sender());
    if(reader)
    {
        qDebug() << "Command finished"
                 << reader->getSentCommand()
                 << reader->getRequestDesc().command << reader->getRequestDesc().arguments;
    }
    else
    {
        qCritical() << "Sender is not an AnswerReader";
    }

    _sendingCommand = false;
    trySendNextCommand();
}

QNetworkRequest HttpTunnelClient::createRequest(const QUrl &url)
{
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/octet-stream");
    return request;
}

void HttpTunnelClient::onSslErrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
    bool ignoreErrors = true;

    for(const QSslError &error : errors)
    {
        if(!_ignoredSslErrors.contains(error.error()))
        {
            ignoreErrors = false;
        }
    }

    if(ignoreErrors)
    {
        reply->ignoreSslErrors();
    }
}
