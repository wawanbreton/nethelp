#include "abstractcommandreader.h"

#include <QTimer>
#include <QUrlQuery>


AbstractCommandReader::AbstractCommandReader(int timeout, QObject *parent) :
    QObject(parent)
{
    QTimer::singleShot(timeout, this, &AbstractCommandReader::onTimeout);
}

void AbstractCommandReader::setDevice(QIODevice *device)
{
    _device = device;
    connect(_device, &QIODevice::readyRead, this, &AbstractCommandReader::onReadyRead);
    connect(_device, &QIODevice::destroyed, this, &AbstractCommandReader::onTransferFinished);

    if(_device->bytesAvailable())
    {
        // In some cases, device is given after the readyRead signal has been emitted
        onReadyRead();
    }
}

void AbstractCommandReader::onFatalError()
{
    onFinished(false);
}

void AbstractCommandReader::onTransferFinished()
{
    if(_status == WaitingForHeaderData || _status == WaitingForPayloadData)
    {
        // It seems that no data is to be received, emit the finished signal right now
        onFinished(true);
    }
}

void AbstractCommandReader::onReadyRead()
{
    _buffer.append(_device->readAll());

    if(_status == WaitingForHeaderData)
    {
        if(parseHeader(_buffer, _requestDesc.command, _requestDesc.arguments, _contentLength))
        {
            _requestDesc.expectsData = _contentLength > 0;

            emit headerParsed(_requestDesc);

            if(_requestDesc.expectsData)
            {
                _status = WaitingForPayloadData;
            }
            else
            {
                onFinished(true);
            }
        }
    }

    if(_status == WaitingForPayloadData && !_buffer.isEmpty())
    {
        bool over = false;

        if(_contentLength > 0)
        {
            _readDataBytes += _buffer.size();
            over = _readDataBytes >= _contentLength;
            if(over || !_sendDataWhenComplete)
            {
                emit dataAvailable(_buffer, over);
                _buffer.clear();
            }
        }
        else
        {
            over = true;
        }

        if(over)
        {
            onFinished(true);
        }
    }
}

void AbstractCommandReader::onFinished(bool success)
{
    _status = success ? AllDataReceived : Error;
    disconnect(_device, nullptr, this, nullptr);
    emit finished();
}

void AbstractCommandReader::parseUrl(const QString &url,
                                     HttpTunnelCommand::Enum &command,
                                     ArgsList &arguments)
{
    const QList<QPair<QString, QString>> items = QUrlQuery(url).queryItems();
    if(!items.isEmpty())
    {
        QString commandStr = items.first().first.mid(1);
        command = HttpTunnelCommand::fromString(commandStr);

        for(const QPair<QString, QString> &argument : items.mid(1))
        {
            if(arguments.constFind(argument.first) == arguments.constEnd())
            {
                arguments.insert(argument.first, argument.second);
            }
            else
            {
                qWarning() << "Argument" << argument.first
                           << "has been used multiple times, skipping";
            }
        }
    }

    if(command == HttpTunnelCommand::None)
    {
        qWarning() << "Unable to parse command from URL" << url;
    }
}
