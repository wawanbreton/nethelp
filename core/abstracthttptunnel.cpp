#include "abstracthttptunnel.h"

#include <QUrlQuery>


AbstractHttpTunnel::AbstractHttpTunnel(QObject *parent) : QObject(parent)
{
}

void AbstractHttpTunnel::sendCommand(const NetHelpCommand &command)
{
    qDebug() << "<=== Command to send" << command.command
             << command.arguments << command.data.size();

    if(_commandsToSend.isEmpty())
    {
        if(sendCommandImpl(command))
        {
            qDebug() << ">=== Command sent";
        }
        else
        {
            // It is not possible to send the command right now, keep it for later
            qDebug() << ">=== Keep for later (not possible yet)";
            _commandsToSend.append(command);
        }
    }
    else
    {
        // It is obviously not possible to send the command right now
        if(HttpTunnelCommand::largeDataCommand(command.command))
        {
            // Inner data may be gathered with a similar command : try to find one
            for(NetHelpCommand &commandToSend : _commandsToSend)
            {
                if(commandToSend.command == command.command &&
                   commandToSend.arguments == command.arguments)
                {
                    // Aggregate data to previous command, and exit
                    qDebug() << ">=== Aggregate to existing data";
                    commandToSend.data.append(command.data);
                    return;
                }
            }
        }

        qDebug() << ">=== Keep for later (busy)";
        _commandsToSend.append(command);
    }
}

bool AbstractHttpTunnel::trySendNextCommand()
{
    qDebug() << "<=== Trying to send next command";

    if(!_commandsToSend.isEmpty())
    {
        HttpTunnelCommand::Enum highestLevelCommand = HttpTunnelCommand::None;
        int commandToSendIndex = -1;
        for(int commandIndex = 0 ; commandIndex < _commandsToSend.count() ; ++commandIndex)
        {
            const NetHelpCommand &command = _commandsToSend.at(commandIndex);
            if(command.command > highestLevelCommand)
            {
                commandToSendIndex = commandIndex;
                highestLevelCommand = command.command;
            }
        }

        if(commandToSendIndex >= 0)
        {
            const NetHelpCommand &commandToSend = _commandsToSend.at(commandToSendIndex);
            if(sendCommandImpl(commandToSend))
            {
                qDebug() << ">=== Command sent";
                _commandsToSend.removeAt(commandToSendIndex);
                return true;
            }
            else
            {
                qDebug() << ">=== Keep for later (not possible yet)";
            }
        }
        else
        {
            qCritical() << "Unable to find the most prioritary command ?!";
        }
    }
    else
    {
        qDebug() << ">=== No command to send yet";
    }

    return false;
}

QString AbstractHttpTunnel::makeUrl(const QString &host,
                                    HttpTunnelCommand::Enum command,
                                    const ArgsList &args)
{
    QString separator;
    if(!host.endsWith('/'))
    {
        separator = "/";
    }

    QString baseUrl("%1%2%3");
    baseUrl = baseUrl.arg(host);
    baseUrl = baseUrl.arg(separator);
    baseUrl = baseUrl.arg(HttpTunnelCommand::toString(command));

    QUrlQuery query(baseUrl);
    for(auto iterator = args.constBegin() ; iterator != args.constEnd() ; ++iterator)
    {
        if(!iterator.key().isEmpty() && !iterator.value().isEmpty())
        {
            query.addQueryItem(iterator.key(), iterator.value());
        }
    }

    return query.query(QUrl::FullyEncoded);
}
