#pragma once

#include <QObject>

#include <QIODevice>

#include "core/requestdesc.h"

class AbstractCommandReader : public QObject
{
    Q_OBJECT

    public:
        QIODevice *accessDevice() { return _device; }

        const RequestDesc &getRequestDesc() const { return _requestDesc; }

        void setSendDataWhenComplete(bool sendDataWhenComplete)
        { _sendDataWhenComplete = sendDataWhenComplete; }

    signals:
        /*! @brief Signal emitted as soon as the header data is complete
         *  @param desc The description of the received request
         *  @note The signal may not be emitted if the message does not contain relevant header data
         */
        void headerParsed(const RequestDesc &desc);

        /*! @brief Signal emitted after the headerParsed() signal has been emitted, with a request
         *         indicating that data is expected. It may be emitted multiple times if a lot of
         *         data is incoming.
         *  @param data The received data packet, which may not be complete (further data may come)
         *  @note The signal may not be emitted if the message does not contain relevant header data
         *  @sa setSendDataWhenComplete() */
        void dataAvailable(const QByteArray &data, bool complete);

        /*! @brief Signal emitted when the reader has finished its job, either when there is no
         *         incoming data, or all incoming data has been parsed, or an error occured */
        void finished();

    protected:
        explicit AbstractCommandReader(int timeout, QObject *parent = nullptr);

        void setDevice(QIODevice *device);

        void onFatalError();

        void onTransferFinished();

        virtual void onTimeout() = 0;

        virtual bool parseHeader(QByteArray &buffer,
                                 HttpTunnelCommand::Enum &command,
                                 ArgsList &arguments,
                                 int &contentLength) = 0;

        static void parseUrl(const QString &url,
                             HttpTunnelCommand::Enum &command,
                             ArgsList &arguments);

    private:
        typedef enum
        {
            WaitingForHeaderData,
            WaitingForPayloadData,
            AllDataReceived,
            Error
        } ReaderStatus;

    private:
        void onReadyRead();

        void onFinished(bool success);

    private:
        QIODevice *_device{nullptr};
        QByteArray _buffer;
        RequestDesc _requestDesc;
        ReaderStatus _status{WaitingForHeaderData};
        int _contentLength{-1};
        int _readDataBytes{0};
        bool _sendDataWhenComplete{false};
};

