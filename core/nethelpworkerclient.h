#pragma once

#include "core/nethelpworker.h"

#include <QNetworkProxy>
#include <QSslError>

#include "common/clientendpointparams.h"

class NetHelpWorkerClient : public NetHelpWorker
{
    Q_OBJECT

    public:
        explicit NetHelpWorkerClient(const QString &serverUrl,
                                     const QNetworkProxy &proxy,
                                     const QString &desiredIdentifier,
                                     const QString &desiredPassword,
                                     const QList<QSslError::SslError> &ignoredSslErrors,
                                     const ClientEndPointParams &params,
                                     QObject *parent = nullptr);
};
