#pragma once

#include <QObject>

#include "core/nethelpcommand.h"

class AbstractCommandReader;

#warning Possible optimization : send commands in parallel, which implies giving them an order ID
class AbstractHttpTunnel : public QObject
{
    Q_OBJECT

    public:
        explicit AbstractHttpTunnel(QObject *parent = nullptr);

        void sendCommand(const NetHelpCommand &command);

    signals:
        void incomingCommand(AbstractCommandReader *reader);

    protected:
        bool trySendNextCommand();

        virtual bool sendCommandImpl(const NetHelpCommand &command) = 0;

        static QString makeUrl(const QString &host,
                               HttpTunnelCommand::Enum command,
                               const ArgsList &args);

    private:
        QList<NetHelpCommand> _commandsToSend;
};
