/*! @file   nethelpclient.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelpendpointclient.h"

#include <QDir>
#include <QFile>
#include <QFileInfoList>

#include "common/constants.h"
#include "core/httptunnelclient.h"
#ifdef NETHELP_CORE_TCP
# include "core/tcppseudoclient.h"
#endif
#ifdef NETHELP_CORE_CAMERA
# include "core/webcamgrabber.h"
#endif


NetHelpEndPointClient::NetHelpEndPointClient(const QString &serverUrl,
                                             const QNetworkProxy &proxy,
                                             const QString &desiredIdentifier,
                                             const QString &desiredPassword,
                                             const QList<QSslError::SslError> &ignoredSslErrors,
                                             const ClientEndPointParams &params,
                                             QObject *parent) :
    NetHelpEndPoint("client",
                    serverUrl,
                    proxy,
                    desiredIdentifier,
                    desiredPassword,
                    ignoredSslErrors,
                    params,
                    parent),
    _params(params)
{
    connect(accessClient(), &HttpTunnelClient::downloadFile,
            this,           &NetHelpEndPointClient::onDownloadFile);
    connect(accessClient(), &HttpTunnelClient::directoryListed,
            this,           &NetHelpEndPointClient::onListDirectory);
    #ifdef NETHELP_CORE_TCP
    connect(accessClient(), &HttpTunnelClient::tcpConnected,
            this,           &NetHelpEndPointClient::onTcpConnected);
    connect(accessClient(), &HttpTunnelClient::tcpData,
            this,           &NetHelpEndPointClient::onClientData);
    connect(accessClient(), &HttpTunnelClient::tcpDisconnected,
            this,           &NetHelpEndPointClient::onTcpDisconnected);
    #endif

    #ifdef NETHELP_CORE_CAMERA
    WebcamGrabber *grabber = new WebcamGrabber(this);
    connect(accessClient(),  &HttpTunnelClient::pictureAsked,
            grabber,         &WebcamGrabber::grabImage);
    connect(accessClient(),  &HttpTunnelClient::controlTaken,
            grabber,         [grabber] { grabber->start(); });
    connect(grabber,         &WebcamGrabber::imageGrabbed,
            this,            &NetHelpEndPointClient::onImageGrabbed);
    #endif
}

void NetHelpEndPointClient::onDownloadFile(const Path &filePath)
{
    QByteArray fileData;

    QFile file(filePath.toLocalized().getPath());
    if(file.exists())
    {
        if(file.open(QIODevice::ReadOnly))
        {
            fileData = qCompress(file.readAll(), 9);
        }
        else
        {
            qWarning() << "Unable to open file" << filePath
                       << "for reading :" << file.errorString();
        }
    }
    else
    {
        qWarning() << "Requested file" << filePath << " does not exist";
    }

    accessClient()->sendFileData(filePath, fileData, fileData.size());
}

void NetHelpEndPointClient::onListDirectory(const Path &dirPath)
{
    QList<FileInfo> builtInfos;
    QFileInfoList files;
    if(dirPath.isRoot())
    {
        // We want the root listing, i.e. on UNIX, the content of "/",
        // and on Windows, the list of drives
        #ifdef Q_OS_UNIX
        files = QDir("/").entryInfoList(QDir::NoFilter, QDir::Name | QDir::DirsFirst);
        #else
        for(const QFileInfo &driveInfo : QDir::drives())
        {
            FileInfo builtInfo;

            builtInfo.name = driveInfo.filePath().remove('/');
            builtInfo.isFile = false;
            builtInfo.size = 0;

            builtInfos << builtInfo;
        }
        #endif
    }
    else
    {
        QDir dir(dirPath.toLocalized().getPath());
        if(dir.exists())
        {
            files = dir.entryInfoList(QDir::NoFilter, QDir::Name | QDir::DirsFirst);
        }
        else
        {
            qWarning() << "The required directory" << dir.absolutePath() << "does not exist";
        }
    }

    if(builtInfos.isEmpty())
    {
        for(const QFileInfo &fileInfo : files)
        {
            if(fileInfo.fileName() != "." && fileInfo.fileName() != "..")
            {
                FileInfo builtInfo;

                builtInfo.name = fileInfo.fileName();
                if(fileInfo.isDir())
                {
                    builtInfo.isFile = false;
                    builtInfo.size = 0;
                }
                else
                {
                    builtInfo.isFile = true;
                    builtInfo.size = fileInfo.size();
                }

                builtInfos << builtInfo;
            }
        }
    }

    accessClient()->sendListDirectory(dirPath, builtInfos);
}

#ifdef NETHELP_CORE_CAMERA
void NetHelpEndPointClient::onImageGrabbed(const QByteArray &data)
{
    accessClient()->sendPicture(data);
}
#endif

#ifdef NETHELP_CORE_TCP
void NetHelpEndPointClient::onTcpConnected(quint16 port, int socketIndex)
{
    TcpPseudoClient *pseudoClient = new TcpPseudoClient(port, socketIndex, this);

    connect(pseudoClient,   &TcpPseudoClient::serverData,
            accessClient(), &HttpTunnelClient::sendTcpData);

    _pseudoClients.append(pseudoClient);

    qInfo() << "TCP socket connected" << port << socketIndex;
}

void NetHelpEndPointClient::onClientData(const QByteArray &data, quint16 port, int socketIndex)
{
    for(TcpPseudoClient *pseudoClient : _pseudoClients)
    {
        if(pseudoClient->getPort() == port && pseudoClient->getSocketIndex() == socketIndex)
        {
            pseudoClient->onClientData(data);
            return;
        }
    }

    qWarning() << "Received data for unknown port" << port;
}

void NetHelpEndPointClient::onTcpDisconnected(quint16 port, int socketIndex)
{
    QMutableListIterator<TcpPseudoClient *> iterator(_pseudoClients);
    while(iterator.hasNext())
    {
        iterator.next();
        if(iterator.value()->getPort() == port && iterator.value()->getSocketIndex() == socketIndex)
        {
            qInfo() << "TCP socket disconnected" << port << socketIndex;
            iterator.value()->deleteLater();
            iterator.remove();
            return;
        }
    }

    qWarning() << "Unexpected TCP socket disconnected" << port << socketIndex;
}
#endif
