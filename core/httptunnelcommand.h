/*! @file   httptunnelcommand.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include "common/enum.h"

/*! @brief Enum listing the possible commands for the NetHelp protocol
    @warning Comands order define their priority ! Handle with care :) */
class HttpTunnelCommand : public QObject
{
    Q_OBJECT
    Q_ENUMS(Enum)

    public:
        typedef enum
        {
            None = 0,

            Hello,
            Goodbye,
            Listen,

            ClientsList,
            ClientData,

            ListDirectory,

            DownloadFile,
            FileData,

            GetPicture,
            PictureData,

            TcpDisconnected,
            TcpData,
            TcpConnected,

            TakeControl,
            ControlTaken,
            ReleaseControl,
            ControlReleased,
            PleaseReleaseControl,
        } Enum;

        /*! @brief Indicates whether the command carries "large" data : such commands are to be
         *         forwarded as soon as possible from the server to the remote client, so their
         *         inner data may be split back into multiple commands */
        static bool largeDataCommand(Enum command)
        {
            return command == FileData || command == TcpData || command == PictureData;
        }

    DEBUG_ENUM(HttpTunnelCommand)
    PRINT_ENUM(HttpTunnelCommand)
    PARSE_ENUM(HttpTunnelCommand)
};
