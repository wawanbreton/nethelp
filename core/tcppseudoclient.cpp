/*! @file   vncproxy.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "tcppseudoclient.h"

#include <QTimer>


TcpPseudoClient::TcpPseudoClient(quint16 port, int socketIndex, QObject *parent) :
    QObject(parent),
    _port(port),
    _socketIndex(socketIndex),
    _socket(new QTcpSocket(this))
{
    connect(_socket, &QTcpSocket::connected, this, &TcpPseudoClient::onSocketConnected);
    connect(_socket,
            static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
            this,
            &TcpPseudoClient::onSocketError);

    _timeStart = QDateTime::currentDateTime();
    connectSocket();
}

void TcpPseudoClient::onSocketConnected()
{
    qInfo() << "Pseudo-client connected to port" << _port << _socketIndex;

    if(!_startBuffer.isEmpty())
    {
        // Send data that has been received before the socket was connected
        onClientData(_startBuffer);
        _startBuffer.clear();
    }

    disconnect(_socket, 0, this, 0);
    connect(_socket, &QTcpSocket::readyRead, this, &TcpPseudoClient::onReadyRead);
}

void TcpPseudoClient::onSocketError(QAbstractSocket::SocketError socketError)
{
    // We let 10 seconds for the server to start, as the port may not be open at the very beginning
    if(socketError == QAbstractSocket::ConnectionRefusedError &&
       _timeStart.secsTo(QDateTime::currentDateTime()) < 10)
    {
        // Retry later
        QTimer::singleShot(200, this, &TcpPseudoClient::connectSocket);
    }
}

void TcpPseudoClient::onReadyRead()
{
    emit serverData(_socket->readAll(), _port, _socketIndex);
}

void TcpPseudoClient::onClientData(const QByteArray &data)
{
    if(_socket->isWritable())
    {
        // Take the data coming from the tunnel and forwards them to the server
        _socket->write(data);
        _socket->flush();
    }
    else
    {
        _startBuffer.append(data);
    }
}
