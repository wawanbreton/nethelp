/*! @file   nethelpclient.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelpendpoint.h"

#include <QFile>
#include <QDir>
#include <QSslConfiguration>

#include "common/path.h"
#include "core/httptunnelclient.h"


NetHelpEndPoint::NetHelpEndPoint(const QString &type,
                                 const QString &serverUrl,
                                 const QNetworkProxy &proxy,
                                 const QString &desiredIdentifier,
                                 const QString &desiredPassword,
                                 const QList<QSslError::SslError> &ignoredSslErrors,
                                 const ClientEndPointParams &params,
                                 QObject *parent) :
    QObject(parent),
    _client(new HttpTunnelClient(type,
                                 serverUrl,
                                 proxy,
                                 params,
                                 desiredIdentifier,
                                 desiredPassword,
                                 ignoredSslErrors,
                                 this))
{
    connect(_client, &HttpTunnelClient::openSessionEnd,   this, &NetHelpEndPoint::onOpenSessionEnd);
    connect(_client, &HttpTunnelClient::connectionLost,   this, &NetHelpEndPoint::connectionLost);
    connect(_client, &HttpTunnelClient::over,             this, &NetHelpEndPoint::over);
    connect(_client, &HttpTunnelClient::controlTaken,     this, &NetHelpEndPoint::controlTaken);
    connect(_client, &HttpTunnelClient::controlReleased,  this, &NetHelpEndPoint::controlReleased);

}

NetHelpEndPoint::~NetHelpEndPoint()
{
    stopSession();
}

void NetHelpEndPoint::startSession()
{
    if(!_client->isConnected())
    {
        _client->tryConnect();
    }
    else
    {
        qWarning() << "Client is already open";
    }
}

void NetHelpEndPoint::stopSession()
{
    if(_client->isConnected())
    {
        _client->disconnect();
    }
}

void NetHelpEndPoint::onOpenSessionEnd(const QString &error)
{
    HttpTunnelClient *client = qobject_cast<HttpTunnelClient *>(sender());
    if(client)
    {
        if(error.isEmpty())
        {
            emit openSessionEnd(client->getIdentifier(), client->getPassword(), "");
        }
        else
        {
            // Unable to establish the connection
            stopSession();

            emit openSessionEnd("", "", error);
            emit over();
        }
    }
    else
    {
        qCritical() << "Sender is not a HttpTunnelClient";
    }

}
