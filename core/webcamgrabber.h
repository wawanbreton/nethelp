/*! @file   webcamgrabber.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QThread>

#include <opencv2/videoio/videoio.hpp>

#include <QMutex>

/*! @brief Class which implements the communication with the inner library to grab still images
           from a webcam in a standard and platform-independant way

    The inner thread is here to ensure that we grab all the images from the webcam, so that when we
    ask for an image, we don't get a previous image stored in any internal buffer. To improve
    performances, we only call the grab() method of the capture object inside the thread, so that
    it only takes the raw data from the device, without decoding them. The data are decoded only
    in the grabImage() method, when asked. However, this means that we have to wait for the last
    grab() to be over before we can start decoding the data, which introduces a latency, but it is
    still better than decoding every frame as we only want a few of them.
 */
class WebcamGrabber : public QThread
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The container object */
        explicit WebcamGrabber(QObject *parent = nullptr);

        /*! @brief Destructor, which asks the thread to stop, waits for it to be finished, and then
                   destroys the inner resources */
        virtual ~WebcamGrabber();

        /*! @brief Slot to be called when an image is to be grabbed from the webcam */
        void grabImage();

    signals:
        /*! @brief Signal emitted when an image has been grabbed from the webcam
            @param data The data containing the JPEG-compressed image */
        void imageGrabbed(const QByteArray &data);

    protected:
        /*! @brief Method called by the Qt engine : it contains the inner thread execution code */
        virtual void run() override;

    private:
        cv::VideoCapture *_capture;
        cv::Mat _rawFrame;
        QMutex _mutex;
        QMutex _mutex2;
        std::vector<uchar> _frameBuf;
};
