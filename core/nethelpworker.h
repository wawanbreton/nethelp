#pragma once

#include <QObject>


class NetHelpEndPoint;

class NetHelpWorker : public QObject
{
    Q_OBJECT

    public:
        /*! @brief Starts the communication session with the server
            @note The function returns no result, but the openSessionEnd signal will be emitted
                  when the connection has been established or if an error occured */
        void startSession();

        /*! @brief Stops the communication session with the server
         *  @note Once this method has been called, wait for the over() signal before destroying
         *        the object so that the communication has time to close properly */
        void stopSession();

        /*! @brief Gets the current session ID
            @return The current session ID, or an empty string if no communication session
                    is running */
        const QString &getSessionId() const { return _sessionId; }

        /*! @brief Gets the current session password
            @return The current session password, or an empty string if no communication session
                    is running */
        const QString &getSessionPassword() const { return _sessionPassword; }

        /*! @brief Indicates whether a communication session is currently running
            @return True if a communication session is currently running */
        bool isSessionRunning() const
        { return !_sessionId.isEmpty(); }

        /*! @brief Indicates if a remote viewer is currently controlling an application
            @return True if a remote viewer is currently controlling an application */
        bool isRemoteControlTaken() const { return _remoteControlTaken; }

    signals:
        /*! @brief Signal emitted when the session opening sequence is over
            @param identifier The identifier affected by the server,
                              or an empty string if an error occured
            @param password The password to be sent to the server to start the remote control
            @param errorMsg The error message if an error occured, or an empty string */
        void openSessionEnd(const QString &identifier,
                            const QString &password,
                            const QString &errorMsg);

        /*! @brief Signal emitted when the connection to the server has been lost
                           without being asked */
        void connectionLost();

        /*! @brief Signal emitted when a viewer has taken the control of the
         *         application (client-side) or when a remote application has been
         *         taken control of (viewer-side) */
        void controlTaken();

        /*! @brief Signal emitted when the viewer has released the control of
                   the application */
        void controlReleased();

        /*! @brief Signal emitted after stopSession() has been called, once all the communication
         *         has been properly shut down */
        void over();

    protected:
        explicit NetHelpWorker(NetHelpEndPoint *endPoint, QObject *parent = nullptr);

        NetHelpEndPoint *accessEndPoint() { return _endPoint; }

    private:
        void onOpenSessionEnd(const QString &identifier,
                              const QString &password,
                              const QString &errorMsg);

        void onTakeControl();

        void onReleaseControl();

    private:
        NetHelpEndPoint *_endPoint{nullptr};
        QString _sessionId;
        QString _sessionPassword;
        bool _remoteControlTaken{false};
};

