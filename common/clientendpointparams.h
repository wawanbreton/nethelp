#pragma once

#include <QMap>
#include <QStringList>
#include <QJsonObject>

#include "common/categorizedfiles.h"
#include "common/path.h"

typedef struct
{
    quint16 actualPort;
    quint16 replacementPort;
    QString name;
} MappedTcpPort;

typedef struct
{
    QString key;
    QString value;
} InformationPiece;

typedef struct
{
    QList<InformationPiece> information;
    QByteArray iconData;
    QString guiDescription;
    QList<CategorizedFiles> interestingFiles;
    QList<MappedTcpPort> tcpPorts;

    QJsonObject save() const;

    void load(const QJsonObject &jsonObject);
} ClientEndPointParams;

Q_DECLARE_METATYPE(ClientEndPointParams);
