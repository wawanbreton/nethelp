/*! @file   messagehandler.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QObject>

#include <QDir>
#include <QTextStream>

/*! @brief This class is used to log message form application */
class MessageHandler : public QObject
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param logDir the directory path where log file are created
            @param parent the parent object */
        MessageHandler(const QString &logDir, QObject *parent = nullptr);

        /*! @brief Destructor */
        ~MessageHandler();

        /*! @brief This slot is used to change the log level displayed and saved in log file. Each
                   time this slot is called, the log level increase (QDebug -> QWarning ->
                   QCritical -> QFatal -> QDebug -> ...)
            @return The new log level */
        QtMsgType changeLogLevel();

    public:
        /*! @brief Adds a message in the log file. When the line is written,
                   it is flushed in the file
            @param message The message to output in the log file
            @param type The message type */
        void addMessage(const QString &message, QtMsgType type);

        /*! @brief This function deletes from log directory files which are a week older */
        void cleanOldLogFile();

        /*! @brief    This is the MsgType logged in log file getter
            @return   The MsgType logged in log file (QDebug, QWarning, QCritical or QFatal) */
        QtMsgType getMsgType() const
        {return _msgType;}

        /*! @brief This is the MsgType logged in log file setter
            @param msgType The MsgType logged in log file (QDebug, QWarning, QCritical or QFatal) */
        void setMsgType(QtMsgType msgType)
        {_msgType = msgType;}

    private:
        /*! @brief This is the handler function called when a message must be logged
            @param type the message type which is logged
            @param context The logged message context
            @param msg the message which have to be logged */
        static void handler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    private:
        /*! @brief This function is used to change the file where logs are written to the
                   current day file

            The function closes the current file and creates a new file if no file already exists
            for the current day, otherwise uses it. */
        void changeFile();

    private:
        static MessageHandler *_instance;

    private :
        QtMsgType _msgType;
        QDir _logDir;
        QFile *_logFile;
        QTextStream _logTextStream;
        QTextStream _stdoutTextStream;
};
