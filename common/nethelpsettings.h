/*! @file   nethelpsettings.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QSettings>

#include <QNetworkProxy>

/*! @brief Subclass of a QSetting to access the configuration easily : just instantiate it to get
           the current configuration, and call the save() method to save any modification
    @note There is no save method, because this is done automatically in the destructor */
class NetHelpSettings : protected QSettings
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit NetHelpSettings(QObject *parent = nullptr) : QSettings(parent) {}

        /*! @brief Gets the configured server host
            @return The configured server host */
        QString getServerHost() const
        { return value("network/server_host", "").toString(); }

        /*! @brief Sets the configured server host
            @param serverHost The configured server host */
        void setServerHost(const QString &serverHost)
        { setValue("network/server_host", serverHost); }

        /*! @brief Gets whether a proxy is used to access the Internet
            @return True if the proxy configuration should be considered, false otherwise */
        bool getProxyActive() const
        { return value("network/proxy_active", false).toBool(); }

        /*! @brief Sets whether a proxy is used to access the Internet
            @param proxyActive True if the proxy configuration should be considered,
                               false otherwise */
        void setProxyActive(bool proxyActive)
        { setValue("network/proxy_active", proxyActive); }

        /*! @brief Gets the configured proxy host
            @return The configured proxy host */
        QString getProxyHost() const
        { return value("network/proxy_host", "").toString(); }

        /*! @brief Sets the configured proxy host
            @param proxyHost The configured proxy host */
        void setProxyHost(const QString &proxyHost)
        { setValue("network/proxy_host", proxyHost); }

        /*! @brief Gets the configured proxy port
            @return The configured proxy port */
        quint16 getProxyPort() const
        { return value("network/proxy_port", 8080).toUInt(); }

        /*! @brief Sets the configured proxy port
            @param proxyPort The configured proxy port */
        void setProxyPort(quint16 proxyPort)
        { setValue("network/proxy_port", proxyPort); }

        /*! @brief Gets whether the proxy requires an authentication
            @return True if the proxy requires an authentication, false otherwise */
        bool getProxyAuth() const
        { return value("network/proxy_auth", false).toBool(); }

        /*! @brief Sets whether the proxy requires an authentication
            @param proxyAuth True if the proxy requires an authentication, false otherwise */
        void setProxyAuth(bool proxyAuth)
        { setValue("network/proxy_auth", proxyAuth); }

        /*! @brief Gets the proxy login
            @return The proxy login */
        QString getProxyLogin() const
        { return value("network/proxy_login", "").toString(); }

        /*! @brief Sets the proxy login
            @param proxyLogin The proxy login */
        void setProxyLogin(const QString &proxyLogin)
        { setValue("network/proxy_login", proxyLogin); }

        /*! @brief Gets the proxy password
            @return The proxy password */
        QString getProxyPassword() const
        { return value("network/proxy_password", "").toString(); }

        /*! @brief Sets the proxy password
            @param proxyPassword The proxy password */
        void setProxyPassword(const QString &proxyPassword)
        { setValue("network/proxy_password", proxyPassword); }

        /*! @brief Gets the globally configured proxy */
        QNetworkProxy getProxy() const;
};
