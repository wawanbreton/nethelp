/*! @file   networkconfigurationwidget.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QWidget>

namespace Ui { class NetworkConfigurationWidget; }

/*! @brief Widget containing the network configuration for a NetHelp client or viewer */
class NetworkConfigurationWidget : public QWidget
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit NetworkConfigurationWidget(QWidget *parent = nullptr);

        /*! @brief Destructor */
        ~NetworkConfigurationWidget();

        /*! @brief Resets the displayed configuration to it's initial value */
        void reset();

        /*! @brief Saves the displayed configuration */
        void save();

    private:
        Ui::NetworkConfigurationWidget *_ui;
};
