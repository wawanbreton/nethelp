/*! @file   nethelpsettings.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelpsettings.h"

#include <QNetworkProxy>

#include "core/nethelpendpoint.h"


QNetworkProxy NetHelpSettings::getProxy() const
{
    QNetworkProxy proxy;
    if(getProxyActive())
    {
        proxy.setType(QNetworkProxy::HttpProxy);
        proxy.setHostName(getProxyHost());
        proxy.setPort(getProxyPort());

        if(getProxyAuth())
        {
            proxy.setUser(getProxyLogin());
            proxy.setPassword(getProxyPassword());
        }
    }
    else
    {
        proxy.setType(QNetworkProxy::NoProxy);
    }

    return proxy;
}
