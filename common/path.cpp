/*! @file   path.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "path.h"

#include <QDir>
#include <QDebug>


Path Path::toNormalized() const
{
    if(isNormalized())
    {
        return Path(*this);
    }
    else
    {
        return Path(_path.split(QDir::separator(), QString::SkipEmptyParts).join("/"), true);
    }
}

Path Path::toLocalized() const
{
    if(!isNormalized())
    {
        return Path(*this);
    }
    else
    {
        QStringList splittedPath = _path.split('/', QString::SkipEmptyParts);

        #ifdef Q_OS_UNIX
        // "home" -> "/home"
        splittedPath.prepend("");
        #else
        if(splittedPath.count() == 1)
        {
            // "C:" -> "C:\"
            splittedPath << "";
        }
        #endif

        return Path(splittedPath.join(QDir::separator()), false);
    }
}

Path Path::subElement(const QString &subDir) const
{
    Path normalized = toNormalized();
    if(normalized.isRoot())
    {
        return Path(subDir, true);
    }
    else
    {
        return Path(toNormalized().getPath() + "/" + subDir, true);
    }
}

Path Path::parentDir() const
{
    if(isRoot())
    {
        return Path();
    }
    else
    {
        Path normalized = toNormalized();
        QStringList splittedPath = normalized.getPath().split('/', QString::SkipEmptyParts);
        return Path(QStringList(splittedPath.mid(0, splittedPath.count() - 1)).join("/"), true);
    }
}

bool Path::isRoot() const
{
    if(isNormalized())
    {
        return getPath().isEmpty();
    }
    else
    {
        #ifdef Q_OS_UNIX
        return getPath() == "/";
        #else
        return getPath().isEmpty();
        #endif
    }
}

QDebug operator<<(QDebug dbg, const Path &path)
{
    dbg.nospace() << "(" << path.getPath() << ", " << (path.isNormalized() ? "normalized" : "localized") << ")";

    return dbg.maybeSpace();
}

QDataStream &operator<<(QDataStream &dataStream, const Path &path)
{
    dataStream << path.toNormalized().getPath();

    return dataStream;
}

QDataStream &operator>>(QDataStream &dataStream, Path &path)
{
    QString rawNormalizedPath;

    dataStream >> rawNormalizedPath;

    path = Path(rawNormalizedPath, true);

    return dataStream;
}
