#pragma once

#include <QString>

namespace HttpStatusCode
{
    typedef enum
    {
        Ok = 200,         // Everything is fine, there may be some payload data
        NoContent = 204,  // Nothing to send yet, please try again
        BadRequest = 400, // Fatal error : request is invalid
        NotFound = 404    // Non-fatal error : request is unknown
    } Enum;

    QString text(Enum code);
}
