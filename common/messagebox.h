/*! @file   messagebox.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QDialog>

#include <QAbstractButton>
#include <QMessageBox>

/*! @brief Represents a button description : the custom label to be displayed, and the standard
           type of button to be used, especially for the displayed icon */
typedef QPair<QString, QMessageBox::StandardButton> ButtonDesc;

/*! @brief Util class to display advanced messages boxes */
class MessageBox
{
    public:
        /*! @brief Displayed a standard message box, with buttons containing custom texts
            @param parent The parent widget
            @param icon The main icon of the message box
            @param message The main message to be displayed
            @param buttons List containing the buttons to be displayed and their custom text */
        template <typename Class, typename Func>
        static void show(Class *receiver,
                         Func slot,
                         QWidget *parent,
                         QMessageBox::Icon icon,
                         const QString &message,
                         const QList<ButtonDesc> &buttons = QList<ButtonDesc>());

        static void show(QWidget *parent, QMessageBox::Icon icon, const QString &message)
        { showMessageBox(parent, icon, message, QList<ButtonDesc>()); }

        static void critical(QWidget *parent, const QString &message)
        { show(parent, QMessageBox::Critical, message); }

    private:
        static QMessageBox *showMessageBox(QWidget *parent,
                                           QMessageBox::Icon icon,
                                           const QString &message,
                                           const QList<ButtonDesc> &buttons);
};

template<typename Class, typename Func>
void MessageBox::show(Class *receiver,
                      Func slot,
                      QWidget *parent,
                      QMessageBox::Icon icon,
                      const QString &message,
                      const QList<ButtonDesc> &buttons)
{


    QObject::connect(showMessageBox(parent, icon, message, buttons),
                     &QMessageBox::finished,
                     receiver,
                     slot);
}
