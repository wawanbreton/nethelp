#include "httpstatuscode.h"


QString HttpStatusCode::text(HttpStatusCode::Enum code)
{
    switch(code)
    {
        case Ok:
            return "OK";
        case BadRequest:
            return "Bad Request";
        case NoContent:
            return "No Content";
        case NotFound:
            return "Not Found";
    }

    return QString();
}
