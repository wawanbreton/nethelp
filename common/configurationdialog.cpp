/*! @file   configurationdialog.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "configurationdialog.h"
#include "ui_configurationdialog.h"

#include <QCloseEvent>
#include <QPushButton>


ConfigurationDialog::ConfigurationDialog(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::ConfigurationWidget)
{
    _ui->setupUi(this);

    connect(_ui->buttonBox, &QDialogButtonBox::clicked,
            this,           &ConfigurationDialog::onButtonClicked);

    reset();

    adjustSize();
}

ConfigurationDialog::~ConfigurationDialog()
{
    delete _ui;
}

void ConfigurationDialog::onButtonClicked(QAbstractButton *button)
{
    QDialogButtonBox::ButtonRole role = _ui->buttonBox->buttonRole(button);

    if(role == QDialogButtonBox::RejectRole)
    {
        reset();
        hide();
    }
    else if(role == QDialogButtonBox::ResetRole)
    {
        reset();
    }
    else if(role == QDialogButtonBox::AcceptRole)
    {
        save();
        hide();
    }
}

void ConfigurationDialog::reset()
{
    _ui->networkConfigWidget->reset();
}

void ConfigurationDialog::save()
{
    _ui->networkConfigWidget->save();
}
