/*! @file   clientendpointparams.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "clientendpointparams.h"

#include <QBuffer>
#include <QDataStream>
#include <QJsonArray>


QJsonObject ClientEndPointParams::save() const
{
    QJsonObject jsonObject;

    QJsonObject objectInformation;
    for(const InformationPiece &info : information)
    {
        objectInformation.insert(info.key, info.value);
    }
    jsonObject.insert("information", objectInformation);

    jsonObject.insert("iconData", QString::fromUtf8(iconData.toBase64()));
    jsonObject.insert("guiDescription", guiDescription);

    QJsonArray arrayInterestingFiles;
    for(const CategorizedFiles &interestingFiles : interestingFiles)
    {
        arrayInterestingFiles.append(interestingFiles.save());
    }
    jsonObject.insert("interestingFiles", arrayInterestingFiles);

    QJsonArray arrayTcpPorts;
    for(const MappedTcpPort &tcpPort : tcpPorts)
    {
        QJsonObject objectTcpPort;
        objectTcpPort["actualPort"] = tcpPort.actualPort;
        objectTcpPort["replacementPort"] = tcpPort.replacementPort;
        objectTcpPort["name"] = tcpPort.name;

        arrayTcpPorts.append(objectTcpPort);
    }
    jsonObject.insert("tcpPorts", arrayTcpPorts);

    return jsonObject;
}

void ClientEndPointParams::load(const QJsonObject &jsonObject)
{
    QJsonObject objectInformation = jsonObject["information"].toObject();
    for(auto iterator = objectInformation.constBegin() ;
        iterator != objectInformation.constEnd() ;
        ++iterator)
    {
        information.append({iterator.key(), iterator.value().toString()});
    }

    iconData = QByteArray::fromBase64(jsonObject["iconData"].toString().toUtf8());
    guiDescription = jsonObject["guiDescription"].toString();

    for(const QJsonValue &interestingFileValue : jsonObject["interestingFiles"].toArray())
    {
        CategorizedFiles files;
        files.load(interestingFileValue.toObject());
        interestingFiles.append(files);
    }

    for(const QJsonValue &valueTcpPort : jsonObject["tcpPorts"].toArray())
    {
        QJsonObject objectTcpPort = valueTcpPort.toObject();

        MappedTcpPort tcpPort;
        tcpPort.actualPort = objectTcpPort.value("actualPort").toInt();
        tcpPort.replacementPort = objectTcpPort.value("replacementPort").toInt();
        tcpPort.name = objectTcpPort.value("name").toString();

        tcpPorts.append(tcpPort);
    }
}
