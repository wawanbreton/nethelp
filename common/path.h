/*! @file   path.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QStringList>
#include <QMetaType>

/*! @brief Represents a path to a file or to a directory.

    The special feature of this class is that the stored path is declared as "normalized" or
    "localized" : as files paths are not managed under UNIX and other OSes the same way, we have
    defined a normalized path which is the same whatever the plaform is. This way, we can share
    paths from a platform to an other, even if the paths are encoded differently. The only thing the
    developper has to care about is to know whether a path is normalized or localized when creating
    one, and the same when displaying/using one.

    Normalized paths are formatted the following way : elements are separated by slashes (which are
    reserved characters under UNIX and Windows) and they don't contain a slash at the beginning
    neither at the end. Exemples are better that words :

    UNIX
    /home/foo/bar.png
    NORMALIZED
    home/foo/bar.png

    Windows
    C:\Documents and Settings\foo\bar.png
    NORMALIZED
    C:/Documents and Settings/foo/bar.png

    Note : the root folder is materialized by an empty path. On UNIX, it means "/" and on Windows,
    it means "The list of drives".
*/
class Path
{
    public:
        /*! @brief Builds a path pointing to the root */
        Path() : _path(), _normalized(true) {}

        /*! @brief Builds a specific path
            @param path The real path, normalized or localized
            @param normalized Indicates whether the given path is in localized or normalized format
        */
        Path(const QString &path, bool normalized) : _path(path), _normalized(normalized) {}

        /*! @brief Builds a path from the list of elements
            @param path The parts of the path
            @sa getParts() */
        Path(const QStringList &path) : _path(path.join("/")), _normalized(true) {}

        /*! @brief Copy constructor
            @param other The other path to copy the parameters from */
        Path(const Path &other) : _path(other._path), _normalized(other._normalized) {}

        /*! @brief Returns a path which is a normalized version of the current path
            @return A path which is a normalized version of the current path. In case the current
                    path is already normalized, we just return a copy. */
        Path toNormalized() const;

        /*! @brief Returns a path which is a localized version of the current path
            @return A path which is a localized version of the current path. In case the current
                    path is already localized, we just return a copy. */
        Path toLocalized() const;

        /*! @brief Returns a path which points to the given sub-element in the current path
            @param subElement The name of the sub-element in the current path, which may be a folder
                              or a file
            @return The absolute path of the sub-element */
        Path subElement(const QString &subElement) const;

        /*! @brief Returns a path which points to the parent directory of the current path
            @return A path which points to the parent directory of the current path. In case the
                    current path is the root, we return a path to the root */
        Path parentDir() const;

        /*! @brief Gets the elements of the path
            @return The elements of the path
            @sa Path(const QStringList &path) */
        QStringList getParts() const
        { return toNormalized().getPath().split('/', QString::SkipEmptyParts); }

        /*! @brief Indicates whether the current path points to the root
            @return True if the current path points to the root, false if it points to anything else
        */
        bool isRoot() const;

        /*! @brief Indicates whether the current path is normalized
            @return True if the current path is normalized, false if it is localized */
        bool isNormalized() const { return _normalized; }

        /*! @brief Gets the raw content of the current path, which may be normalized or localized
            @return The raw content of the current path, which may be normalized or localized */
        const QString &getPath() const { return _path; }

        /*! @brief Equality operator
            @param other The path to be compared to
            @return True if the current path and the given path point to the same element */
        bool operator ==(const Path &other) const
        { return toNormalized().getPath() == other.toNormalized().getPath(); }

        /*! @brief Inequality operator
            @param other The path to be compared to
            @return True if the current path and the given path don't point to the same element */
        bool operator !=(const Path &other) const
        { return toNormalized().getPath() != other.toNormalized().getPath(); }

    private:
        QString _path;
        bool _normalized;
};

/*! @brief Operator used to easily print a path on the debug output
    @param dbg The debug output to print the path to
    @param path The path to be printed
    @return The debug output used to print the path to */
QDebug operator<<(QDebug dbg, const Path &path);

Q_DECLARE_METATYPE(Path)
Q_DECLARE_METATYPE(QList<Path>)
