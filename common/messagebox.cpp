/*! @file   messagebox.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "messagebox.h"


QMessageBox *MessageBox::showMessageBox(QWidget *parent,
                                        QMessageBox::Icon icon,
                                        const QString &message,
                                        const QList<ButtonDesc> &buttons)
{
    QMessageBox::StandardButtons standardButtons = QMessageBox::NoButton;
    if(buttons.isEmpty())
    {
        standardButtons = QMessageBox::Ok;
    }

    QMessageBox *messageBox = new QMessageBox(icon,
                                              QString(),
                                              message,
                                              standardButtons,
                                              parent);

    for(const ButtonDesc &desc : buttons)
    {
        messageBox->addButton(desc.second);
        messageBox->button(desc.second)->setText(desc.first);
    }

    QObject::connect(messageBox, &QMessageBox::finished, messageBox, &QMessageBox::deleteLater);

    messageBox->show();

    return messageBox;
}
