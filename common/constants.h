#pragma once

namespace Constants
{
    // Server-side : maximum duration to read the content of a request.
    // Client-side ! maximum duration to read the answer to a request.
    static constexpr int requestTimeout{20000};

    // Server-side : maximum duration of a listen request. If we have nothing to say after this
    // amount of time, we send a 204 error code and the client is supposed to re-listen ASAP.
    static constexpr int listenNoDataTimeout{60000};

    // Listen timeouts base margin : we consider that if something is longer that this delay, it
    // means that something went wrong
    static constexpr int listenReconnectMargin{5000};

    // Client-side : maximum duration of a listen request. If the server does not answer within this
    // time, we consider the request invalid and issue a new one.
    static constexpr int serverListenAnswerTimeout{listenNoDataTimeout + listenReconnectMargin};

    // Server-side : once a listen request is closed (anyhow), the client has at most this time
    // to issue a new listen request/
    static constexpr int listenReconnectTimeout{2 * listenReconnectMargin};
}
