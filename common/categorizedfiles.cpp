/*! @file   categorizedfiles.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "categorizedfiles.h"

#include <QJsonArray>


QJsonObject CategorizedFiles::save() const
{
    QJsonObject jsonObject;
    jsonObject.insert("category", category);

    QJsonArray arrayFiles;
    for(const Path &file : files)
    {
        arrayFiles.append(file.toNormalized().getPath());
    }
    jsonObject.insert("files", arrayFiles);

    return jsonObject;
}

void CategorizedFiles::load(const QJsonObject &jsonObject)
{
    category = jsonObject["category"].toString();

    files.clear();
    for(const QJsonValue &value : jsonObject["files"].toArray())
    {
        files.append(Path(value.toString(), true));
    }
}
