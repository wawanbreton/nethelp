/*! @file   configurationdialog.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QDialog>

#include <QAbstractButton>
#include <QUrl>
#include <QNetworkProxy>

namespace Ui { class ConfigurationWidget; }

/*! @brief Dialog used to configure a NetHelp client in order to make it work correctly */
class ConfigurationDialog : public QDialog
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit ConfigurationDialog(QWidget *parent = nullptr);

        /*! @brief Destructor, which also destroys the inner Ui object */
        ~ConfigurationDialog();

    protected:
        /*! @brief Method called by the Qt engine when the window is closed */
        virtual void closeEvent(QCloseEvent *) override
        { reset(); }

    private:
        /*! @brief Slot called when a button of the bottom-bar is clicked
            @param button The button which has been pressed */
        void onButtonClicked(QAbstractButton *button);

        /*! @brief Resets the displayed configuration to it's initial value */
        void reset();

        /*! @brief Saves the displayed configuration */
        void save();

    private:
        Ui::ConfigurationWidget *_ui;
};
