/*! @file   messagehandler.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "messagehandler.h"

#include <QTimer>
#include <QDebug>
#include <QDateTime>


MessageHandler *MessageHandler::_instance = nullptr;

MessageHandler::MessageHandler(const QString &logDir, QObject *parent) :
    QObject(parent),
    _msgType(QtWarningMsg),
    _logDir(logDir),
    _logFile(nullptr),
    _logTextStream(),
    _stdoutTextStream(stdout)
{
    if(_instance)
    {
        qWarning() << "There was already a MessageHandler instance";
    }
    _instance = this;


    qInstallMessageHandler(handler);

    changeFile();
}

MessageHandler::~MessageHandler()
{
    if(_logFile)
    {
        delete _logFile;
        _logFile = nullptr;
    }

    if(_instance == this)
    {
        _instance = nullptr;
        qInstallMessageHandler(0);
    }
}

QtMsgType MessageHandler::changeLogLevel()
{
    QtMsgType oldMsgType = _msgType;

    if(oldMsgType == QtFatalMsg)
    {
        _msgType = QtDebugMsg;
    }
    else
    {
        _msgType = QtMsgType(oldMsgType + 1);
    }


    qDebug() << "Change log level from" << oldMsgType << "to" << _msgType;

    return _msgType;
}

void MessageHandler::handler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    if(_instance)
    {
        QString methodName = QString::fromUtf8(context.function);
        methodName = methodName.mid(0, methodName.indexOf('('));
        methodName = methodName.mid(methodName.lastIndexOf(' ') + 1);

        _instance->addMessage(QString("%1 %2").arg(methodName).arg(msg), type);
    }
}

void MessageHandler::addMessage(const QString &message, QtMsgType type)
{
    QString typeStr;
    switch(type)
    {
        case QtDebugMsg:
            typeStr = "[DEBUG]   ";
            break;
        case QtInfoMsg:
            typeStr = "[INFO]    ";
            break;
        case QtWarningMsg:
            typeStr = "[WARNING] ";
            break;
        case QtCriticalMsg:
            typeStr = "[CRITICAL]";
            break;
        case QtFatalMsg:
            typeStr = "[FATAL]   ";
            break;
    }

    QString dateStr = QDateTime::currentDateTime().toString("hh:mm:ss:zzz");
    QString fullMessage = QString("%1 %2 %3").arg(dateStr).arg(typeStr).arg(message);

    // If the message is important enough, print it
    if(type >= _msgType)
    {
        _logTextStream << fullMessage << endl;
        _logTextStream.flush();

        _stdoutTextStream << fullMessage << endl;
    }
}

void MessageHandler::cleanOldLogFile()
{
    if(_logDir.exists())
    {
        for(const QString &fileName : _logDir.entryList(QStringList("logs_*.log")))
        {
            QString fileNameDate = fileName;
            fileNameDate.remove("logs_").remove(".log");

            QStringList date = fileNameDate.split("-");

            if(date.size() != 4)
            {
                qWarning() << "Error when split date : wrong parameter number :" << date.size();
                return;
            }

            QDate ref = QDate::currentDate();
            ref = ref.addDays(-7);

            QDate fileDate(date.at(0).toInt(), date.at(1).toInt(), date.at(2).toInt());

            if(fileDate <= ref)
            {
                qInfo() << fileName << "removed !";

               _logDir.remove(fileName);
            }
        }
    }
    else
    {
        qWarning() << "Error when looking for log directory : no such directory";
    }
}

void MessageHandler::changeFile()
{
    if(_logDir.exists())
    {
        if(_logFile)
        {
            delete _logFile;
        }
        QString dateString = QDateTime::currentDateTime().toString("yyyy-MM-dd-dddd");
        _logFile = new QFile(QString("%1/logs_%2.log").arg(_logDir.path()).arg(dateString));

        if(_logFile->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
        {
            _logTextStream.setDevice(_logFile);
        }
        else
        {
            qWarning() << "Error when openning the log file : " << _logFile->errorString();
            delete _logFile;
            _logFile = nullptr;
            _logTextStream.setDevice(nullptr);
        }
    }
    else
    {
        qWarning() << "Error when looking for log directory : no such directory";
    }

    // Set the next change log file rotation
    QDateTime currentTime = QDateTime::currentDateTime();
    QDate today = QDate::currentDate();

    int delay = 1000 * currentTime.secsTo(QDateTime(today.addDays(1), QTime(0 ,0 ,0)));
    QTimer::singleShot(delay, this, &MessageHandler::changeFile);

    cleanOldLogFile();
}
