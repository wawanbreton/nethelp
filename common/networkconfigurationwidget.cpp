/*! @file   networkconfigurationwidget.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "networkconfigurationwidget.h"
#include "ui_networkconfigurationwidget.h"

#include "common/nethelpsettings.h"


NetworkConfigurationWidget::NetworkConfigurationWidget(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::NetworkConfigurationWidget)
{
    _ui->setupUi(this);
}

NetworkConfigurationWidget::~NetworkConfigurationWidget()
{
    delete _ui;
}

void NetworkConfigurationWidget::reset()
{
    NetHelpSettings settings;

    _ui->lineEditServer->setText(settings.getServerHost());

    _ui->radioButtonProxy->setChecked(settings.getProxyActive());
    _ui->lineEditProxy->setText(settings.getProxyHost());
    _ui->spinBoxProxyPort->setValue(settings.getProxyPort());
    _ui->checkBoxAuthRequired->setChecked(settings.getProxyAuth());
    _ui->lineEditLogin->setText(settings.getProxyLogin());
    _ui->lineEditPassword->setText(settings.getProxyPassword());
}

void NetworkConfigurationWidget::save()
{
    NetHelpSettings settings;

    settings.setServerHost(_ui->lineEditServer->text());

    settings.setProxyActive(_ui->radioButtonProxy->isChecked());
    settings.setProxyHost(_ui->lineEditProxy->text());
    settings.setProxyPort(_ui->spinBoxProxyPort->value());
    settings.setProxyAuth(_ui->checkBoxAuthRequired->isChecked());
    settings.setProxyLogin(_ui->lineEditLogin->text());
    settings.setProxyPassword(_ui->lineEditPassword->text());
}
