QT += core gui widgets

TARGET = client
TEMPLATE = app

NETHELP_ROOT = ..
NETHELP_COMMON = $${NETHELP_ROOT}/common
NETHELP_CLIENT = $${NETHELP_ROOT}/client

INCLUDEPATH += $${NETHELP_ROOT}

win32 {
  RC_FILE = client.rc
  OTHER_FILES += client.rc
}

NETHELP_CORE_PACKAGES = camera tcp endpoint-client
include($${NETHELP_ROOT}/core/core.pri)

include($${NETHELP_COMMON}/common.pri)

SOURCES *= $${NETHELP_CLIENT}/main.cpp
SOURCES *= $${NETHELP_CLIENT}/nethelptrayicon.cpp
SOURCES *= $${NETHELP_COMMON}/configurationdialog.cpp
SOURCES *= $${NETHELP_COMMON}/messagehandler.cpp
SOURCES *= $${NETHELP_COMMON}/nethelpsettings.cpp
SOURCES *= $${NETHELP_COMMON}/networkconfigurationwidget.cpp

HEADERS *= $${NETHELP_CLIENT}/nethelptrayicon.h
HEADERS *= $${NETHELP_COMMON}/configurationdialog.h
HEADERS *= $${NETHELP_COMMON}/messagehandler.h
HEADERS *= $${NETHELP_COMMON}/nethelpsettings.h
HEADERS *= $${NETHELP_COMMON}/networkconfigurationwidget.h

FORMS *= $${NETHELP_COMMON}/configurationdialog.ui
FORMS *= $${NETHELP_COMMON}/networkconfigurationwidget.ui

RESOURCES *= $${NETHELP_COMMON}/icons.qrc
