/*! @file   nethelptrayicon.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelptrayicon.h"

#include <QApplication>
#include <QSettings>
#include <QProcess>
#include <QFile>
#include <QHostInfo>
#include <QTimer>

#include "common/configurationdialog.h"
#include "common/nethelpsettings.h"
#include "core/nethelpworkerclient.h"


NetHelpTrayIcon::NetHelpTrayIcon(QObject *parent) :
    QSystemTrayIcon(parent),
    _menu(new QMenu()),
    _configWidget(new ConfigurationDialog())
{
    updateIconAndMenu();

    setContextMenu(_menu);
}

NetHelpTrayIcon::~NetHelpTrayIcon()
{
    delete _menu;
    delete _configWidget;

    stopVncServerIfNeeded();
}

void NetHelpTrayIcon::onConfiguration()
{
    _configWidget->show();
    _configWidget->raise();
    _configWidget->activateWindow();
}

void NetHelpTrayIcon::onClose()
{
    if(_client && _client->isSessionRunning())
    {
        connect(_client, &NetHelpWorkerClient::over,
                this,    []() { QApplication::exit(0); });
        _client->stopSession();

        // In case the goodbye message can't be sent properly
        QTimer::singleShot(1000, this, []() { QApplication::exit(0); });
    }
    else
    {
        QApplication::exit(0);
    }
}

void NetHelpTrayIcon::onStartSession()
{
    NetHelpSettings settings;

    ClientEndPointParams params;
    params.information = computeClientInformation();

    QFile file(":/icons/computer_32.png");
    file.open(QIODevice::ReadOnly);
    params.iconData = file.readAll();

    params.tcpPorts.append({5900, 5901, "vnc"});
    #ifdef Q_OS_LINUX
    params.tcpPorts.append({22, 2222, "ssh"});
    #endif

    _client = new NetHelpWorkerClient(settings.getServerHost(),
                                      settings.getProxy(),
                                      QHostInfo::localHostName(),
                                      QString(),
                                      QList<QSslError::SslError>(),
                                      params,
                                      this);

    connect(_client, &NetHelpWorkerClient::openSessionEnd,
            this,    &NetHelpTrayIcon::onStartSessionEnd);
    connect(_client, &NetHelpWorkerClient::controlTaken,
            this,    &NetHelpTrayIcon::onRemoteControlTaken);
    connect(_client, &NetHelpWorkerClient::controlReleased,
            this,    &NetHelpTrayIcon::onControlReleased);
    connect(_client, &NetHelpWorkerClient::connectionLost,
            this,    &NetHelpTrayIcon::onConnectionLost);
    connect(_client, &NetHelpWorkerClient::over,
            this,    &NetHelpTrayIcon::onClientOver);

    _client->startSession();
}

void NetHelpTrayIcon::onStartSessionEnd(const QString &identifier,
                                        const QString &password,
                                        const QString &errorMsg)
{
    if(errorMsg.isEmpty())
    {
        showMessage(tr("Session successfully opened"),
                    tr("Identifier : %1\nPassword: %2").arg(identifier).arg(password));
    }
    else
    {
        showMessage(tr("Unable to open session"), errorMsg, QSystemTrayIcon::Critical);
    }

    updateIconAndMenu();
}

void NetHelpTrayIcon::onStopSession()
{
    _client->stopSession();

    stopVncServerIfNeeded();
}

void NetHelpTrayIcon::onRemoteControlTaken()
{
    stopVncServerIfNeeded();

    startVncServerIfNeeded();

    showMessage(tr("Active session"),
                tr("A remote viewer has taken the control of your desktop"));

    updateIconAndMenu();
}

void NetHelpTrayIcon::onControlReleased()
{
    onStopSession();
    showMessage(tr("End of session"), tr("The remote viewer has ended the control session"));
}

void NetHelpTrayIcon::updateIconAndMenu()
{
    _menu->clear();
    QPixmap pixmapIcon(":/icons/icon_64.png");
    if(_client && _client->isSessionRunning())
    {
        setIcon(pixmapIcon);

        QString sessionId = _client->getSessionId();
        QString password = _client->getSessionPassword();

        _menu->addAction(QIcon(":/icons/config_64.png"),
                         tr("Configuration ..."),
                         this,
                         &NetHelpTrayIcon::onConfiguration);
        _menu->addAction(QIcon(":/icons/stop_64.png"),
                         tr("End remote help session %1").arg(sessionId),
                         this,
                         &NetHelpTrayIcon::onStopSession);

        if(_client->isRemoteControlTaken())
        {
            setToolTip(tr("NetHelp client : active remote control session"));
        }
        else
        {
            setToolTip(tr("NetHelp client : waiting for remote viewer to take the control") + "\n" +
                       tr("Identifier : %1\nPassword: %2").arg(sessionId).arg(password));
        }
    }
    else
    {
        setIcon(QIcon(pixmapIcon).pixmap(pixmapIcon.size(), QIcon::Disabled));

        _menu->addAction(QIcon(":/icons/config_64.png"), tr("Configuration ..."),
                         this, &NetHelpTrayIcon::onConfiguration);
        _menu->addAction(QIcon(":/icons/play_64.png"), tr("Start remote help session"),
                         this, &NetHelpTrayIcon::onStartSession);

        setToolTip(tr("NetHelp client : inactive"));
    }

    _menu->addAction(QIcon(":/icons/exit_64.png"), tr("Close"), this, &NetHelpTrayIcon::onClose);
}

bool NetHelpTrayIcon::startVncServerIfNeeded()
{
    if(!_vncServerProcess)
    {
        _vncServerProcess = new QProcess(this);
        #ifdef Q_OS_LINUX
        _vncServerProcess->start("x11vnc", QStringList() << "-listen" << "localhost" << "-xdamage");
        #endif
        #ifdef Q_OS_WIN
        QString path = "G:\\Program Files\\TightVNC";
        _vncServerProcess->setWorkingDirectory(path);
        _vncServerProcess->start("\"" + path + "/tvnserver.exe" + "\"");
        #endif
        if(_vncServerProcess->waitForStarted(5000))
        {
            connect(_vncServerProcess,
                    static_cast<void (QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
                    this,
                    &NetHelpTrayIcon::onVncProcessError);
        }
        else
        {
            showMessage(tr("Error"),
                        tr("Unable to start VNC server : %1").arg(_vncServerProcess->errorString()),
                        QSystemTrayIcon::Critical);

            delete _vncServerProcess;
            _vncServerProcess = nullptr;

            return false;
        }
    }

    return true;
}

void NetHelpTrayIcon::stopVncServerIfNeeded()
{
    if(_vncServerProcess)
    {
        if(_vncServerProcess->state() == QProcess::Running)
        {
            disconnect(_vncServerProcess, static_cast<void (QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
                       this,              &NetHelpTrayIcon::onVncProcessError);
            //_vncServerProcess->close();
            _vncServerProcess->kill();
            if(_vncServerProcess->state() == QProcess::Running &&
               !_vncServerProcess->waitForFinished(5000))
            {
                qWarning() << "Could not wait for VNC server to stop properly";
            }
        }

        _vncServerProcess->deleteLater();
        _vncServerProcess = nullptr;
    }
}

QList<InformationPiece> NetHelpTrayIcon::computeClientInformation()
{
    QList<InformationPiece> result;

    QString version;

    #if defined Q_OS_WIN32 || defined Q_OS_WINCE
    switch(QSysInfo::windowsVersion())
    {
        case QSysInfo::WV_32s:
            version = "Windows 3.1";
            break;
        case QSysInfo::WV_95:
            version = "Windows 95";
            break;
        case QSysInfo::WV_98:
            version = "Windows 98";
            break;
        case QSysInfo::WV_Me:
            version = "Windows Me";
            break;
        case QSysInfo::WV_NT:
            version = "Windows NT";
            break;
        case QSysInfo::WV_2000:
            version = "Windows 2000";
            break;
        case QSysInfo::WV_XP:
            version = "Windows XP";
            break;
        case QSysInfo::WV_2003:
            version = "Windows Server 2003";
            break;
        case QSysInfo::WV_VISTA:
            version = "Windows Vista";
            break;
        case QSysInfo::WV_WINDOWS7:
            version = "Windows 7";
            break;
        case QSysInfo::WV_CE:
            version = "Windows CE";
            break;
        case QSysInfo::WV_CENET:
            version = "Windows CE .NET";
            break;
        case QSysInfo::WV_CE_5:
            version = "Windows CE 5.x";
            break;
        case QSysInfo::WV_CE_6:
            version = "Windows CE 6.x";
            break;
        default:
            version = "Windows ?";
            break;
    }
    #endif

    #ifdef Q_OS_LINUX
    QString linuxVersion = readProcessLine("uname", QStringList() << "--all");
    result.append({"Kernel version", linuxVersion});

    version = readProcessLine("lsb_release", QStringList() << "-i").split('\t')[1];
    version += " " + readProcessLine("lsb_release", QStringList() << "-r").split('\t')[1];
    version += " " + readProcessLine("lsb_release", QStringList() << "-c").split('\t')[1];
    #endif

    #ifdef Q_OS_MAC
    switch(QSysInfo::MacintoshVersion)
    {
        case QSysInfo::MV_10_3:
            version = "Max OS X 10.3";
            break;
        case QSysInfo::MV_10_4:
            version = "Max OS X 10.4";
            break;
        case QSysInfo::MV_10_5:
            version = "Max OS X 10.5";
            break;
        case QSysInfo::MV_10_6:
            version = "Max OS X 10.6";
            break;
        default:
            version = "Mac OS X ?";
            break;
    }
    #endif

    result.append({"System version", version});

    return result;
}

QString NetHelpTrayIcon::readProcessLine(const QString &command, const QStringList &args)
{
    _processError = (QProcess::ProcessError)-1;

    QProcess process;
    connect(&process, static_cast<void (QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
            this,     &NetHelpTrayIcon::onVncProcessError);
    process.start(command, args);
    process.waitForFinished(-1);

    if(_processError >= QProcess::FailedToStart)
    {
        qWarning() << "Error" << _processError << "when running program" << command << args
                   << ":" << process.errorString();
        return "";
    }

    return QString(process.readLine()).remove('\n');
}

void NetHelpTrayIcon::onVncProcessError()
{
    if(_vncServerProcess)
    {
        showMessage(tr("Error"),
                    tr("VNC server error : %1").arg(_vncServerProcess->errorString()),
                    QSystemTrayIcon::Critical);

        qWarning() << "VNC process error :" << _vncServerProcess->errorString();

        _vncServerProcess->deleteLater();
        _vncServerProcess = nullptr;
    }
    else
    {
        qCritical() << "There is no running VNC server process ?!";
    }
}

void NetHelpTrayIcon::onConnectionLost()
{
    showMessage(tr("Error"),
                tr("The connection with the server has been lost"),
                QSystemTrayIcon::Critical);
}

void NetHelpTrayIcon::onClientOver()
{
    stopVncServerIfNeeded();

    _client->deleteLater();
    _client = nullptr;

    updateIconAndMenu();
}
