/*! @file   client/main.cpp
    @brief  This file is the entry point of the NetHelp client application
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include <QApplication>

#include <QDebug>

#include "client/nethelptrayicon.h"
#include "common/messagehandler.h"
#include "common/path.h"


/*! @brief NetHelp client entry point
    @param argc The number of given arguments
    @param argv The list of given arguments
    @return The program exit code */
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setOrganizationName("NetHelp");
    app.setOrganizationDomain("sourceforge.net/projects/nethelp");
    app.setApplicationName("NetHelpClient");
    app.setApplicationVersion("0.1");

    qRegisterMetaType<Path>();

    MessageHandler handler(".");
    handler.setMsgType(QtDebugMsg);

    NetHelpTrayIcon trayIcon;
    trayIcon.show();

    app.setQuitOnLastWindowClosed(false);

    return app.exec();
}
