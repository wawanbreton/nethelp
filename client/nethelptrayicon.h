/*! @file   nethelptrayicon.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QSystemTrayIcon>

#include <QMenu>
#include <QProcess>

#include "common/clientendpointparams.h"

class NetHelpWorkerClient;

/*! @brief This class implements the behavior of the NetHelp system tray icon. Actually, it contains
           the biggest part of the application execution code, which is not very clean, but very
           convenient. */
class NetHelpTrayIcon : public QSystemTrayIcon
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit NetHelpTrayIcon(QObject *parent = nullptr);

        /*! @brief Destructor */
        virtual ~NetHelpTrayIcon();

    private:
        /*! @brief Slot called when the configuration action is triggered */
        void onConfiguration();

        /*! @brief Slot called when the close action is triggered */
        void onClose();

        /*! @brief Slot called when the start session action is triggered */
        void onStartSession();

        /*! @brief Slot called when the session opening sequency is over
            @param identifier The server-assigned identifier, or an empty string if an error occured
            @param password The password to be sent to the server to start the remote control
            @param errorMsg The error message, or en empty string if everything went well */
        void onStartSessionEnd(const QString &identifier,
                               const QString &password,
                               const QString &errorMsg);

        /*! @brief Slot called when the stop session action is triggered */
        void onStopSession();

        /*! @brief Slot called when a remote viewer has taken the control of the application */
        void onRemoteControlTaken();

        /*! @brief Slot called when the remote viewer has released the control of the application */
        void onControlReleased();

        /*! @brief Starts the local VNC server, if not started yet
            @return True if the server could be started, false otherwise. In this case, a message
                    will be shown over the system tray icon. */
        bool startVncServerIfNeeded();

        /*! @brief Stops the local VNC server, if running */
        void stopVncServerIfNeeded();

        /*! @brief Computes the local system version string as textual information
            @note The informations are sent in untranslated english, because the remote technician
                  is supposed to read them */
        QList<InformationPiece> computeClientInformation();

        /*! @brief Start an system command with the given arguments and read it's output
            @param command The system command to be started
            @param args Arguments to be given to the command
            @return The command output, or an empty string in case of error */
        QString readProcessLine(const QString &command, const QStringList &args);

        /*! @brief Updates the displayed icon and the interactive menu according to the current
                   application status */
        void updateIconAndMenu();

        /*! @brief Slot called when an error occurs on a process started with readProcessLine
            @param error The process error which just occured
            @sa NetHelpTrayIcon::readProcessLine */
        void onProcessError(QProcess::ProcessError error)
        { _processError = error; }

        /*! @brief Slot called when an error occurs with the VNC server process */
        void onVncProcessError();

        /*! @brief Slot called when the connection to the server has been lost
                   without being asked */
        void onConnectionLost();

        void onClientOver();

    private:
        QMenu *_menu{nullptr};
        QWidget *_configWidget{nullptr};
        NetHelpWorkerClient *_client{nullptr};
        QProcess *_vncServerProcess{nullptr};
        QProcess::ProcessError _processError;
};
