QT       += core gui network xml widgets

TARGET = viewer
TEMPLATE = app

NETHELP_ROOT = ..
NETHELP_COMMON = $${NETHELP_ROOT}/common
NETHELP_VIEWER = $${NETHELP_ROOT}/viewer

INCLUDEPATH += $${NETHELP_ROOT}

LIBS += -lvncclient

# Special define for krdc
DEFINES += QTONLY

win32 {
  # Executable icon
  RC_FILE = viewer.rc
  OTHER_FILES += viewer.rc
}

NETHELP_CORE_PACKAGES = tcp endpoint-viewer
include($${NETHELP_ROOT}/core/core.pri)

include($${NETHELP_COMMON}/common.pri)

SOURCES *= $${NETHELP_COMMON}/categorizedfiles.cpp
SOURCES *= $${NETHELP_COMMON}/messagebox.cpp
SOURCES *= $${NETHELP_COMMON}/messagehandler.cpp
SOURCES *= $${NETHELP_COMMON}/nethelpsettings.cpp
SOURCES *= $${NETHELP_COMMON}/networkconfigurationwidget.cpp
SOURCES *= $${NETHELP_VIEWER}/browseremotefilesystemdialog.cpp
SOURCES *= $${NETHELP_VIEWER}/clientitemdelegate.cpp
SOURCES *= $${NETHELP_VIEWER}/clientsmanager.cpp
SOURCES *= $${NETHELP_VIEWER}/configuregroupdialog.cpp
SOURCES *= $${NETHELP_VIEWER}/filterclientsproxymodel.cpp
SOURCES *= $${NETHELP_VIEWER}/groupsmanager.cpp
SOURCES *= $${NETHELP_VIEWER}/krdc/remoteview.cpp
SOURCES *= $${NETHELP_VIEWER}/krdc/vncclientthread.cpp
SOURCES *= $${NETHELP_VIEWER}/krdc/vncview.cpp
SOURCES *= $${NETHELP_VIEWER}/main.cpp
SOURCES *= $${NETHELP_VIEWER}/mainwindow.cpp
SOURCES *= $${NETHELP_VIEWER}/noexpanditemstyle.cpp
SOURCES *= $${NETHELP_VIEWER}/remotecontrolwindow.cpp
SOURCES *= $${NETHELP_VIEWER}/scaledpixmap.cpp
SOURCES *= $${NETHELP_VIEWER}/signaldroptreeview.cpp
SOURCES *= $${NETHELP_VIEWER}/viewersettings.cpp
SOURCES *= $${NETHELP_VIEWER}/viewerconfigurationdialog.cpp

HEADERS *= $${NETHELP_COMMON}/categorizedfiles.h
HEADERS *= $${NETHELP_COMMON}/messagebox.h
HEADERS *= $${NETHELP_COMMON}/messagehandler.h
HEADERS *= $${NETHELP_COMMON}/nethelpsettings.h
HEADERS *= $${NETHELP_COMMON}/networkconfigurationwidget.h
HEADERS *= $${NETHELP_VIEWER}/browseremotefilesystemdialog.h
HEADERS *= $${NETHELP_VIEWER}/clientitemdelegate.h
HEADERS *= $${NETHELP_VIEWER}/clientsmanager.h
HEADERS *= $${NETHELP_VIEWER}/configuregroupdialog.h
HEADERS *= $${NETHELP_VIEWER}/filterclientsproxymodel.h
HEADERS *= $${NETHELP_VIEWER}/groupsmanager.h
HEADERS *= $${NETHELP_VIEWER}/krdc/remoteview.h
HEADERS *= $${NETHELP_VIEWER}/krdc/vncclientthread.h
HEADERS *= $${NETHELP_VIEWER}/krdc/vncview.h
HEADERS *= $${NETHELP_VIEWER}/mainwindow.h
HEADERS *= $${NETHELP_VIEWER}/noexpanditemstyle.h
HEADERS *= $${NETHELP_VIEWER}/remotecontrolwindow.h
HEADERS *= $${NETHELP_VIEWER}/scaledpixmap.h
HEADERS *= $${NETHELP_VIEWER}/signaldroptreeview.h
HEADERS *= $${NETHELP_VIEWER}/viewersettings.h
HEADERS *= $${NETHELP_VIEWER}/viewerconfigurationdialog.h

FORMS *= $${NETHELP_COMMON}/networkconfigurationwidget.ui
FORMS *= $${NETHELP_VIEWER}/browseremotefilesystemdialog.ui
FORMS *= $${NETHELP_VIEWER}/configuregroupdialog.ui
FORMS *= $${NETHELP_VIEWER}/mainwindow.ui
FORMS *= $${NETHELP_VIEWER}/remotecontrolwindow.ui
FORMS *= $${NETHELP_VIEWER}/viewerconfigurationdialog.ui

RESOURCES *= $${NETHELP_COMMON}/icons.qrc
