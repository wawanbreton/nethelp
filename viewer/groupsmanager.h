/*! @file   groupsmanager.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QStringList>
#include <QIcon>
#include <QMap>
#include <QSet>

typedef struct
{
    QString name;
    QIcon icon;
    QSet<QString> members;
} Group;

/*! @brief Manager of the groups hierarchy. The Group structure is never exposed externally :
           to access the data of a group, you must use the getter methods, giving the group ID,
           which is actually the index of the groups in the inner list, so it remains valid until
           a modification on the list is made -> be warned ! */
class GroupsManager
{
    public:
        /*! @brief Constructor */
        GroupsManager();

        /*! @brief Adds a new group with the default configuration
            @return The ID of the groups which has been added
            @note The save() method is called automatically once the group is added */
        int addGroup();

        /*! @brief Removes the given group
            @param group The ID of the group to be removed
            @note The save() method is called automatically once the group is removed */
        void removeGroup(int group);

        /*! @brief Gets the group containing the given client
            @param member The client which may be contained in a group
            @return The ID of the group containing the given client, or -1 if there is none */
        int groupFromMember(const QString &member) const;

        /*! @brief Gets the name of the given group
            @param group The ID of the group which name is required
            @return The name of the given group */
        const QString &getGroupName(int group) const
        { return _groups[group]->name; }

        /*! @brief Sets the name of the given group
            @param group The ID of the group which name is to be set
            @param name The new name of the group */
        void setGroupName(int group, const QString &name)
        { _groups[group]->name = name; }

        /*! @brief Gets the icon of the given group
            @param group The ID of the group which icon is required
            @return The icon of the given group, which may be Null */
        const QIcon &getGroupIcon(int group) const
        { return _groups[group]->icon; }

        /*! @brief Sets the icon of the given group
            @param group The ID of the group which icon is to be set
            @param icon The new icon of the group */
        void setGroupIcon(int group, const QIcon &icon)
        { _groups[group]->icon = icon; }

        /*! @brief Gets the list of clients associated to the group
            @param group The ID of the group which members are required
            @return The list of clients associated to the group */
        QStringList getGroupMembers(int group) const
        { return _groups[group]->members.toList(); }

        /*! @brief Sets the list of clients associated to the group
            @param group The ID of the group which members are to be set
            @param members The new list of associated clients */
        void setGroupMembers(int group, const QStringList &members)
        { _groups[group]->members = QSet<QString>::fromList(members); }

        /*! @brief Gets the count of associated clients of the given group
            @param group The group which members count is required
            @return The number of associated clients of the given group */
        int groupMembersCount(int group)
        { return _groups[group]->members.count(); }

        /*! @brief Gets the current groups count
            @return The current groups count */
        int count() const { return _groups.count(); }

        /*! @brief Saves the groups configuration to the file */
        void save() const;

    private:
        QList<Group *> _groups;
};
