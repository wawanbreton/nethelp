/*! @file   clientsmanager.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "clientsmanager.h"

#include <QDebug>


ClientsManager::ClientsManager(QObject *parent) :
    QObject(parent)
{
}

ClientEndPointParams ClientsManager::getParams(const QString &identifier) const
{
    return _clients.value(identifier).params;
}

void ClientsManager::onClientsListUpdate(const QList<StringBoolPair> &clients)
{
    QList<QString> notFoundClients = _clients.keys();

    // Loop the received list to find the new clients and update the controlled status of existing
    for(StringBoolPair client : clients)
    {
        auto iterator = _clients.find(client.first);

        if(iterator != _clients.end())
        {
            // The client is still present, check whether its controlled status has changed
            if(client.second != iterator.value().remoteControlled)
            {
                iterator.value().remoteControlled = client.second;
                emit clientControlChanged(iterator.key(), client.second);
            }

            notFoundClients.removeAll(iterator.key());
        }
        else
        {
            // This is a new client, hello !
            _clients.insert(client.first, {client.second, ClientEndPointParams()});
            emit newClient(client.first, client.second);
        }
    }

    // Now, remove clients which are no more present
    for(QString notFoundClient : notFoundClients)
    {
        _clients.remove(notFoundClient);
        emit clientRemoved(notFoundClient);
    }
}

void ClientsManager::onClientParamsReceived(const QString &identifier,
                                            const ClientEndPointParams &params)
{
    auto iterator = _clients.find(identifier);
    if(iterator != _clients.end())
    {
        iterator.value().params = params;
    }
    else
    {
        qWarning() << "Unknown identifier" << identifier;
    }
}
