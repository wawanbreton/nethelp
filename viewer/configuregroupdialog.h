/*! @file   configuregroupdialog.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QDialog>

namespace Ui { class ConfigureGroupDialog; }

/*! @brief Dialog used to configure a group : its name, icon and custom notes */
class ConfigureGroupDialog : public QDialog
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit ConfigureGroupDialog(QWidget *parent = nullptr);

        /*! @brief Destructor, which also destroys the inner Ui object */
        ~ConfigureGroupDialog();

        /*! @brief Gets the entered name of the group
            @return The entered name of the group */
        QString getName() const;

        /*! @brief Sets the entered name of the group
            @param name The entered name of the group */
        void setName(const QString &name);

        /*! @brief Gets the entered icon of the group
            @return The entered icon of the group */
        QIcon getIcon() const;

        /*! @brief Sets the entered icon of the group
            @param icon The entered icon of the group */
        void setIcon(const QIcon& icon);

    private:
        /*! @brief Slot called when the icon button is clicked */
        void onButtonIconClicked();

    private:
        Ui::ConfigureGroupDialog *_ui;
};
