/*! @file   configurationdialog.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "viewerconfigurationdialog.h"
#include "ui_viewerconfigurationdialog.h"

#include <QCloseEvent>
#include <QPushButton>
#include <QDir>
#include <QFileDialog>

#include "viewer/viewersettings.h"


ViewerConfigurationDialog::ViewerConfigurationDialog(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::ConfigurationWidget)
{
    _ui->setupUi(this);

    connect(_ui->buttonBox,    &QDialogButtonBox::clicked,
            this,              &ViewerConfigurationDialog::onButtonClicked);
    connect(_ui->buttonBrowse, &QPushButton::clicked,
            this,              &ViewerConfigurationDialog::onButtonBrowseClicked);

    reset();

    adjustSize();
}

ViewerConfigurationDialog::~ViewerConfigurationDialog()
{
    delete _ui;
}

void ViewerConfigurationDialog::onButtonClicked(QAbstractButton *button)
{
    QDialogButtonBox::ButtonRole role = _ui->buttonBox->buttonRole(button);

    if(role == QDialogButtonBox::RejectRole)
    {
        reject();
    }
    else if(role == QDialogButtonBox::ResetRole)
    {
        reset();
    }
    else if(role == QDialogButtonBox::AcceptRole)
    {
        save();
        accept();
    }
}

void ViewerConfigurationDialog::onButtonBrowseClicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Select the storage folder"));
    if(!dir.isEmpty())
    {
        _ui->lineEditStorageFolder->setText(dir);
        updateStorageFolderValidity();
    }
}

void ViewerConfigurationDialog::reset()
{
    _ui->networkConfigWidget->reset();

    _ui->lineEditStorageFolder->setText(ViewerSettings().getStoreDir());
    _ui->comboBoxQuality->setCurrentIndex(((int)ViewerSettings().getDisplayQuality()) - 1);
    updateStorageFolderValidity();
}

void ViewerConfigurationDialog::save()
{
    _ui->networkConfigWidget->save();

    ViewerSettings settings;
    settings.setStoreDir(_ui->lineEditStorageFolder->text());
    settings.setDisplayQuality((RemoteView::Quality)(_ui->comboBoxQuality->currentIndex() + 1));
}

void ViewerConfigurationDialog::updateStorageFolderValidity()
{
    QPalette palette;

    QString path = _ui->lineEditStorageFolder->text();
    if(!path.isEmpty() && !QDir(path).exists())
    {
        palette.setColor(QPalette::Disabled, QPalette::Base, QColor(211, 91, 107));
        palette.setColor(QPalette::Disabled, QPalette::Text, Qt::black);
    }

    _ui->lineEditStorageFolder->setPalette(palette);
}
