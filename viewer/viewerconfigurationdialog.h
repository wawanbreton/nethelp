/*! @file   viewerconfigurationdialog.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QDialog>

#include <QAbstractButton>
#include <QUrl>
#include <QNetworkProxy>

namespace Ui { class ConfigurationWidget; }

/*! @brief Dialog used to configure a NetHelp client in order to make it work correctly */
class ViewerConfigurationDialog : public QDialog
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit ViewerConfigurationDialog(QWidget *parent = nullptr);

        /*! @brief Destructor, which also destroys the inner Ui object */
        ~ViewerConfigurationDialog();

    private:
        /*! @brief Slot called when a button of the bottom-bar is clicked
            @param button The button which has been pressed */
        void onButtonClicked(QAbstractButton *button);

        /*! @brief Slot called when the browse button has been clicked */
        void onButtonBrowseClicked();

        /*! @brief Resets the displayed configuration to its initial value */
        void reset();

        /*! @brief Saves the displayed configuration */
        void save();

        /*! @brief Updates the visual validity of the storage path, which becomes red if the
                   specified path does not exist */
        void updateStorageFolderValidity();

    private:
        Ui::ConfigurationWidget *_ui;
};
