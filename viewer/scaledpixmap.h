/*! @file   scaledpixmap.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QWidget>

#include <QImage>

/*! @brief Very simple widget which displays an image in it's original ratio, taking the maximum
           available space */
class ScaledPixmap : public QWidget
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit ScaledPixmap(QWidget *parent = nullptr);

        /*! @brief Gets the currently displayed image
            @return The currently displayed image */
        const QImage &getImage() const
        { return _image; }

        /*! @brief Sets the image to be displayed
            @param image The image to be displayed */
        void setImage(const QImage &image)
        { _image = image; update(); }

    protected:
        /*! @brief Method called by the Qt engine when the widget is to be drawn */
        virtual void paintEvent(QPaintEvent *) override;

    private:
        QImage _image;
};
