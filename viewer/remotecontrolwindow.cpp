/*! @file   remotecontrolwindow.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "remotecontrolwindow.h"
#include "ui_remotecontrolwindow.h"

#include <QApplication>
#include <QCloseEvent>
#include <QDateTime>
#include <QDomDocument>
#include <QMenu>
#include <QFileDialog>
#include <QProgressBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QDesktopServices>
#include <QTimer>
#include <QProcess>
#include <QKeySequence>
#include <QFileIconProvider>

#include "common/messagebox.h"
#include "core/nethelpworkerviewer.h"
#include "viewer/browseremotefilesystemdialog.h"
#include "viewer/viewersettings.h"


RemoteControlWindow::RemoteControlWindow(NetHelpWorkerViewer *endPoint,
                                         const QString &group,
                                         const QString &clientId,
                                         const QIcon &clientIcon,
                                         const ClientEndPointParams &params,
                                         QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::RemoteControlWindow),
    _endPoint(endPoint),
    _clientId(clientId),
    _clientIcon(clientIcon)
{
    _ui->setupUi(this);

    _ui->actionOpenSessionFolder->setIcon(QFileIconProvider().icon(QFileIconProvider::Folder));

    connect(_ui->actionRetrieveFiles,     &QAction::triggered,
            this,                         &RemoteControlWindow::onBrowseFiles);
    connect(_ui->actionTakeRemotePicture, &QAction::triggered,
            _endPoint,                    &NetHelpWorkerViewer::requestPicture);
    connect(_ui->actionScreenshot,        &QAction::triggered,
            this,                         &RemoteControlWindow::takeScreenshot);
    connect(_ui->actionOpenSessionFolder, &QAction::triggered,
            this,                         &RemoteControlWindow::onOpenSessionFolder);
    connect(_ui->actionCloseSession,      &QAction::triggered,
            this,                         &RemoteControlWindow::askEndSession);
    connect(_ui->buttonSaveImage,         &QPushButton::clicked,
            this,                         &RemoteControlWindow::onButtonSaveImageClicked);
    connect(_ui->actionBrowse,            &QAction::triggered,
            this,                         &RemoteControlWindow::onBrowseFiles);

    connect(_endPoint, &NetHelpWorkerViewer::fileData,
            this,      &RemoteControlWindow::onFileDownloaded);
    connect(_endPoint, &NetHelpWorkerViewer::picture,
            this,      &RemoteControlWindow::onPicture);
    connect(_endPoint, &NetHelpWorkerViewer::controlReleased,
            this,      &RemoteControlWindow::onControlLost);
    connect(_endPoint, &NetHelpWorkerViewer::connectionLost,
            this,      &RemoteControlWindow::onConnectionLost);

    _ui->actionRetrieveFiles->setMenu(new QMenu(this));

    ViewerSettings settings;
    restoreGeometry(settings.getRemoteControlWindowGeometry());
    restoreState(settings.getRemoteControlWindowState());

    _ui->vncView->installEventFilter(this);
    _ui->vncView->setHost("localhost");
    _ui->vncView->setPort(5901);
    _ui->vncView->setQuality(settings.getDisplayQuality());
    _ui->vncView->showDotCursor(RemoteView::CursorOn);
    _ui->vncView->start();

    setWindowTitle(tr("NetHelp remote control - %1").arg(clientId));

    QString storeDir = QDir::homePath() + "/NetHelp/";
    QString confDir = settings.getStoreDir();
    if(!confDir.isEmpty() && QDir(confDir).exists())
    {
        storeDir = confDir + "/";
    }

    if(!group.isEmpty())
    {
        storeDir += group + "/";
    }
    storeDir += QDateTime::currentDateTime().toString("yyyy-MM-dd-hh'h'mm'm'ss");
    storeDir += "_" + clientId;

    if(QDir().mkpath(storeDir))
    {
        _storeDir = QDir(storeDir);
        message(tr("Data will be stored in folder %1").arg(storeDir), true);
    }
    else
    {
        _storeDir = QDir();
        message(tr("Could not create session directory, data will be stored in local directory"), true);
        qWarning() << "Unable to create the data storage directory";
    }
    _logFile.setFileName(_storeDir.filePath("log.txt"));
    _logFile.open(QIODevice::WriteOnly);
    message(tr("Session started"), false);

    QList<InformationPiece> information = params.information;
    for(const MappedTcpPort &tcpPort : params.tcpPorts)
    {
        QString portInfo = tr("%1 : %2 -> %3");
        portInfo = portInfo.arg(tcpPort.name);
        portInfo = portInfo.arg(tcpPort.actualPort);
        portInfo = portInfo.arg(tcpPort.replacementPort);

        information.append({tr("Remote TCP port"), portInfo});
    }
    clientInformation(information);

    if(!params.guiDescription.isEmpty())
    {
        onClientDescription(params.guiDescription);
    }

    onFilesList(params.interestingFiles);
}

RemoteControlWindow::~RemoteControlWindow()
{
    ViewerSettings settings;
    settings.setRemoteControlWindowGeometry(saveGeometry());
    settings.setRemoteControlWindowState(saveState());

    delete _ui;
}

void RemoteControlWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    askEndSession();
}

bool RemoteControlWindow::eventFilter(QObject *object, QEvent *event)
{
    if(event->type() == QEvent::Resize)
    {
        updateViewportSize();
    }

    return QMainWindow::eventFilter(object, event);
}

void RemoteControlWindow::clientInformation(const QList<InformationPiece> &information)
{
    QStandardItemModel *model = new QStandardItemModel(this);

    for(const InformationPiece &info : information)
    {
        message(tr("%1 : %2").arg(info.key).arg(info.value), false);
        model->appendRow({new QStandardItem(info.key), new QStandardItem(info.value)});
    }

    _ui->listInformation->setModel(model);
}

void RemoteControlWindow::onClientDescription(const QString &description)
{
    QDomDocument doc;
    QString errorMsg;
    int errorLine;
    int errorColumn;
    if(doc.setContent(description, &errorMsg, &errorLine, &errorColumn))
    {
        QDomNode rootNode = doc.firstChild();
        QDomNodeList childNodes = rootNode.childNodes();
        for(int i=0 ; i<childNodes.count() ; i++)
        {
            QDomNode childNode = childNodes.at(i);
            if(childNode.nodeName() == "screenpos")
            {
                int x=0;
                int y=0;

                QDomNamedNodeMap attributes = childNode.attributes();
                if(attributes.contains("x"))
                {
                    x = attributes.namedItem("x").nodeValue().toInt();
                }
                if(attributes.contains("y"))
                {
                    y = attributes.namedItem("y").nodeValue().toInt();
                }

                _ui->vncView->move(x, y);
            }
            else if(childNode.nodeName() == "button")
            {
                QDomNamedNodeMap attributes = childNode.attributes();

                Qt::Key key = (Qt::Key)0;
                if(attributes.contains("key"))
                {
                    QKeySequence sequence(attributes.namedItem("key").nodeValue());
                    if(sequence.count() == 1)
                    {
                        key = (Qt::Key)sequence[0];
                    }
                }

                if(key == (Qt::Key)0)
                {
                    qWarning() << "No valid key has been given for the button";
                    continue;
                }

                QString text;
                if(attributes.contains("text"))
                {
                    text = attributes.namedItem("text").nodeValue();
                }

                QIcon icon;
                QSize iconSize;
                if(attributes.contains("icon"))
                {
                    QString iconStr = attributes.namedItem("icon").nodeValue();
                    QImage image = QImage::fromData(QByteArray::fromBase64(iconStr.toUtf8()));
                    iconSize = image.size();
                    icon = QIcon(QPixmap::fromImage(image));
                }

                if(text.isEmpty() && icon.isNull())
                {
                    qWarning() << "No valid text nor icon has been given for the button";
                    continue;
                }

                int x=-1, y=-1, width=-1, height=-1;
                if(attributes.contains("x"))
                {
                    x = attributes.namedItem("x").nodeValue().toInt();
                }
                if(attributes.contains("y"))
                {
                    y = attributes.namedItem("y").nodeValue().toInt();
                }
                if(attributes.contains("width"))
                {
                    width = attributes.namedItem("width").nodeValue().toInt();
                }
                if(attributes.contains("height"))
                {
                    height = attributes.namedItem("height").nodeValue().toInt();
                }

                if(x < 0 || y < 0 || width <= 0 || height <= 0)
                {
                    qWarning() << "No valid rectangle has been given for the button";
                    continue;
                }

                QPushButton *button = new QPushButton(_ui->viewport);
                button->setGeometry(x, y, width, height);
                button->setCheckable(false);
                if(!text.isEmpty())
                {
                    button->setText(text);
                }
                if(!icon.isNull())
                {
                    button->setIcon(icon);
                    button->setIconSize(iconSize);
                }

                button->setProperty("key", (int)key);
                connect(button, &QPushButton::pressed,
                        this,   &RemoteControlWindow::onSpecialButtonPressed);
                connect(button, &QPushButton::released,
                        this,   &RemoteControlWindow::onSpecialButtonReleased);

                button->show();
            }
        }

        updateViewportSize();
    }
    else
    {
        qWarning() << "Received invalid XML data :"
                   << "Line" << errorLine << "column" << errorColumn << ":" << errorMsg;
    }
}

void RemoteControlWindow::onFilesList(const QList<CategorizedFiles> &interestingFiles)
{
    QMenu *menu = _ui->actionRetrieveFiles->menu();

    menu->removeAction(_ui->actionBrowse);

    if(interestingFiles.count())
    {
        for(int i=0 ; i<interestingFiles.count() ; i++)
        {
            QAction *action = menu->addAction(interestingFiles[i].category,
                                              this,
                                              &RemoteControlWindow::onDownloadInterestingFiles);
            action->setProperty("filesList", QVariant::fromValue(interestingFiles[i].files));
        }

        menu->addAction(_ui->actionBrowse);
    }
}

void RemoteControlWindow::onFileDownloaded(const Path &filePath,
                                           const QByteArray &data,
                                           int size)
{
    DownloadFile *actualDownloadFile = nullptr;
    for(DownloadFile &downloadFile : _filesDownloads)
    {
        if(downloadFile.remoteName == filePath)
        {
            actualDownloadFile = &downloadFile;
        }
    }

    if(!actualDownloadFile)
    {
        qWarning() << "There is no currently registered downloaded file" << filePath;
        return;
    }

    actualDownloadFile->data.append(data);

    QProgressBar *progressBar = qobject_cast<QProgressBar *>(_ui->tableFilesDownloads->cellWidget(actualDownloadFile->row, 1));
    progressBar->setValue((actualDownloadFile->data.size() / ((qreal)size)) * 100.0);

    if(actualDownloadFile->data.size() == size)
    {
        // File download is over
        QFile downloadedFile(actualDownloadFile->localName.toLocalized().getPath());
        if(downloadedFile.open(QIODevice::WriteOnly))
        {
            downloadedFile.write(qUncompress(actualDownloadFile->data));
            downloadedFile.flush();
            downloadedFile.close();

            message(tr("Downloaded file %1").arg(actualDownloadFile->localName.toLocalized().getPath()), true);
        }
        else
        {
            message(tr("Unable to open destination file %1 for writing : %2").arg(actualDownloadFile->localName.toLocalized().getPath()).arg(downloadedFile.errorString()), true);
        }

        setCellWidget(actualDownloadFile->row, QDialogButtonBox::Open);
        actualDownloadFile->over = true;
    }
}

void RemoteControlWindow::onPicture(const QImage &image)
{
    _ui->widgetPicture->setImage(image);
    _ui->buttonSaveImage->setEnabled(!image.size().isEmpty());
}

void RemoteControlWindow::onControlLost()
{
    message(tr("The remote client has disconnected"), false);
    MessageBox::show(this,
                     &RemoteControlWindow::sessionEnded,
                     this,
                     QMessageBox::Critical,
                     tr("The remote client has disconnected"));
}

void RemoteControlWindow::onConnectionLost()
{
    message(tr("Connection to server lost"), false);
    sessionEnded();
}

void RemoteControlWindow::message(const QString &msg, bool showInGui)
{
    QString datedMsg = QDateTime::currentDateTime().toString("yyyy/MM/dd hh'h'mm'm'ss'.'zzz");
    datedMsg += " " + msg + '\n';

    if(_logFile.isWritable())
    {
        _logFile.write(datedMsg.toUtf8());
        _logFile.flush();
    }
    else
    {
        qInfo() << datedMsg;
    }

    if(showInGui)
    {
        _ui->statusBar->showMessage(msg);
    }
}

void RemoteControlWindow::setCellWidget(int row, QDialogButtonBox::StandardButton button)
{
    QDialogButtonBox box(button);
    QPushButton *pushButton = box.button(button);
    _ui->tableFilesDownloads->setCellWidget(row, 2, pushButton);
    connect(pushButton, &QPushButton::clicked,
            this,       &RemoteControlWindow::onButtonFileActionClicked);
}

void RemoteControlWindow::buildTableRow(const DownloadFile &info)
{
    _ui->tableFilesDownloads->setItem(info.row, 0, new QTableWidgetItem(info.remoteName.toNormalized().getPath()));
    _ui->tableFilesDownloads->setCellWidget(info.row, 1, new QProgressBar());

    if(info.over)
    {
        setCellWidget(info.row, QDialogButtonBox::Open);
    }
    else
    {
        setCellWidget(info.row, QDialogButtonBox::Cancel);
    }
}

void RemoteControlWindow::appendFilesToDownload(const QList<Path> &files)
{
    int rowToFill = _ui->tableFilesDownloads->rowCount();
    _ui->tableFilesDownloads->setRowCount(rowToFill + files.count());

    for(const Path &fileToDownload : files)
    {
        DownloadFile info;
        QString baseName = QFileInfo(fileToDownload.toNormalized().getPath()).fileName();
        info.localName = Path(_storeDir.filePath(baseName), false);
        info.remoteName = fileToDownload;
        info.row = rowToFill;
        info.over = false;
        _filesDownloads << info;

        buildTableRow(info);

        rowToFill++;

        _endPoint->downloadFile(fileToDownload);
    }
}

void RemoteControlWindow::updateViewportSize()
{
    QRect rect;
    for(const QObject *object : _ui->viewport->children())
    {
        const QWidget *childWidget = qobject_cast<const QWidget *>(object);
        if(childWidget)
        {
            rect |= childWidget->geometry();
        }
    }

    _ui->viewport->setGeometry(QRect(QPoint(0, 0), rect.bottomRight()));
}

void RemoteControlWindow::sessionEnded()
{
    disconnect(_endPoint, nullptr, this, nullptr);

    message(tr("Session closed"), false);
    _logFile.close();

    deleteLater();
}

void RemoteControlWindow::askEndSession()
{
    MessageBox::show(this,
                     [this](int code) { if(code == QMessageBox::Yes) { endSession(); }},
                     this,
                     QMessageBox::Warning,
                     tr("Are you sure you want to end the remote session ?"),
                     QList<ButtonDesc>() << qMakePair(tr("End session"), QMessageBox::Yes) <<
                                          qMakePair(tr("Resume session"), QMessageBox::Cancel));
}

void RemoteControlWindow::endSession()
{
    _endPoint->releaseControl();
    sessionEnded();
}

void RemoteControlWindow::takeScreenshot()
{
    QPixmap screenshot = _ui->vncView->takeScreenshot();
    QString fileName = _storeDir.filePath(QDateTime::currentDateTimeUtc().toString("hh'h'mm'm'ss'.'zzz") + ".jpg");
    if(screenshot.save(fileName, "JPG", 90))
    {
        message(tr("Screenshot saved to file %1").arg(fileName), true);
    }
    else
    {
        message(tr("Error when saving screenshot"), true);
    }
}

void RemoteControlWindow::onBrowseFiles()
{
    QDialog *dialog = new BrowseRemoteFileSystemDialog(_endPoint, _clientId, _clientIcon, this);

    connect(dialog, &QDialog::accepted, this,   &RemoteControlWindow::onBrowseFileDialogAccepted);
    connect(dialog, &QDialog::finished, dialog, &QDialog::deleteLater);

    dialog->show();
}

void RemoteControlWindow::onBrowseFileDialogAccepted()
{
    BrowseRemoteFileSystemDialog *dialog = qobject_cast<BrowseRemoteFileSystemDialog *>(sender());
    if(dialog)
    {
        if(dialog->getSelectedFiles().count() > 0)
        {
            appendFilesToDownload(dialog->getSelectedFiles());
        }
    }
    else
    {
        qCritical() << "Sender is not a BrowseRemoteFileSystemDialog" << sender();
    }
}

void RemoteControlWindow::onDownloadInterestingFiles()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if(action)
    {
        appendFilesToDownload(action->property("filesList").value<QList<Path> >());
    }
    else
    {
        qCritical() << "Sender is not a QAction";
    }
}


void RemoteControlWindow::onButtonFileActionClicked()
{
    QPushButton *button = qobject_cast<QPushButton *>(sender());
    if(!button)
    {
        qCritical() << "Sender is not a QPushButton";
        return;
    }

    for(int row=0 ; row<_ui->tableFilesDownloads->rowCount() ; row++)
    {
        if(button == _ui->tableFilesDownloads->cellWidget(row, 2))
        {
            for(int i=0 ; i<_filesDownloads.count() ; i++)
            {
                const DownloadFile &fileInfo = _filesDownloads[i];

                if(fileInfo.row == row)
                {
                    if(fileInfo.over)
                    {
                        // Open the file with the default associated program
                        QDesktopServices::openUrl("file:///" + fileInfo.localName.toNormalized().getPath());
                    }
                    else
                    {
                        // Remove the file from the list
                        _filesDownloads.removeAt(i);

                        for(int j=0 ; j<_filesDownloads.count() ; j++)
                        {
                            DownloadFile &fileToModify = _filesDownloads[j];
                            if(fileToModify.row > fileInfo.row)
                            {
                                fileToModify.row--;
                                buildTableRow(fileToModify);
                            }
                        }

                        _ui->tableFilesDownloads->setRowCount(_ui->tableFilesDownloads->rowCount() - 1);
                    }

                    break;
                }
            }

            break;
        }
    }
}

void RemoteControlWindow::onButtonSaveImageClicked()
{
    QString file = "webcam_" + QTime::currentTime().toString("hh'h'mm'm'ss.zzz") + ".jpg";
    file = _storeDir.absoluteFilePath(file);

    _ui->widgetPicture->getImage().save(file, "JPG", 80);

    message(tr("Saved webcam image to %1").arg(file), true);
}

void RemoteControlWindow::onOpenSessionFolder()
{
    QDesktopServices::openUrl("file:///" + _storeDir.absolutePath());
}

void RemoteControlWindow::onSpecialButtonPressed()
{
    int key = sender()->property("key").toInt();
    Qt::KeyboardModifiers modifiers = QApplication::keyboardModifiers();
    QKeyEvent *keyEvent = new QKeyEvent(QEvent::KeyPress, key, modifiers);

    QApplication::postEvent(_ui->vncView, keyEvent);
}

void RemoteControlWindow::onSpecialButtonReleased()
{
    int key = sender()->property("key").toInt();
    Qt::KeyboardModifiers modifiers = QApplication::keyboardModifiers();
    QKeyEvent *keyEvent = new QKeyEvent(QEvent::KeyRelease, key, modifiers);

    QApplication::postEvent(_ui->vncView, keyEvent);
}
