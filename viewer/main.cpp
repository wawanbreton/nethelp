/*! @file   viewer/main.cpp
    @brief  This file is the entry point of the NetHelp viewer application
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include <QApplication>
#include <QCommandLineParser>

#include "common/messagehandler.h"
#include "common/path.h"
#include "viewer/mainwindow.h"


QString boolOptionTostring(const QString &message, bool option)
{
    return message.arg(option ? "true" : "false");
}

/*! @brief NetHelp viewer entry point
    @param argc The number of given arguments
    @param argv The list of given arguments
    @return The program exit code */
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setOrganizationName("NetHelp");
    app.setOrganizationDomain("sourceforge.net/projects/nethelp");
    app.setApplicationName("NetHelp viewer");
    app.setApplicationVersion("1.2");
    app.setWindowIcon(QIcon(":/icons/icon_64.png"));
    app.setQuitOnLastWindowClosed(false);

    // Initialize parser
    QCommandLineParser parser;
    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsCompactedShortOptions);
    parser.setApplicationDescription("This is the NetHelp viewer application, "
                                     "you can use the following options :");

    parser.addHelpOption();

    // Debug option
    bool displayDebug = false;
    QCommandLineOption optionDebug({"d", "debug"},
                                   boolOptionTostring("Display debug messages (default %1)",
                                                      displayDebug));
    parser.addOption(optionDebug);

    parser.process(app);

    MessageHandler handler(".");
    handler.setMsgType(parser.isSet(optionDebug) ? QtDebugMsg : QtWarningMsg);

    qRegisterMetaType<Path>();

    MainWindow w;
    w.show();

    return app.exec();
}
