#define AppBin "viewer.exe"
#define AppName "NetHelp Viewer"
#define AppVersion "1.3"
#define AppURL "https://bitbucket.org/wawanbreton/nethelp/"
#define AppPublisher "Erwan MATHIEU"

[Setup]
AppId={{057E687E-DEAD-389D-BEEF-401DEAEAC8EE}}
AppName={#AppName}              
AppVerName={#AppName} - v{#AppVersion}     
AppVersion={#AppVersion}

AppPublisher={#AppPublisher}
AppPublisherURL={#AppURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}       
AppCopyright=Copyright � 2019 {#AppPublisher}

VersionInfoVersion={#AppVersion}
DefaultDirName={userpf}\{#AppName}
LicenseFile=../../setup/misc/license.txt
DisableProgramGroupPage=yes
OutputBaseFilename=NetHelpViewer-setup
Compression=lzma
PrivilegesRequired=lowest
SetupIconFile=../viewer_icon.ico

[Files]
; Main executable
Source: "../{#AppBin}"; DestDir: "{app}"; Flags: ignoreversion

; Qt and its dependancies
#define MsysDir "C:\msys64\mingw64"
Source: "{#MsysDir}\bin\Qt5Core.dll";              DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\Qt5Gui.dll";               DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\Qt5Network.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\Qt5Widgets.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\Qt5Xml.dll";               DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libbz2-1.dll";             DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libcrypto-1_1-x64.dll";    DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libdouble-conversion.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libffi-6.dll";             DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libfreetype-6.dll";        DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libgcc_s_seh-1.dll";       DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libgcrypt-20.dll";         DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libglib-2.0-0.dll";        DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libgmp-10.dll";            DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libgnutls-30.dll";         DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libgpg-error-0.dll";       DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libgraphite2.dll";         DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libharfbuzz-0.dll";        DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libhogweed-5.dll";         DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libiconv-2.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libicudt65.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libicuin65.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libicuuc65.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libidn2-0.dll";            DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libintl-8.dll";            DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libjpeg-8.dll";            DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\liblzo2-2.dll";            DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libnettle-7.dll";          DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libp11-kit-0.dll";         DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libpcre-1.dll";            DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libpcre2-16-0.dll";        DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libpng16-16.dll";          DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libsasl2-3.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libssl-1_1-x64.dll";       DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libstdc++-6.dll";          DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libtasn1-6.dll";           DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libunistring-2.dll";       DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libvncclient.dll";         DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libwinpthread-1.dll";      DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\libzstd.dll";              DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\bin\zlib1.dll";                DestDir: "{app}"; Flags: ignoreversion
Source: "{#MsysDir}\share\qt5\plugins\platforms\qwindows.dll";        DestDir: "{app}\platforms"; Flags: ignoreversion
Source: "{#MsysDir}\share\qt5\plugins\styles\qwindowsvistastyle.dll"; DestDir: "{app}\styles";    Flags: ignoreversion
Source: "{#MsysDir}\share\qt5\plugins\bearer\qgenericbearer.dll";     DestDir: "{app}\bearer";    Flags: ignoreversion

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}";

[Icons]
Name: "{group}\{#AppName}";        Filename: "{app}\{#AppBin}"; Comment: "{#AppName} {#AppVersion}"
Name: "{userdesktop}\{#AppName}";  Filename: "{app}\{#AppBin}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#AppBin}"; Description: "{cm:LaunchProgram,{#AppName}}"; Flags: nowait postinstall skipifsilent
