/*! @file   mainwindow.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QMainWindow>

#include <QMap>
#include <QIcon>
#include <QStandardItemModel>

#include "common/clientendpointparams.h"
#include "common/stringboolpair.h"

namespace Ui { class MainWindow; }

class NetHelpWorkerViewer;
class RemoteControlWindow;
class ClientsManager;
class GroupsManager;

/*! @brief This is the viewer main window, which displays the connected clients and allows starting
           a remote control session */
class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit MainWindow(QWidget *parent = nullptr);

        /*! @brief Destructor, which also destroys the inner Ui object */
        virtual ~MainWindow();

    protected:
        virtual void closeEvent(QCloseEvent *event) override;

    private:
        /*! @brief Removes all the clients from the list */
        void clearClients();

        /*! @brief Adds a new group */
        void addGroup();

        void deleteGroup(int group);

        void onDeleteGroupConfirmed(int group);

        /*! @brief Edits the given group
            @param group The group to be edited */
        void editGroup(int group);

        void onGroupEdited(int group);

        /*! @brief Appends a group item to the display list
            @param name The name of the group
            @param icon The icon of the group */
        void appendGroupItem(const QString &name, const QIcon &icon=QIcon());

        /*! @brief Finds a client item in the display list among the groups
            @param identifier The unique identifier of the client to be found */
        QStandardItem *findClientItem(const QString &identifier);

        /*! @brief Slot called when the connect to server button is clicked
            @note The button may also be the disconnect button, according to the current status */
        void onButtonConnectClick();

        /*! @brief Slot called when the take control button is clicked */
        void onButtonTakeControlClick();

        /*! @brief Slot called when the remote control taking is over
         *  @param success Indicates whether the client remote control was taken */
        void onTakeControlResult(bool success);

        /*! @brief Slot called when the clients list selection changes */
        void onListSelectionChanged();

        /*! @brief Slot called when the session opening sequency is over
            @param identifier The server-assigned identifier, or an empty string if an error occured
            @param password The password should be always empty as we can't take control of a viewer
            @param errorMsg The error message, which is empty if everything went well */
        void onSessionOpenEnd(const QString &identifier,
                              const QString &password,
                              const QString &errorMsg);

        /*! @brief Slot called when the icon of a client is available
            @param clientId The identifier of the client which icon is available
            @param iconData The client icon as PNG data */
        void onClientParams(const QString &clientId, const ClientEndPointParams &params);

        /*! @brief Slot to be called when the connection to the server has been lost
                   without being asked */
        void onConnectionLost();

        /*! @brief Updates the display of the server connection status */
        void updateConnectedStatus();

        /*! @brief Slot called when a new client is connected
            @param identifier The unique identifier of the client
            @param remoteControlled Indicates if the client is currently remotely controlled */
        void onNewClient(const QString &identifier, bool remoteControlled);

        /*! @brief Slot called when a client remote controlled status changed
            @param identifier The identifier of the client which changed
            @param remoteControlled Indicates if the client is currently remotely controlled */
        void onClientControlChanged(const QString &identifier, bool remoteControlled);

        /*! @brief Slot called when a client is disconnected
            @param identifier The identifier of the disconnected client */
        void onClientRemoved(const QString &identifier);

        /*! @brief Slot called when a menu is requested by the clients list
            @param pos The mouse position of the menu to be displayed */
        void onRequestTreeMenu(const QPoint &pos);

        /*! @brief Slot called when a client has been moved to a new group */
        void onClientsLayoutChanged();

        /*! @brief Slot called when the user double-clicked on a list item
            @param index The item of the list which has been clicked */
        void onItemDoubleClicked(const QModelIndex &index);

        void onEndPointOver();

        void connectToServer();

        QDialog *displayConfig();

    private:
        Ui::MainWindow *_ui{nullptr};
        NetHelpWorkerViewer *_endPoint{nullptr};
        QStandardItemModel *_clientsListModel{nullptr};
        ClientsManager *_clientsManager{nullptr};
        GroupsManager *_groupsManager{nullptr};
};
