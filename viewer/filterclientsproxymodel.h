/*! @file   filterclientsproxymodel.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QSortFilterProxyModel>

/*! @brief Simple proxy model to display only the remote-controlled clients, or all */
class FilterClientsProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit FilterClientsProxyModel(QObject *parent = nullptr);

        /*! @brief Sets whether the controlled client should be hidden
            @param hideControlled True if the controlled client should be hidden, false if they
                                  should be displayed */
        void setHideControlled(bool hideControlled)
        { setDisplayControlled(!hideControlled); }

        /*! @brief Sets whether the controlled client should be displayed
            @param displayControlled True if the controlled client should be displayed, false if
                                     they should be hidden */
        void setDisplayControlled(bool displayControlled)
        { _displayControlled = displayControlled; invalidateFilter(); }

    protected:
        /*! @brief Method re-implemented to displayed the relevant clients according to the current
                   filter configuration
            @param sourceRow The original row which may be filtered
            @param sourceParent The original parent
            @return True if the row should be displayed, false if it should be hidden */
        bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

    private:
        bool _displayControlled;
};
