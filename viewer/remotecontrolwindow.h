/*! @file   remotecontrolwindow.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QMainWindow>
#include <QDir>
#include <QQueue>
#include <QDialogButtonBox>

#include "common/categorizedfiles.h"
#include "common/clientendpointparams.h"
#include "common/fileinfo.h"
#include "common/path.h"

namespace Ui { class RemoteControlWindow; }

class NetHelpWorkerViewer;

/*! @brief Window used when controlling a remote client : it displays the remote desktop, the list
           of informations, the downloaded files, and the remote picture. All these elements are
           dock widgets wich may be moved, hidden or taken out of the window. */
class RemoteControlWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit RemoteControlWindow(NetHelpWorkerViewer *endPoint,
                                     const QString &group,
                                     const QString &clientId,
                                     const QIcon &clientIcon,
                                     const ClientEndPointParams &params,
                                     QWidget *parent = nullptr);

        /*! @brief Destructor, which also destroys the inner Ui object */
        ~RemoteControlWindow();

    protected:
        /*! @brief Method called by the Qt engine when the window is to be closed by the user
            @param event Object giving informations about the event, and allowing to cancel it */
        virtual void closeEvent(QCloseEvent *event) override;

        /*! @brief Method called when a filtered object receives an event
         *  @param object The object which received the event
         *  @param event The event sent to the object
         *  @return True if the event should be filtered, false if it should be forwarded */
        virtual bool eventFilter(QObject *object, QEvent *event) override;

    private:
        /*! @brief Describes the parameters and status of a file to be downloaded */
        typedef struct
        {
            Path remoteName;
            Path localName;
            int row;
            bool over;
            QByteArray data;
        } DownloadFile;

    private:
        /*! @brief Logs a message in the session file, and displays it in the GUI if asked
            @param msg The message to be logged
            @param showInGui Indicates if the message should be displayed to the user in the GUI */
        void message(const QString &msg, bool showInGui);

        /*! @brief Sets a standard button in the files downloads table
            @param row The row of the table to be filled
            @param button The button to be inserted in the cell */
        void setCellWidget(int row, QDialogButtonBox::StandardButton button);

        /*! @brief Builds a row in the files downloads table according
                   to the given file informations
            @param info The informations about the file download */
        void buildTableRow(const DownloadFile &info);

        /*! @brief Appends a list of files to be downloaded to the current download queue
            @param files The remote absolute paths of the files to be downloaded */
        void appendFilesToDownload(const QList<Path> &files);

        /*! @brief Updates the size of the scrollarea viewport according to its current content */
        void updateViewportSize();

        /*! @brief Slot to be called when a textual information is received from the client
            @param information The information received from the client */
        void clientInformation(const QList<InformationPiece> &information);

        /*! @brief Slot to be called when a GUI description is received from the client
            @param description The description received from the client */
        void onClientDescription(const QString &description);

        /*! @brief Slot to be called when the interesting client files list is received
            @param interestingFiles The list of interesting files, sorted by categories */
        void onFilesList(const QList<CategorizedFiles> &interestingFiles);

        void onTakeControlStart();

        void askEndSession();

        void endSession();

        /*! @brief Takes a screenshot of the remote desktop and saves it in the session directory */
        void takeScreenshot();

        /*! @brief Slot called when the browse remote files action is triggered */
        void onBrowseFiles();

        /*! @brief Slot called when the browse remote files dialog is accepted */
        void onBrowseFileDialogAccepted();

        /*! @brief Slot called when the download interesting files action is triggered */
        void onDownloadInterestingFiles();

        /*! @brief Slot called when a file download action button (open or cancel) is clicked */
        void onButtonFileActionClicked();

        /*! @brief Slot called when the "save image" button is clicked */
        void onButtonSaveImageClicked();

        /*! @brief Slot called when the "open session folder" action is triggered */
        void onOpenSessionFolder();

        /*! @brief Slot called when a shortcut button is pressed */
        void onSpecialButtonPressed();

        /*! @brief Slot called when a shortcut button is released */
        void onSpecialButtonReleased();

        void onFileDownloaded(const Path &filePath, const QByteArray &data, int size);

        /*! @brief Slot to be called when a remote picture is received
            @param image The received picture */
        void onPicture(const QImage &image);

        /*! @brief Slot called when the remote control has been lost without being asked, probably
                   because the client has closed the session from his own */
        void onControlLost();

        void onConnectionLost();

        /*! @brief Method called when the remote control sesssion has ended by any way */
        void sessionEnded();

    private:
        Ui::RemoteControlWindow *_ui{nullptr};
        NetHelpWorkerViewer *_endPoint{nullptr};
        const QString _clientId;
        const QIcon _clientIcon;
        QDir _storeDir;
        QFile _logFile;
        QList<DownloadFile> _filesDownloads;
};
