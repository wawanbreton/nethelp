/*! @file   configuregroupdialog.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "configuregroupdialog.h"
#include "ui_configuregroupdialog.h"

#include <QFileDialog>

#include "common/messagebox.h"


ConfigureGroupDialog::ConfigureGroupDialog(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::ConfigureGroupDialog)
{
    _ui->setupUi(this);

    connect(_ui->buttonIcon, &QPushButton::clicked,
            this,            &ConfigureGroupDialog::onButtonIconClicked);
}

ConfigureGroupDialog::~ConfigureGroupDialog()
{
    delete _ui;
}

QString ConfigureGroupDialog::getName() const
{
    return _ui->lineEditName->text();
}

void ConfigureGroupDialog::setName(const QString &name)
{
    setWindowTitle(tr("Edition of group %1").arg(name));

    _ui->lineEditName->setText(name);
}

QIcon ConfigureGroupDialog::getIcon() const
{
    return _ui->buttonIcon->icon();
}

void ConfigureGroupDialog::setIcon(const QIcon& icon)
{
    _ui->buttonIcon->setIcon(icon);
    _ui->buttonIcon->setText(icon.isNull() ? tr("None") : "");
}

void ConfigureGroupDialog::onButtonIconClicked()
{
    QString file = QFileDialog::getOpenFileName(this,
                                                tr("Select the group icon"),
                                                QString(),
                                                tr("Images (*.png *.jpg *.jpeg *.bmp)"));

    if(!file.isNull())
    {
        QPixmap pixmap(file);
        if(!pixmap.isNull())
        {
            setIcon(QIcon(pixmap.scaled(32, 32, Qt::KeepAspectRatio, Qt::SmoothTransformation)));
        }
        else
        {
            MessageBox::critical(this, tr("Unable to load icon file %1").arg(file));
        }
    }
}
