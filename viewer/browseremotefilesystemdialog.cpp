/*! @file   browseremotefilesystemdialog.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "browseremotefilesystemdialog.h"
#include "ui_browseremotefilesystemdialog.h"

#ifdef Q_OS_WIN
#define _WIN32_IE 0x0500
#include <qt_windows.h>
#include <commctrl.h>
#include <objbase.h>
#endif

#include <QTreeView>

#include "core/nethelpworkerviewer.h"
#include "viewer/noexpanditemstyle.h"


BrowseRemoteFileSystemDialog::BrowseRemoteFileSystemDialog(NetHelpWorkerViewer *endPoint,
                                                           const QString &clientId,
                                                           const QIcon &clientIcon,
                                                           QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::BrowseRemoteFileSystemDialog),
    _endPoint(endPoint),
    _modelFiles(new QStandardItemModel(this)),
    _modelPath(new QStandardItemModel(this)),
    _driveRegExp("[A-Z]:")
{
    _ui->setupUi(this);

    _ui->listFiles->setModel(_modelFiles);

    QTreeView *pathTree = new QTreeView(_ui->comboBox);
    pathTree->setHeaderHidden(true);
    pathTree->setIndentation(12);
    pathTree->setItemsExpandable(false);
    pathTree->setRootIsDecorated(false);
    pathTree->setStyle(new NoExpandItemStyle(pathTree->style()));

    _ui->comboBox->setView(pathTree);
    _ui->comboBox->setModel(_modelPath);

    _modelPath->appendRow(new QStandardItem(clientIcon, clientId));
    rebuildPath();

    connect(_ui->comboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this,          &BrowseRemoteFileSystemDialog::onSelectedPathChanged);

    connect(_endPoint, &NetHelpWorkerViewer::directoryListed,
            this,      &BrowseRemoteFileSystemDialog::onFilesListReceived);
    connect(_ui->listFiles,  &QListView::doubleClicked,
            this,            &BrowseRemoteFileSystemDialog::onItemDoubleClicked);
    connect(_ui->listFiles->selectionModel(), &QItemSelectionModel::selectionChanged,
            this,                            &BrowseRemoteFileSystemDialog::onSelectedFilesChanged);
    connect(_ui->buttonGoUp, &QPushButton::clicked,
            this,            &BrowseRemoteFileSystemDialog::onButtonGoUpClicked);
    connect(_ui->buttonDownload, &QPushButton::clicked,
            this,                &BrowseRemoteFileSystemDialog::onAccept);

    rebuildFilesList();
}

BrowseRemoteFileSystemDialog::~BrowseRemoteFileSystemDialog()
{
    delete _ui;
}

void BrowseRemoteFileSystemDialog::rebuildFilesList()
{
    _endPoint->listDirectory(_currentPath);
}

void BrowseRemoteFileSystemDialog::rebuildPath()
{
    int depth = 0;
    QStandardItem *lastItem = _modelPath->item(0);
    lastItem->removeRow(0);

    for(const QString &pathPart : _currentPath.getParts())
    {
        QIcon icon;
        if(depth == 0 && _driveRegExp.exactMatch(pathPart))
        {
            icon = _iconProvider.icon(QFileIconProvider::Drive);
        }
        else
        {
            icon = _iconProvider.icon(QFileIconProvider::Folder);
        }

        QStandardItem *item = new QStandardItem(icon, pathPart);

        lastItem->setChild(0, item);

        lastItem = item;
        depth++;
    }

    QTreeView *tree = qobject_cast<QTreeView *>(_ui->comboBox->view());
    tree->expandAll();

    _ui->comboBox->blockSignals(true);

    // Now we would like to set the lowest part of the path as the selected element in the combobox.
    // This is quite tricky to be done, so the only way I found is the following :
    QModelIndex index = _modelPath->indexFromItem(lastItem);
    _ui->comboBox->setRootModelIndex(index.parent());
    _ui->comboBox->setCurrentIndex(0);
    _ui->comboBox->setRootModelIndex(QModelIndex());
    tree->setCurrentIndex(index);

    _ui->comboBox->blockSignals(false);
}

void BrowseRemoteFileSystemDialog::onFilesListReceived(const Path &dirPath,
                                                       const QList<FileInfo> &infos)
{
    if(dirPath == _currentPath)
    {
        _modelFiles->clear();

        for(const FileInfo &info : infos)
        {
            QStandardItem *item = new QStandardItem(info.name);
            if(info.isFile)
            {
                QIcon icon;
                QFileInfo fileInfo(info.name);
                icon = _iconProvider.icon(fileInfo);
                item->setIcon(icon);
                item->setData(true, Qt::UserRole + 1);
            }
            else
            {
                if(_currentPath.isRoot() && _driveRegExp.exactMatch(info.name))
                {
                    // We assume the directory is a drive, which is very probably true
                    item->setIcon(_iconProvider.icon(QFileIconProvider::Drive));
                }
                else
                {
                    item->setIcon(_iconProvider.icon(QFileIconProvider::Folder));
                }
            }
            item->setData(info.isFile, Qt::UserRole + 1);

            _modelFiles->appendRow(item);
        }

        onSelectedFilesChanged();
    }
    else
    {
        qWarning() << "Received unexpected files list of directory" << dirPath;
    }
}

void BrowseRemoteFileSystemDialog::onItemDoubleClicked(const QModelIndex &index)
{
    bool isFile = index.data(Qt::UserRole + 1).toBool();
    if(isFile)
    {
        onAccept();
    }
    else
    {
        moveToPath(_currentPath.subElement(index.data(Qt::DisplayRole).toString()));
    }
}

void BrowseRemoteFileSystemDialog::onSelectedPathChanged()
{
    int depth = 0;
    QModelIndex index = _ui->comboBox->view()->currentIndex();
    while(index.parent().isValid())
    {
        index = index.parent();
        depth++;
    }

    moveToPath(Path(_currentPath.getParts().mid(0, depth)));
}

void BrowseRemoteFileSystemDialog::onSelectedFilesChanged()
{
    for(const QModelIndex &index : _ui->listFiles->selectionModel()->selectedIndexes())
    {
        if(index.data(Qt::UserRole + 1).toBool())
        {
            _ui->buttonDownload->setEnabled(true);
            return;
        }
    }

    _ui->buttonDownload->setEnabled(false);
}

void BrowseRemoteFileSystemDialog::onAccept()
{
    _selectedFiles.clear();

    for(const QModelIndex &index : _ui->listFiles->selectionModel()->selectedIndexes())
    {
        if(index.data(Qt::UserRole + 1).toBool())
        {
            _selectedFiles << _currentPath.subElement(index.data(Qt::DisplayRole).toString());
        }
    }

    accept();
}
