/*! @file   mainwindow.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QInputDialog>
#include <QNetworkProxy>
#include <QMenu>
#include <QTimer>

#include "common/messagebox.h"
#include "core/nethelpworkerviewer.h"
#include "viewer/clientitemdelegate.h"
#include "viewer/clientsmanager.h"
#include "viewer/configuregroupdialog.h"
#include "viewer/filterclientsproxymodel.h"
#include "viewer/groupsmanager.h"
#include "viewer/remotecontrolwindow.h"
#include "viewer/viewerconfigurationdialog.h"
#include "viewer/viewersettings.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow),
    _clientsListModel(new QStandardItemModel(this)),
    _clientsManager(new ClientsManager(this)),
    _groupsManager(new GroupsManager())
{
    _ui->setupUi(this);

    FilterClientsProxyModel *filterModel = new FilterClientsProxyModel(this);
    filterModel->setSourceModel(_clientsListModel);
    _clientsListModel->invisibleRootItem()->setDropEnabled(false);

    _ui->listClients->setModel(filterModel);
    connect(_ui->listClients, &SignalDropTreeView::somethingDropped,
            this,             &MainWindow::onClientsLayoutChanged);

    delete _ui->listClients->itemDelegate();
    _ui->listClients->setItemDelegate(new ClientItemDelegate(this));

    connect(_ui->buttonConfig,      &QPushButton::clicked, this, &MainWindow::displayConfig);
    connect(_ui->buttonConnect,     &QPushButton::clicked, this, &MainWindow::onButtonConnectClick);
    connect(_ui->buttonTakeControl, &QPushButton::clicked, this, &MainWindow::onButtonTakeControlClick);
    connect(_ui->checkBoxFilter,    &QCheckBox::toggled,
            filterModel,            &FilterClientsProxyModel::setHideControlled);
    connect(_ui->listClients->selectionModel(), &QItemSelectionModel::selectionChanged,
            this,                               &MainWindow::onListSelectionChanged);
    connect(_ui->listClients, &SignalDropTreeView::customContextMenuRequested,
            this,             &MainWindow::onRequestTreeMenu);
    connect(_ui->listClients, &SignalDropTreeView::doubleClicked,
            this,             &MainWindow::onItemDoubleClicked);

    updateConnectedStatus();

    connect(_clientsManager, &ClientsManager::newClient, this, &MainWindow::onNewClient);
    connect(_clientsManager, &ClientsManager::clientControlChanged,
            this,            &MainWindow::onClientControlChanged);
    connect(_clientsManager, &ClientsManager::clientRemoved, this, &MainWindow::onClientRemoved);

    NetHelpSettings settings;
    if(!settings.getServerHost().isEmpty())
    {
        // Try to connect right now
        connectToServer();
    }

    appendGroupItem("Default group");

    for(int i=0 ; i<_groupsManager->count() ; i++)
    {
        appendGroupItem(_groupsManager->getGroupName(i), _groupsManager->getGroupIcon(i));
    }

    _ui->listClients->expandAll();

    restoreGeometry(ViewerSettings().getMainWindowGeometry());
}

MainWindow::~MainWindow()
{
    ViewerSettings().setMainWindowGeometry(saveGeometry());

    delete _ui;
    delete _groupsManager;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(_endPoint && _endPoint->isSessionRunning())
    {
        connect(_endPoint, &NetHelpWorkerViewer::over,
                this,      []() { QApplication::exit(0); });
        _endPoint->stopSession();

        // In case the goodbye message can't be sent properly
        QTimer::singleShot(10000, this, []() { QApplication::exit(0); });
    }
    else
    {
        QApplication::exit(0);
    }

    return QMainWindow::closeEvent(event);
}

void MainWindow::clearClients()
{
    // Sets a pseudo-list of clients which is empty to the model, which will empty the view
    _clientsManager->onClientsListUpdate(QList<StringBoolPair>());
}

void MainWindow::addGroup()
{
    int group = _groupsManager->addGroup();
    appendGroupItem(_groupsManager->getGroupName(group), _groupsManager->getGroupIcon(group));
}

void MainWindow::deleteGroup(int group)
{
    if(_groupsManager->groupMembersCount(group) == 0)
    {
        // Groups is empty, don't ask a confirmation
        onDeleteGroupConfirmed(group);
    }
    else
    {
        QString message = tr("The group '%1' contains associated clients, are you sure you want to delete it ?");
        message = message.arg(_groupsManager->getGroupName(group));

        QList<ButtonDesc> buttons;
        buttons << qMakePair(tr("Delete group"), QMessageBox::Yes);
        buttons << qMakePair(tr("Don't delete group"), QMessageBox::Cancel);

        MessageBox::show(this,
                         [this, group](int code) { if(code == QMessageBox::Yes) { onDeleteGroupConfirmed(group); } },
                         this,
                         QMessageBox::Warning,
                         message,
                         buttons);
    }
}

void MainWindow::onDeleteGroupConfirmed(int group)
{
    QList<QStandardItem *> children = _clientsListModel->item(group + 1)->takeColumn(0);
    if(children.count() > 0)
    {
        _clientsListModel->item(0)->appendRows(children);
    }

    _groupsManager->removeGroup(group);
    delete _clientsListModel->takeRow(group + 1)[0];
}

void MainWindow::editGroup(int group)
{
    ConfigureGroupDialog *dialog = new ConfigureGroupDialog(this);

    dialog->setName(_groupsManager->getGroupName(group));
    dialog->setIcon(_groupsManager->getGroupIcon(group));

    connect(dialog, &ConfigureGroupDialog::accepted,
            this,   [this, group] { onGroupEdited(group); });
    connect(dialog, &ConfigureGroupDialog::finished,
            dialog, &ConfigureGroupDialog::deleteLater);

    dialog->setModal(true);
    dialog->show();
}

void MainWindow::onGroupEdited(int group)
{
    ConfigureGroupDialog *dialog = qobject_cast<ConfigureGroupDialog *>(sender());
    if(dialog)
    {
        QStandardItem *groupItem = _clientsListModel->item(group + 1);
        groupItem->setText(dialog->getName());
        groupItem->setIcon(dialog->getIcon());

        _groupsManager->setGroupName(group, dialog->getName());
        _groupsManager->setGroupIcon(group, dialog->getIcon());
        _groupsManager->save();
    }
    else
    {
        qCritical() << "Sender is not a ConfigureGroupDialog" << sender();
    }
}

void MainWindow::appendGroupItem(const QString &name, const QIcon &icon)
{
    QStandardItem *item = new QStandardItem(icon, name);
    item->setDragEnabled(false);
    item->setDropEnabled(true);
    _clientsListModel->appendRow(item);

    QModelIndex index = _ui->listClients->model()->index(_clientsListModel->rowCount() - 1, 0);
    _ui->listClients->expand(index);
}

QStandardItem *MainWindow::findClientItem(const QString &identifier)
{
    QModelIndexList items = _clientsListModel->match(_clientsListModel->index(0, 0),
                                                     Qt::DisplayRole, identifier, 1,
                                                     Qt::MatchFixedString | Qt::MatchCaseSensitive | Qt::MatchRecursive);

    if(items.count() > 0)
    {
        return _clientsListModel->itemFromIndex(items[0]);
    }

    return nullptr;
}

void MainWindow::onButtonConnectClick()
{
    if(_endPoint)
    {
        _endPoint->stopSession();
    }
    else if(NetHelpSettings().getServerHost().isEmpty())
    {
        connect(displayConfig(), &QDialog::accepted, this, &MainWindow::connectToServer);
    }
    else
    {
        connectToServer();
    }
}

QDialog *MainWindow::displayConfig()
{
    ViewerConfigurationDialog *dialog = new ViewerConfigurationDialog(this);

    connect(dialog, &ViewerConfigurationDialog::finished,
            dialog, &ViewerConfigurationDialog::deleteLater);

    dialog->setModal(true);
    dialog->show();

    return dialog;
}

void MainWindow::onButtonTakeControlClick()
{
    QModelIndexList selectedItems = _ui->listClients->selectionModel()->selectedIndexes();
    if(selectedItems.count() == 0)
    {
        MessageBox::critical(this, tr("Please select the client you want to take the control of"));
    }
    else if(selectedItems[0].data(Qt::UserRole + 1).toBool())
    {
        MessageBox::critical(this,
tr("Unable to take the control of the client because it is already controlled by an other viewer"));
    }
    else
    {
        QString password = QInputDialog::getText(this, tr("Password"),
                                     tr("Please enter the session password given by the customer"));
        if(!password.isEmpty())
        {
            QString clientId = selectedItems[0].data().toString();
            _endPoint->takeControl(clientId, password, _clientsManager->getParams(clientId));
        }
    }
}

void MainWindow::onTakeControlResult(bool success)
{
    if(success)
    {
        QModelIndexList selectedItems = _ui->listClients->selectionModel()->selectedIndexes();
        QString clientId = selectedItems[0].data().toString();
        QIcon clientIcon = selectedItems[0].data(Qt::DecorationRole).value<QIcon>();

        QString group;
        if(selectedItems[0].parent().row() > 0)
        {
            group = selectedItems[0].parent().data(Qt::DisplayRole).toString();
        }

        RemoteControlWindow *rcWindow = new RemoteControlWindow(_endPoint,
                                                                group,
                                                                clientId,
                                                                clientIcon,
                                                                _clientsManager->getParams(clientId),
                                                                this);

        connect(rcWindow, &RemoteControlWindow::destroyed,
                this,     &MainWindow::show);

        rcWindow->show();
        rcWindow->raise();
        rcWindow->activateWindow();

        hide();
    }
    else
    {
        MessageBox::critical(this,
                tr("Unable to take the control of the client : please check the entered password"));
    }
}

void MainWindow::onListSelectionChanged()
{
    QModelIndexList selectedIndexes = _ui->listClients->selectionModel()->selectedIndexes();
    _ui->buttonTakeControl->setEnabled(selectedIndexes.count() == 1 &&
                                       selectedIndexes[0].parent().isValid());
}

void MainWindow::onSessionOpenEnd(const QString &/*identifier*/,
                                  const QString &/*password*/,
                                  const QString &errorMsg)
{
    if(!errorMsg.isEmpty())
    {
        MessageBox::critical(this, tr("Error when connecting to server : %1").arg(errorMsg));
    }
    else
    {
        clearClients();
        updateConnectedStatus();
    }
}

void MainWindow::onClientParams(const QString &clientId, const ClientEndPointParams &params)
{
    QIcon icon = QIcon(QPixmap::fromImage(QImage::fromData(params.iconData, "PNG")));

    QStandardItem *item = findClientItem(clientId);
    if(item)
    {
        item->setIcon(icon);
    }
}

void MainWindow::onConnectionLost()
{
    MessageBox::critical(this, tr("The connection with the server has been lost"));
}

void MainWindow::updateConnectedStatus()
{
    if(_endPoint && _endPoint->isSessionRunning())
    {
        _ui->buttonConnect->setText(tr("Disconnect"));
        _ui->buttonConnect->setIcon(QIcon(":/icons/stop_64.png"));
        _ui->groupBoxClients->setEnabled(true);
    }
    else
    {
        _ui->buttonConnect->setText(tr("Connect"));
        _ui->buttonConnect->setIcon(QIcon(":/icons/play_64.png"));
        _ui->groupBoxClients->setEnabled(false);
        clearClients();
    }
}

void MainWindow::onNewClient(const QString &identifier, bool remoteControlled)
{
    QStandardItem *item = new QStandardItem(identifier);
    _endPoint->requestClientParams(identifier);

    item->setData(remoteControlled, Qt::UserRole + 1);
    item->setDragEnabled(true);
    item->setDropEnabled(false);

    int group = _groupsManager->groupFromMember(identifier);
    QStandardItem *groupItem = _clientsListModel->item(group + 1);

    groupItem->setChild(groupItem->rowCount(), item);
}

void MainWindow::onClientControlChanged(const QString &identifier, bool remoteControlled)
{
    QStandardItem *item = findClientItem(identifier);
    if(item)
    {
        item->setData(remoteControlled, Qt::UserRole + 1);
    }
}

void MainWindow::onClientRemoved(const QString &identifier)
{
    QStandardItem *item = findClientItem(identifier);
    if(item)
    {
        delete item->parent()->takeRow(item->row())[0];
    }
}

void MainWindow::onRequestTreeMenu(const QPoint &pos)
{
    QAction *action = nullptr;
    QModelIndex index = _ui->listClients->indexAt(pos);
    if(index.isValid())
    {
        if(index.parent().isValid())
        {
            // User clicked on a client icon
        }
        else
        {
            // User clicked on a group icon
            QMenu menu(_ui->listClients);

            menu.addAction(_ui->actionEditGroup);
            menu.addAction(_ui->actionAddGroup);
            menu.addAction(_ui->actionDeleteGroup);

            _ui->actionDeleteGroup->setEnabled(index.row() != 0);
            _ui->actionEditGroup->setEnabled(index.row() != 0);

            #warning bad
            action = menu.exec(_ui->listClients->mapToGlobal(pos));
        }
    }
    else
    {
        // User clicked around the icons
        QMenu menu(_ui->listClients);

        menu.addAction(_ui->actionAddGroup);

        #warning bad
        action = menu.exec(_ui->listClients->mapToGlobal(pos));
    }

    if(action == _ui->actionAddGroup)
    {
        addGroup();
    }
    else if(action == _ui->actionDeleteGroup)
    {
        deleteGroup(index.row() - 1);
    }
    else if(action == _ui->actionEditGroup)
    {
        editGroup(index.row() - 1);
    }
}

void MainWindow::onClientsLayoutChanged()
{
    // Only rebuild the affectations of present clients
    QStringList presentClients = _clientsManager->getClientsIdentifiers();
    bool somethingChanged = false;

    for(int group=0 ; group<_groupsManager->count() ; group++)
    {
        QStandardItem *groupItem = _clientsListModel->item(group + 1);
        QStringList currentMembers = _groupsManager->getGroupMembers(group);
        QStringList affectedMembers;
        for(int row=0 ; row<groupItem->rowCount() ; row++)
        {
            affectedMembers << groupItem->child(row)->text();
        }

        QStringList newMembers;
        for(const QString &currentMember: currentMembers)
        {
            if(presentClients.contains(currentMember))
            {
                // Remove clients which are no more in the group
                if(!affectedMembers.contains(currentMember))
                {
                    somethingChanged = true;
                }
                else
                {
                    newMembers << currentMember;
                    affectedMembers.removeAll(currentMember);
                }
            }
            else
            {
                // Client is not currently present, just transfer it
                newMembers << currentMember;
            }
        }

        // Add new clients
        if(!affectedMembers.isEmpty())
        {
            newMembers << affectedMembers;
            somethingChanged = true;
        }

        _groupsManager->setGroupMembers(group, newMembers);
    }

    if(somethingChanged)
    {
        _groupsManager->save();
    }
}

void MainWindow::onItemDoubleClicked(const QModelIndex &index)
{
    if(index.parent().isValid())
    {
        // This is a client
        onButtonTakeControlClick();
    }
    else if(index.row() > 0)
    {
        // This is an editable group
        editGroup(index.row() - 1);
    }
}

void MainWindow::onEndPointOver()
{
    if(_endPoint)
    {
        _endPoint->deleteLater();
        _endPoint = nullptr;

        updateConnectedStatus();
    }
}

void MainWindow::connectToServer()
{
    NetHelpSettings settings;

    _endPoint = new NetHelpWorkerViewer(settings.getServerHost(), settings.getProxy(), {}, this);

    connect(_endPoint, &NetHelpWorkerViewer::openSessionEnd,
            this,      &MainWindow::onSessionOpenEnd);
    connect(_endPoint, &NetHelpWorkerViewer::clientParams,
            this,      &MainWindow::onClientParams);
    connect(_endPoint, &NetHelpWorkerViewer::connectionLost,
            this,      &MainWindow::onConnectionLost);
    connect(_endPoint, &NetHelpWorkerViewer::takeControlResult,
            this,      &MainWindow::onTakeControlResult);
    connect(_endPoint, &NetHelpWorkerViewer::over,
            this,      &MainWindow::onEndPointOver);
    connect(_endPoint,       &NetHelpWorkerViewer::clientsList,
            _clientsManager, &ClientsManager::onClientsListUpdate);
    connect(_endPoint,       &NetHelpWorkerViewer::clientParams,
            _clientsManager, &ClientsManager::onClientParamsReceived);

    _endPoint->startSession();
}
