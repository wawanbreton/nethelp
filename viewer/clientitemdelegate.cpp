/*! @file   clientitemdelegate.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "clientitemdelegate.h"

#include <QPainter>
#include <QApplication>
#include <QDebug>


ClientItemDelegate::ClientItemDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{
}

void ClientItemDelegate::paint(QPainter *painter,
                               const QStyleOptionViewItem &option,
                               const QModelIndex &index) const
{
    // Let the style draw the selection box
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &option, painter, nullptr);

    QRect textRect = option.rect;
    QIcon icon = index.data(Qt::DecorationRole).value<QIcon>();

    if(icon.isNull() && !index.parent().isValid())
    {
        // This is a group without an icon, draw the default group icon
        icon = QIcon(":/icons/group_32.png");
    }
    // If the icon is present, draw it
    if(!icon.isNull())
    {
        QPixmap pixmap;
        QIcon::Mode mode = QIcon::Normal;
        if(!index.parent().isValid() && !index.model()->index(0, 0, index).isValid())
        {
            // This is an icon of a group which is currently empty, so draw it disabled
            mode = QIcon::Disabled;
        }
        pixmap = icon.pixmap(QSize(32, 32), mode);

        // Set a delta in case the icon is a rectangle, to draw it centered inside the square
        QPoint delta;
        if(pixmap.width() < 32)
        {
            delta.setX((32 - pixmap.width()) / 2);
        }
        if(pixmap.height() < 32)
        {
            delta.setY((32 - pixmap.height()) / 2);
        }

        painter->drawPixmap(option.rect.topLeft() + QPoint(6, 6) + delta, pixmap);

        textRect.adjust(pixmap.width() + 14, 0, 0, 0);

        // If the client is currently under control, display a mark over the icon
        if(index.data(Qt::UserRole + 1).toBool())
        {
            QPixmap pixmap = QPixmap(":/icons/busy_16.png");
            painter->drawPixmap(option.rect.topLeft() + QPoint(22, 22), pixmap);
        }
    }

    // Draw the client name
    painter->drawText(textRect, Qt::AlignLeft | Qt::AlignVCenter,
                      index.data(Qt::DisplayRole).toString());
}
