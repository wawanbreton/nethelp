#include "noexpanditemstyle.h"

#include <QStyleOptionViewItem>


void NoExpandItemStyle::drawPrimitive(QStyle::PrimitiveElement element,
                                      const QStyleOption *option,
                                      QPainter *painter,
                                      const QWidget *widget) const
{
    if(element == QStyle::PE_IndicatorBranch)
    {
        const QStyleOptionViewItem *optionItem =
                qstyleoption_cast<const QStyleOptionViewItem *>(option);
        QStyleOptionViewItem newOption(*optionItem);

        newOption.state &= ~(QStyle::State_Children);

        baseStyle()->drawPrimitive(element, &newOption, painter, widget);
    }
    else
    {
        baseStyle()->drawPrimitive(element, option, painter, widget);
    }
}
