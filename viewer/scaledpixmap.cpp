/*! @file   scaledpixmap.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "scaledpixmap.h"

#include <QPainter>


ScaledPixmap::ScaledPixmap(QWidget *parent) :
    QWidget(parent),
    _image()
{
}

void ScaledPixmap::paintEvent(QPaintEvent *)
{
    if(!_image.isNull())
    {
        QPainter painter(this);

        qreal ratio = width() / ((qreal)height());
        qreal imageRatio = _image.width() / ((qreal)_image.height());

        if(ratio == imageRatio)
        {
            painter.drawImage(rect(), _image);
        }
        else if(ratio > imageRatio)
        {
            int imageWidth = height() * imageRatio;
            painter.drawImage(QRect((width() - imageWidth) / 2, 0, imageWidth, height()), _image);
        }
        else
        {
            int imageHeight = width() / imageRatio;
            painter.drawImage(QRect(0, (height() - imageHeight) / 2, width(), imageHeight), _image);
        }
    }
}
