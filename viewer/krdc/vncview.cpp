/****************************************************************************
**
** Copyright (C) 2007-2008 Urs Wolfer <uwolfer @ kde.org>
**
** This file is part of KDE.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; see the file COPYING. If not, write to
** the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
** Boston, MA 02110-1301, USA.
**
****************************************************************************/

#include "vncview.h"

#ifdef QTONLY
    #include <QMessageBox>
    #include <QInputDialog>
    #define KMessageBox QMessageBox
#else
    #include "settings.h"
    #include <KActionCollection>
    #include <KMainWindow>
    #include <KMessageBox>
    #include <KPasswordDialog>
    #include <KXMLGUIClient>
#endif

#include <QApplication>
#include <QImage>
#include <QPainter>
#include <QMouseEvent>

// Definition of key modifier mask constants
#define KMOD_Alt_R 	0x01
#define KMOD_Alt_L 	0x02
#define KMOD_Meta_L 	0x04
#define KMOD_Control_L 	0x08
#define KMOD_Shift_L	0x10

VncView::VncView(QWidget *parent, const KUrl &url, KConfigGroup configGroup)
        : RemoteView(parent),
        m_initDone(false),
        m_buttonMask(0),
        m_repaint(false),
        m_quitFlag(false),
        m_firstPasswordTry(true),
        m_authenticaionCanceled(false),
        m_dontSendClipboard(false),
        m_horizontalFactor(1.0),
        m_verticalFactor(1.0),
        m_forceLocalCursor(false)
{
    m_url = url;
    m_host = url.host();
    m_port = url.port();

    connect(&vncThread, SIGNAL(imageUpdated(int, int, int, int)), this, SLOT(updateImage(int, int, int, int)), Qt::QueuedConnection);
    connect(&vncThread, SIGNAL(gotCut(const QString&)), this, SLOT(setCut(const QString&)), Qt::QueuedConnection);
    connect(&vncThread, SIGNAL(passwordRequest()), this, SLOT(requestPassword()), Qt::QueuedConnection);
    connect(&vncThread, SIGNAL(outputErrorMessage(QString)), this, SLOT(outputErrorMessage(QString)));

    m_clipboard = QApplication::clipboard();
    connect(m_clipboard, SIGNAL(selectionChanged()), this, SLOT(clipboardSelectionChanged()));
    connect(m_clipboard, SIGNAL(dataChanged()), this, SLOT(clipboardDataChanged()));
    
#ifndef QTONLY
    m_hostPreferences = new VncHostPreferences(configGroup, this);
#else
    Q_UNUSED(configGroup);
#endif
}

VncView::~VncView()
{
    startQuitting();
}

bool VncView::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress ||
        event->type() == QEvent::KeyRelease ||
        event->type() == QEvent::MouseButtonDblClick ||
        event->type() == QEvent::MouseButtonPress ||
        event->type() == QEvent::MouseButtonRelease ||
        event->type() == QEvent::Wheel ||
        event->type() == QEvent::MouseMove)
    {
        return true;
    }

    return RemoteView::eventFilter(obj, event);
}

QSize VncView::framebufferSize()
{
    return m_frame.size();
}

QSize VncView::sizeHint() const
{
    return size();
}

QSize VncView::minimumSizeHint() const
{
    return size();
}

void VncView::scaleResize(int w, int h)
{
    RemoteView::scaleResize(w, h);
    
    if (m_scale) {
        m_verticalFactor = (qreal) h / m_frame.height();
        m_horizontalFactor = (qreal) w / m_frame.width();

#ifndef QTONLY
        if (Settings::keepAspectRatio()) {
            m_verticalFactor = m_horizontalFactor = qMin(m_verticalFactor, m_horizontalFactor);
        }
#else
        m_verticalFactor = m_horizontalFactor = qMin(m_verticalFactor, m_horizontalFactor);
#endif

        const qreal newW = m_frame.width() * m_horizontalFactor;
        const qreal newH = m_frame.height() * m_verticalFactor;
        setMaximumSize(newW, newH); //This is a hack to force Qt to center the view in the scroll area
        resize(newW, newH);
    } 
}

void VncView::updateConfiguration()
{
    RemoteView::updateConfiguration();

    // Update the scaling mode in case KeepAspectRatio changed
    scaleResize(parentWidget()->width(), parentWidget()->height());
}

void VncView::startQuitting()
{
    setStatus(Disconnecting);

    unpressModifiers();

    // Disconnect all signals so that we don't get any more callbacks from the client thread
    disconnect(&vncThread, SIGNAL(imageUpdated(int, int, int, int)), this, SLOT(updateImage(int, int, int, int)));
    disconnect(&vncThread, SIGNAL(gotCut(const QString&)), this, SLOT(setCut(const QString&)));
    disconnect(&vncThread, SIGNAL(passwordRequest()), this, SLOT(requestPassword()));
    disconnect(&vncThread, SIGNAL(outputErrorMessage(QString)), this, SLOT(outputErrorMessage(QString)));

    m_quitFlag = true;

    if (vncThread.isRunning()) {
        vncThread.stop();
    }

    vncThread.wait(1000);

    setStatus(Disconnected);
}

bool VncView::isQuitting()
{
    return m_quitFlag;
}

bool VncView::start()
{
    vncThread.setHost(m_host);
    vncThread.setPort(m_port);
    RemoteView::Quality quality = (RemoteView::Quality)((QCoreApplication::arguments().count() > 2) ?
        QCoreApplication::arguments().at(2).toInt() : m_quality);

    vncThread.setQuality(quality);

    // set local cursor on by default because low quality mostly means slow internet connection
    if (quality == RemoteView::Low) {
        showDotCursor(RemoteView::CursorOn);
    }

    setStatus(Connecting);

    vncThread.start();
    return true;
}

bool VncView::supportsScaling() const
{
    return true;
}

bool VncView::supportsLocalCursor() const
{
    return true;
}

void VncView::requestPassword()
{
    if (m_authenticaionCanceled) {
        startQuitting();
        return;
    }

    setStatus(Authenticating);


    if (!m_url.password().isNull()) {
        vncThread.setPassword(m_url.password());
        return;
    }

    bool ok;
    QString password = QInputDialog::getText(this, //krazy:exclude=qclasses
                                             tr("Password required"),
                                             tr("Please enter the password for the remote desktop:"),
                                             QLineEdit::Password, QString(), &ok);
    m_firstPasswordTry = false;
    if (ok)
        vncThread.setPassword(password);
    else
        m_authenticaionCanceled = true;
}

void VncView::outputErrorMessage(const QString &message)
{
    if (message == "INTERNAL:APPLE_VNC_COMPATIBILTY") {
        setCursor(localDotCursor());
        m_forceLocalCursor = true;
        return;
    }

    emit errorMessage(i18n("VNC failure"), message);
}

void VncView::updateImage(int x, int y, int w, int h)
{
    m_x = x;
    m_y = y;
    m_w = w;
    m_h = h;

    if (m_horizontalFactor != 1.0 || m_verticalFactor != 1.0) {
        // If the view is scaled, grow the update rectangle to avoid artifacts
        m_x-=1;
        m_y-=1;
        m_w+=2;
        m_h+=2;
    }

    m_frame = vncThread.image();

    if (!m_initDone) {
        setAttribute(Qt::WA_StaticContents);
        setAttribute(Qt::WA_OpaquePaintEvent);

        if(m_viewOnly)
        {
            installEventFilter(this);
        }

        setCursor(((m_dotCursorState == CursorOn) || m_forceLocalCursor) ? localDotCursor() : Qt::BlankCursor);

        //setMouseTracking(true); // get mouse events even when there is no mousebutton pressed
        // EMA : don't get mouse move events because we don't care

        setFocusPolicy(Qt::WheelFocus);
        setStatus(Connected);
        emit connected();
        
        if (m_scale) {
            //TODO: qtonly alternative
        }

        m_initDone = true;

    }

    if ((y == 0 && x == 0) && (m_frame.size() != size())) {
        if (m_scale) {
            setMaximumSize(QSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX));
            if (parentWidget())
                scaleResize(parentWidget()->width(), parentWidget()->height());
        } else {
            resize(m_frame.width(), m_frame.height());
            setMaximumSize(m_frame.width(), m_frame.height()); //This is a hack to force Qt to center the view in the scroll area
            setMinimumSize(m_frame.width(), m_frame.height());
            emit framebufferSizeChanged(m_frame.width(), m_frame.height());
        }
    }

    m_repaint = true;
    repaint(qRound(m_x * m_horizontalFactor), qRound(m_y * m_verticalFactor), qRound(m_w * m_horizontalFactor), qRound(m_h * m_verticalFactor));
    m_repaint = false;
}

void VncView::setViewOnly(bool viewOnly)
{
    RemoteView::setViewOnly(viewOnly);

    m_dontSendClipboard = viewOnly;

    if (viewOnly)
        setCursor(Qt::ArrowCursor);
    else
        setCursor(m_dotCursorState == CursorOn ? localDotCursor() : Qt::BlankCursor);
}

void VncView::showDotCursor(DotCursorState state)
{
    RemoteView::showDotCursor(state);

    setCursor(state == CursorOn ? localDotCursor() : Qt::BlankCursor);
}

void VncView::enableScaling(bool scale)
{
    RemoteView::enableScaling(scale);

    if (scale) {
        setMaximumSize(QSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX));
        setMinimumSize(1, 1);
        if (parentWidget())
            scaleResize(parentWidget()->width(), parentWidget()->height());
    } else {
        m_verticalFactor = 1.0;
        m_horizontalFactor = 1.0;

        setMaximumSize(m_frame.width(), m_frame.height()); //This is a hack to force Qt to center the view in the scroll area
        setMinimumSize(m_frame.width(), m_frame.height());
        resize(m_frame.width(), m_frame.height());
    }
}

void VncView::setCut(const QString &text)
{
    m_dontSendClipboard = true;
    m_clipboard->setText(text, QClipboard::Clipboard);
    m_clipboard->setText(text, QClipboard::Selection);
    m_dontSendClipboard = false;
}

void VncView::paintEvent(QPaintEvent *event)
{
    if (m_frame.isNull() || m_frame.format() == QImage::Format_Invalid) {
        RemoteView::paintEvent(event);
        return;
    }

    event->accept();

    QPainter painter(this);

    if (m_repaint) {
        painter.drawImage(QRect(qRound(m_x*m_horizontalFactor), qRound(m_y*m_verticalFactor),
                                qRound(m_w*m_horizontalFactor), qRound(m_h*m_verticalFactor)),
                          m_frame.copy(m_x, m_y, m_w, m_h).scaled(qRound(m_w*m_horizontalFactor),
                                                                  qRound(m_h*m_verticalFactor),
                                                                  Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
    } else {
        QRect rect = event->rect();
        if (rect.width() != width() || rect.height() != height()) {
            const int sx = rect.x()/m_horizontalFactor;
            const int sy = rect.y()/m_verticalFactor;
            const int sw = rect.width()/m_horizontalFactor;
            const int sh = rect.height()/m_verticalFactor;
            painter.drawImage(rect, 
                              m_frame.copy(sx, sy, sw, sh).scaled(rect.width(), rect.height(),
                                                                  Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
        } else {
            painter.drawImage(QRect(0, 0, width(), height()), 
                              m_frame.scaled(m_frame.width() * m_horizontalFactor, m_frame.height() * m_verticalFactor,
                                             Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
        }
    }

    RemoteView::paintEvent(event);
}

void VncView::resizeEvent(QResizeEvent *event)
{
    RemoteView::resizeEvent(event);
    update();
}

bool VncView::event(QEvent *event)
{
    switch (event->type()) {
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
        keyEventHandler(static_cast<QKeyEvent*>(event));
        return true;
        break;
    case QEvent::MouseButtonDblClick:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
        mouseEventHandler(static_cast<QMouseEvent*>(event));
        return true;
        break;
    case QEvent::Wheel:
        wheelEventHandler(static_cast<QWheelEvent*>(event));
        return true;
        break;
    default:
        return RemoteView::event(event);
    }
}

void VncView::mouseEventHandler(QMouseEvent *e)
{
    if (e->type() != QEvent::MouseMove) {
        if ((e->type() == QEvent::MouseButtonPress) ||
                (e->type() == QEvent::MouseButtonDblClick)) {
            if (e->button() & Qt::LeftButton)
                m_buttonMask |= 0x01;
            if (e->button() & Qt::MidButton)
                m_buttonMask |= 0x02;
            if (e->button() & Qt::RightButton)
                m_buttonMask |= 0x04;
        } else if (e->type() == QEvent::MouseButtonRelease) {
            if (e->button() & Qt::LeftButton)
                m_buttonMask &= 0xfe;
            if (e->button() & Qt::MidButton)
                m_buttonMask &= 0xfd;
            if (e->button() & Qt::RightButton)
                m_buttonMask &= 0xfb;
        }
    }

    vncThread.mouseEvent(qRound(e->x() / m_horizontalFactor), qRound(e->y() / m_verticalFactor), m_buttonMask);
}

void VncView::wheelEventHandler(QWheelEvent *event)
{
    int eb = 0;
    if (event->delta() < 0)
        eb |= 0x10;
    else
        eb |= 0x8;

    const int x = qRound(event->x() / m_horizontalFactor);
    const int y = qRound(event->y() / m_verticalFactor);

    vncThread.mouseEvent(x, y, eb | m_buttonMask);
    vncThread.mouseEvent(x, y, m_buttonMask);
}

void VncView::keyEventHandler(QKeyEvent *e)
{
    // strip away autorepeating KeyRelease; see bug #206598
    if (e->isAutoRepeat() && (e->type() == QEvent::KeyRelease))
        return;

    // parts of this code are based on http://italc.sourcearchive.com/documentation/1.0.9.1/vncview_8cpp-source.html

    // hmm, either Win32-platform or too old Qt so we have to handle and
    // translate Qt-key-codes to X-keycodes
    rfbKeySym key = 0;
    switch( e->key() )
    {
        // modifiers are handled separately
        case Qt::Key_Shift: key = XK_Shift_L; break;
        case Qt::Key_Control: key = XK_Control_L; break;
        case Qt::Key_Meta: key = XK_Meta_L; break;
        case Qt::Key_Alt: key = XK_Alt_L; break;
        case Qt::Key_Escape: key = XK_Escape; break;
        case Qt::Key_Tab: key = XK_Tab; break;
        case Qt::Key_Backtab: key = XK_Tab; break;
        case Qt::Key_Backspace: key = XK_BackSpace; break;
        case Qt::Key_Return: key = XK_Return; break;
        case Qt::Key_Insert: key = XK_Insert; break;
        case Qt::Key_Delete: key = XK_Delete; break;
        case Qt::Key_Pause: key = XK_Pause; break;
        case Qt::Key_Print: key = XK_Print; break;
        case Qt::Key_Home: key = XK_Home; break;
        case Qt::Key_End: key = XK_End; break;
        case Qt::Key_Left: key = XK_Left; break;
        case Qt::Key_Up: key = XK_Up; break;
        case Qt::Key_Right: key = XK_Right; break;
        case Qt::Key_Down: key = XK_Down; break;
        case Qt::Key_PageUp: key = XK_Prior; break;
        case Qt::Key_PageDown: key = XK_Next; break;
        case Qt::Key_CapsLock: key = XK_Caps_Lock; break;
        case Qt::Key_NumLock: key = XK_Num_Lock; break;
        case Qt::Key_ScrollLock: key = XK_Scroll_Lock; break;
        case Qt::Key_Super_L: key = XK_Super_L; break;
        case Qt::Key_Super_R: key = XK_Super_R; break;
        case Qt::Key_Menu: key = XK_Menu; break;
        case Qt::Key_Hyper_L: key = XK_Hyper_L; break;
        case Qt::Key_Hyper_R: key = XK_Hyper_R; break;
        case Qt::Key_Help: key = XK_Help; break;
        case Qt::Key_AltGr: key = XK_ISO_Level3_Shift; break;
        case Qt::Key_Multi_key: key = XK_Multi_key; break;
        case Qt::Key_SingleCandidate: key = XK_SingleCandidate; break;
        case Qt::Key_MultipleCandidate: key = XK_MultipleCandidate; break;
        case Qt::Key_PreviousCandidate: key = XK_PreviousCandidate; break;
        case Qt::Key_Mode_switch: key = XK_Mode_switch; break;
        case Qt::Key_Kanji: key = XK_Kanji; break;
        case Qt::Key_Muhenkan: key = XK_Muhenkan; break;
        case Qt::Key_Henkan: key = XK_Henkan; break;
        case Qt::Key_Romaji: key = XK_Romaji; break;
        case Qt::Key_Hiragana: key = XK_Hiragana; break;
        case Qt::Key_Katakana: key = XK_Katakana; break;
        case Qt::Key_Hiragana_Katakana: key = XK_Hiragana_Katakana; break;
        case Qt::Key_Zenkaku: key = XK_Zenkaku; break;
        case Qt::Key_Hankaku: key = XK_Hankaku; break;
        case Qt::Key_Zenkaku_Hankaku: key = XK_Zenkaku_Hankaku; break;
        case Qt::Key_Touroku: key = XK_Touroku; break;
        case Qt::Key_Massyo: key = XK_Massyo; break;
        case Qt::Key_Kana_Lock: key = XK_Kana_Lock; break;
        case Qt::Key_Kana_Shift: key = XK_Kana_Shift; break;
        case Qt::Key_Eisu_Shift: key = XK_Eisu_Shift; break;
        case Qt::Key_Eisu_toggle: key = XK_Eisu_toggle; break;
        #ifdef XK_KOREAN
        case Qt::Key_Hangul: key = XK_Hangul; break;
        case Qt::Key_Hangul_Start: key = XK_Hangul_Start; break;
        case Qt::Key_Hangul_End: key = XK_Hangul_End; break;
        case Qt::Key_Hangul_Hanja: key = XK_Hangul_Hanja; break;
        case Qt::Key_Hangul_Jamo: key = XK_Hangul_Jamo; break;
        case Qt::Key_Hangul_Romaja: key = XK_Hangul_Romaja; break;
        case Qt::Key_Hangul_Jeonja: key = XK_Hangul_Jeonja; break;
        case Qt::Key_Hangul_Banja: key = XK_Hangul_Banja; break;
        case Qt::Key_Hangul_PreHanja: key = XK_Hangul_PreHanja; break;
        case Qt::Key_Hangul_PostHanja: key = XK_Hangul_PostHanja; break;
        case Qt::Key_Hangul_Special: key = XK_Hangul_Special; break;
        #endif
        case Qt::Key_Dead_Grave: key = XK_dead_grave; break;
        case Qt::Key_Dead_Acute: key = XK_dead_acute; break;
        case Qt::Key_Dead_Circumflex: key = XK_dead_circumflex; break;
        case Qt::Key_Dead_Tilde: key = XK_dead_tilde; break;
        case Qt::Key_Dead_Macron: key = XK_dead_macron; break;
        case Qt::Key_Dead_Breve: key = XK_dead_breve; break;
        case Qt::Key_Dead_Abovedot: key = XK_dead_abovedot; break;
        case Qt::Key_Dead_Diaeresis: key = XK_dead_diaeresis; break;
        case Qt::Key_Dead_Abovering: key = XK_dead_abovering; break;
        case Qt::Key_Dead_Doubleacute: key = XK_dead_doubleacute; break;
        case Qt::Key_Dead_Caron: key = XK_dead_caron; break;
        case Qt::Key_Dead_Cedilla: key = XK_dead_cedilla; break;
        case Qt::Key_Dead_Ogonek: key = XK_dead_ogonek; break;
        case Qt::Key_Dead_Iota: key = XK_dead_iota; break;
        case Qt::Key_Dead_Voiced_Sound: key = XK_dead_voiced_sound; break;
        case Qt::Key_Dead_Semivoiced_Sound: key = XK_dead_semivoiced_sound; break;
        case Qt::Key_Dead_Belowdot: key = XK_dead_belowdot; break;
    }

    if( e->key() >= Qt::Key_F1 && e->key() <= Qt::Key_F35 )
    {
        key = XK_F1 + e->key() - Qt::Key_F1;
    }
    else if( key == 0 )
    {
        if( m_mods.contains( XK_Control_L ) &&
              QKeySequence( e->key() ).toString().length() == 1 )
        {
              QString s = QKeySequence( e->key() ).toString();
              if( !m_mods.contains( XK_Shift_L ) )
              {
                    s = s.toLower();
              }
              key = s.utf16()[0];
        }
        else
        {
              key = e->text().utf16()[0];
        }

    }
    // correct translation of AltGr+<character key> (non-US-keyboard layout
    // such as German keyboard layout)
    if( m_mods.contains( XK_Alt_L ) && m_mods.contains( XK_Control_L ) &&
                                key >= 64 && key < 0xF000 )
    {
        unpressModifiers();
        vncThread.keyEvent( XK_ISO_Level3_Shift, true);
    }

    const bool pressed = (e->type() == QEvent::KeyPress);

    // handle modifiers
    if (key == XK_Shift_L || key == XK_Control_L || key == XK_Meta_L || key == XK_Alt_L) {
        if (pressed) {
            m_mods[key] = true;
        } else if (m_mods.contains(key)) {
            m_mods.remove(key);
        } else {
            unpressModifiers();
        }
    }

    if (key) {
        vncThread.keyEvent(key, pressed);
    }
}

void VncView::unpressModifiers()
{
    const QList<unsigned int> keys = m_mods.keys();
    QList<unsigned int>::const_iterator it = keys.constBegin();
    while (it != keys.end()) {
        vncThread.keyEvent(*it, false);
        it++;
    }
    m_mods.clear();
}

void VncView::clipboardSelectionChanged()
{
    if (m_status != Connected)
        return;

    if (m_clipboard->ownsSelection() || m_dontSendClipboard)
        return;

    const QString text = m_clipboard->text(QClipboard::Selection);

    vncThread.clientCut(text);
}

void VncView::clipboardDataChanged()
{
    if (m_status != Connected)
        return;

    if (m_clipboard->ownsClipboard() || m_dontSendClipboard)
        return;

    const QString text = m_clipboard->text(QClipboard::Clipboard);

    vncThread.clientCut(text);
}

#include "moc_vncview.cpp"
