/*! @file   filterclientsproxymodel.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "filterclientsproxymodel.h"

#include <QDebug>


FilterClientsProxyModel::FilterClientsProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    _displayControlled(true)
{
}

bool FilterClientsProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if(!sourceParent.isValid() || _displayControlled)
    {
        return true;
    }
    else
    {
        QModelIndex index = sourceParent.model()->index(sourceRow, 0, sourceParent);
        return !sourceModel()->data(index, Qt::UserRole + 1).toBool();
    }
}
