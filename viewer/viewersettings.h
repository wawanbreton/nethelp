/*! @file   viewersettings.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include "common/nethelpsettings.h"

#include "krdc/remoteview.h"

/*! @brief Contains the NetHelp settings and the viewer-specific stored settings */
class ViewerSettings : public NetHelpSettings
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit ViewerSettings(QObject *parent = nullptr) : NetHelpSettings(parent) {}

        /*! @brief Gets the groups configuration
            @return The groups configuration, which is a string encoded by the caller */
        QString getGroupsConfig() const
        { return value("groups/config", "").toString(); }

        /*! @brief Sets the groups configuration
            @param groupsConfig The groups configuration, which is a string encoded by the caller */
        void setGroupsConfig(const QString &groupsConfig)
        { setValue("groups/config", groupsConfig); }

        /*! @brief Gets the main window saved geometry
            @return The main window saved geometry */
        QByteArray getMainWindowGeometry() const
        { return value("gui/main_window_geometry").toByteArray(); }

        /*! @brief Sets the main window current geometry
            @param geometry The main window current geometry */
        void setMainWindowGeometry(const QByteArray &geometry)
        { setValue("gui/main_window_geometry", geometry); }

        /*! @brief Gets the remote control window saved geometry
            @return The remote control window saved geometry */
        QByteArray getRemoteControlWindowGeometry() const
        { return value("gui/remote_control_window_geometry").toByteArray(); }

        /*! @brief Sets the remote control window current geometry
            @param geometry The remote control window current geometry */
        void setRemoteControlWindowGeometry(const QByteArray &geometry)
        { setValue("gui/remote_control_window_geometry", geometry); }

        /*! @brief Gets the remote control window saved state
            @return The remote control window saved state */
        QByteArray getRemoteControlWindowState() const
        { return value("gui/remote_control_window_state").toByteArray(); }

        /*! @brief Sets the remote control window current state
            @param state The remote control window current state */
        void setRemoteControlWindowState(const QByteArray &state)
        { setValue("gui/remote_control_window_state", state); }

        /*! @brief Gets the sessions data storage folder
            @return The sessions data storage folder */
        QString getStoreDir()
        { return value("store/directory").toString(); }

        /*! @brief Sets the sessions data storage folder
            @param storeDir The sessions data storage folder */
        void setStoreDir(const QString &storeDir)
        { setValue("store/directory", storeDir); }

        /*! @brief Gets the VNC display quality
         *  @return The VNC display quality */
        RemoteView::Quality getDisplayQuality() const
        { return (RemoteView::Quality)value("display/quality", (int)RemoteView::Medium).toInt(); }

        /*! @brief Sets the VNC display quality
         *  @param quality The VNC display quality */
        void setDisplayQuality(RemoteView::Quality quality)
        { setValue("display/quality", (int)quality); }
};
