/*! @file   noexpanditemstyle.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QProxyStyle>

/*! @brief This is just a fake style which only purpose is not to draw the +/- elements
           in a tree view */
class NoExpandItemStyle : public QProxyStyle
{
    public:
        /*! @brief Constructor
            @param baseStyle The base style of the widget, used to draw everything */
        NoExpandItemStyle(QStyle *baseStyle) : QProxyStyle(baseStyle) {}

        /*! @brief Draws a primitive element. This function only calls the base style to do the job,
                   except in the case of drawing a tree branch, where we specify that the node does
                   not have any child, not to draw the +/- element
            @param element The type of element to be drawn
            @param option The options of the element
            @param painter The painter used to draw the element
            @param widget The widget being drawn */
        virtual void drawPrimitive(PrimitiveElement element,
                                   const QStyleOption *option,
                                   QPainter *painter,
                                   const QWidget *widget = nullptr) const override;
};
