/*! @file   browseremotefilesystemdialog.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QDialog>

#include <QStringList>
#include <QStandardItemModel>
#include <QFileIconProvider>
#include <QRegExp>
#include <QItemSelection>

#include "common/fileinfo.h"
#include "common/path.h"

namespace Ui { class BrowseRemoteFileSystemDialog; }

class NetHelpWorkerViewer;

/*! @brief This is a dialog which looks like any standard browse file system dialog, but it browses
           the file system of the remote client */
class BrowseRemoteFileSystemDialog : public QDialog
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param endPoint The object used to communicate with the remote client
            @param clientId The identifier of the remote client
            @param clientIcon The icon of the remote client
            @param parent The parent widget */
        explicit BrowseRemoteFileSystemDialog(NetHelpWorkerViewer *endPoint,
                                              const QString &clientId,
                                              const QIcon &clientIcon,
                                              QWidget *parent = nullptr);

        /*! @brief Destructor, which also destroys the inner Ui object */
        ~BrowseRemoteFileSystemDialog();

        /*! @brief Gets the list of selected files
            @return The list of selected files, which may be empty in case the user cancelled */
        const QList<Path> &getSelectedFiles() { return _selectedFiles; }

    private:
        /*! @brief Rebuilds the model used to display the current files list */
        void rebuildFilesList();

        /*! @brief Rebuilds the model used to display the current path tree */
        void rebuildPath();

        /*! @brief Move to the given path
            @param path The path to move to */
        void moveToPath(const Path &path)
        { _currentPath = path; rebuildFilesList(); rebuildPath(); }

        /*! @brief Slot called when a files list is received from the client
            @param dirPath The path of the directory which has been listed
            @param infos The list of retrieved files informations */
        void onFilesListReceived(const Path &dirPath, const QList<FileInfo> &infos);

        /*! @brief Slot called when the user double-clicked on a file/directory
            @param index The index of the element which has been clicked */
        void onItemDoubleClicked(const QModelIndex &index);

        /*! @brief Slot called when the "move up" button has been clicked */
        void onButtonGoUpClicked()
        { moveToPath(_currentPath.parentDir()); }

        /*! @brief Slot called when an element of the path tree has been selected */
        void onSelectedPathChanged();

        /*! @brief Slot called when the files selection has changed */
        void onSelectedFilesChanged();

        /*! @brief Slot called when the user validates the dialog to download the selected files */
        void onAccept();

    private:
        Ui::BrowseRemoteFileSystemDialog *_ui{nullptr};
        NetHelpWorkerViewer *_endPoint{nullptr};
        Path _currentPath;
        QStandardItemModel *_modelFiles{nullptr};
        QStandardItemModel *_modelPath{nullptr};
        QFileIconProvider _iconProvider;
        QRegExp _driveRegExp;
        QList<Path> _selectedFiles;
};
