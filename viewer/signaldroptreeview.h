/*! @file   signaldroptreeview.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QTreeView>

/*! @brief Simple subclass of treeview which sends a signal on drop event */
class SignalDropTreeView : public QTreeView
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent widget */
        explicit SignalDropTreeView(QWidget *parent = nullptr);

    protected:
        /*! @brief Method called by the Qt engine when something is dropped over the widget
            @param event Object giving informations about the action */
        virtual void dropEvent(QDropEvent *event) override;

    signals:
        /*! @brief Signal emitted whenever something is dropped on the widget */
        void somethingDropped();
};
