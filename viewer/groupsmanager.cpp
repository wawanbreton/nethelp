/*! @file   groupsmanager.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "groupsmanager.h"

#include <QBuffer>
#include <QDebug>

#include "viewer/viewersettings.h"


GroupsManager::GroupsManager() :
    _groups()
{
    QString config = ViewerSettings().getGroupsConfig();

    for(const QString &groupConfig : config.split(";", QString::SkipEmptyParts))
    {
        QStringList groupConfigParts = groupConfig.split(":");
        if(groupConfigParts.count() == 3)
        {
            Group *group = new Group();
            group->name = groupConfigParts[0];
            group->members = QSet<QString>::fromList(groupConfigParts[1].split(",", QString::SkipEmptyParts));

            QString iconString = groupConfigParts[2];
            if(iconString.isEmpty())
            {
                group->icon = QIcon();
            }
            else
            {
                QImage image;
                image.loadFromData(QByteArray::fromBase64(iconString.toUtf8()), "PNG");
                group->icon = QIcon(QPixmap::fromImage(image));
            }

            _groups << group;
        }
        else
        {
            qWarning() << "Malformed group configuration string";
        }
    }
}

int GroupsManager::addGroup()
{
    Group *group = new Group();
    group->name = QObject::tr("Group");
    group->members = QSet<QString>();
    group->icon = QIcon();

    _groups << group;

    save();

    return _groups.count() - 1;
}

void GroupsManager::removeGroup(int group)
{
    Group *groupPtr = _groups[group];
    _groups.removeAt(group);

    delete groupPtr;

    save();
}

int GroupsManager::groupFromMember(const QString &member) const
{
    for(int group=0 ; group<_groups.count() ; group++)
    {
        if(_groups[group]->members.contains(member))
        {
            return group;
        }
    }

    return -1;
}

void GroupsManager::save() const
{
    QStringList groups;

    for(const Group *group : _groups)
    {
        QStringList groupConf;

        groupConf << group->name;
        groupConf << QStringList(group->members.toList()).join(",");
        if(group->icon.isNull())
        {
            groupConf << "";
        }
        else
        {
            QBuffer iconBuffer;
            group->icon.pixmap(32).toImage().save(&iconBuffer, "PNG");
            groupConf << iconBuffer.data().toBase64();
        }

        groups << groupConf.join(":");
    }

    ViewerSettings().setGroupsConfig(groups.join(";"));
}
