/*! @file   clientsmanager.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QObject>

#include <QPair>
#include <QIcon>

#include "common/clientendpointparams.h"
#include "common/stringboolpair.h"

/*! @brief Manages the list of connected clients and their status */
class ClientsManager : public QObject
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit ClientsManager(QObject *parent = nullptr);

        /*! @brief Gets the list of current clients identifiers
            @return The list of current clients identifiers */
        QStringList getClientsIdentifiers() const
        { return _clients.keys(); }

        ClientEndPointParams getParams(const QString &identifier) const;

        /*! @brief Slot to be called when the list of clients changes on the server
            @param clients The clients list , containing the name of the client and a bool
                           which is true in case the client is currently under control */
        void onClientsListUpdate(const QList<StringBoolPair> &clients);

        void onClientParamsReceived(const QString &identifier, const ClientEndPointParams &params);

    signals:
        /*! @brief Signal emitted when a new client is connected to the server
            @param identifier The identifier of the newly connected cliend
            @param remoteControlled Indicates whether the client is currently controlled by an
                                    other viewer */
        void newClient(const QString &identifier, bool remoteControlled);

        /*! @brief Signal emitted when a client's controlled status changes
            @param identifier The identifier of the client which status changed
            @param remoteControlled True if the client is now controlled by an other viewer */
        void clientControlChanged(const QString &identifier, bool remoteControlled);

        /*! @brief Signal emitted when a client disconnects from the server
            @param identifier The identifier of the client which disconnected */
        void clientRemoved(const QString &identifier);

    private:
        typedef struct
        {
            bool remoteControlled;
            ClientEndPointParams params;
        } ClientData;

    private:
        QMap<QString, ClientData> _clients;
};
