/*! @file   clientitemdelegate.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QAbstractItemDelegate>

/*! @brief Delegate displaying the available clients in a list. It actually displays the icons of
           the clients, their name and a smaller icon on top of the other one in case the client is
           already under control by a client */
class ClientItemDelegate : public QAbstractItemDelegate
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param parent The parent container */
        explicit ClientItemDelegate(QObject *parent = nullptr);

        /*! @brief Paints the area for the given item
            @param painter The painter to be used to draw the informations
            @param option Contains various informations about how the item should be displayed
            @param index The index of the item to be displayed */
        virtual void paint(QPainter *painter,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;

        /*! @brief Gets the preferred size for the given element
            @return The preferred size for the given element, which is invariant */
        virtual QSize sizeHint(const QStyleOptionViewItem &/*option*/,
                                      const QModelIndex &/*index*/) const override
        { return QSize(-1, 32 + 12); }
};
