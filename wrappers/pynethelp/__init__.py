# -*- coding: utf-8 -*-

import os
from ctypes import CDLL
  
if os.name == 'nt':
  sLibNetHelp = "nethelpclient-c.dll"
else:
  sLibNetHelp = "libnethelpclient-c.so"

try:
  libnethelpclient = CDLL(sLibNetHelp)
except Exception, oEx:
  print "Error when loading NetHelpClient library (%s) : %s" % (sLibNetHelp, str(oEx))
  libnethelpclient = None

def _convertToRawVector(voValues, oType):
  return (oType*len(voValues))(*[oType(oValue) for oValue in voValues])

def standaloneInit():
  libnethelpclient.NHStandaloneInit()

def keepAlive():
  libnethelpclient.NHKeepMeAlive()

def _exec():
  return libnethelpclient.NHExec()

def exit():
  libnethelpclient.NHExit()
