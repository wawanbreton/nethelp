#!/usr/bin/env python
# -*- coding: utf-8 -*-
# nethelpclient.py
# 
#
# This file is part of NetHelp.
#
# NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
# Lesser General Public License as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with NetHelp.
# If not, see <http:#www.gnu.org/licenses/lgpl.html>

import pynethelp
from pynethelp import libnethelpclient
from ctypes import *


class NetHelpClient:
  
  def __init__(self):
    self._oClient = libnethelpclient.NHNewClient()
    self._oOpenSessionEndCb = None
    self._oControlTakenCb = None
    self._oControlReleasedCb = None
    
  def __del__(self):
    libnethelpclient.NHDeleteClient(self._oClient)
    
  def setServerUrl(self, uUrl):
    libnethelpclient.NHSetServerUrl(self._oClient, uUrl)
    
  def setProxy(self, bProxy, uHostName, dPort, uUser, uPassword):
    libnethelpclient.NHSetProxy(self._oClient, int(bProxy), uHostName, dPort, uUser, uPassword)
    
  def startSession(self, sIconData, uDesiredIdentifier):
    libnethelpclient.NHStartSession(self._oClient, 
                                    pynethelp._convertToRawVector(sIconData, c_char), 
                                    len(sIconData), 
                                    uDesiredIdentifier)
    
  def stopSession(self):
    libnethelpclient.NHStopSession(self._oClient)
    
  def isSessionRunning(self):
    return bool(libnethelpclient.NHIsSessionRunning(self._oClient))
  
  def getSessionId(self):
    libnethelpclient.NHGetSessionId.restype = c_wchar_p
    return libnethelpclient.NHGetSessionId(self._oClient)
  
  def getSessionPassword(self):
    libnethelpclient.NHGetSessionPassword.restype = c_wchar_p
    return libnethelpclient.NHGetSessionPassword(self._oClient)
  
  def isRemoteControlTaken(self):
    return bool(libnethelpclient.NHIsRemoteControlTaken(self._oClient))
  
  def startVncClientProxy(self):
    libnethelpclient.NHStartVncClientProxy(self._oClient)
    
  def stopVncClientProxy(self):
    libnethelpclient.NHStopVncClientProxy(self._oClient)
    
  def sendInformation(self, uInformation):
    libnethelpclient.NHSendInformation(self._oClient, uInformation)
    
  def sendDescription(self, uDescription):
    libnethelpclient.NHSendDescription(self._oClient, uDescription)
    
  def registerInterestingFiles(self, uCategoryName, vuFiles):
    libnethelpclient.NHRegisterInterestingFiles(self._oClient, uCategoryName, '\n'.join(vuFiles))
    
  def registerOpenSessionEndCb(self, fOpenSessionEndCb):
    if fOpenSessionEndCb is None:
      self._oOpenSessionEndCb = None
    else:
      oCallbackType = CFUNCTYPE(None, c_wchar_p, c_wchar_p, c_wchar_p)
      self._oOpenSessionEndCb = oCallbackType(fOpenSessionEndCb)
    
    libnethelpclient.NHRegisterOpenSessionEndCb(self._oClient, self._oOpenSessionEndCb)
    
  def registerControlTakenCb(self, fControlTakenCb):
    if fControlTakenCb is None:
      self._oControlTakenCb = None
    else:
      oCallbackType = CFUNCTYPE(None)
      self._oControlTakenCb = oCallbackType(fControlTakenCb)
      
    libnethelpclient.NHRegisterControlTakenCb(self._oClient, self._oControlTakenCb)
    
  def registerControlReleasedCb(self, fControlReleasedCb):
    if fControlReleasedCb is None:
      self._oControlReleasedCb = None
    else:
      oCallbackType = CFUNCTYPE(None)
      self._oControlReleasedCb = oCallbackType(fControlReleasedCb)
      
    libnethelpclient.NHRegisterControlReleasedCb(self._oClient, self._oControlReleasedCb)
