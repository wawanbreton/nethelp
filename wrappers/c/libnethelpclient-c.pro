#-------------------------------------------------
#
# Project created by QtCreator 2013-07-30T14:11:10
#
#-------------------------------------------------

QT += network

TARGET = nethelpclient-c
TEMPLATE = lib

INCLUDEPATH += ../../nethelpclient
INCLUDEPATH += ../../common

win32 {
  LIBS += -L../../libs/OpenCV/bin
  LIBS += -lopencv_highgui231
  LIBS += -lopencv_core231
}
else {
  LIBS += -lopencv_highgui
  LIBS += -lopencv_core
}

LIBS += -L../../nethelpclient -lnethelpclient

DEFINES += LIBNETHELPCLIENTC

SOURCES += nethelpclient-c.cpp \
           util.cpp
HEADERS += nethelpclient-c.h \
           nethelpclientwrapper.h \
           util.h
