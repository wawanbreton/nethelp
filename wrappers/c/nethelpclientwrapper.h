/*! @file   nethelpclientwrapper.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#ifndef NETHELPCLIENTWRAPPER_H
#define NETHELPCLIENTWRAPPER_H

#include <QObject>

#include "nethelpclient.h"
#include "util.h"

class NetHelpClientWrapper : public QObject
{
    Q_OBJECT

    public:
        inline NetHelpClientWrapper() :
            QObject(),
            wrapped(new NetHelpClient("client", this)),
            openSessionEndCb(NULL),
            controlTakenCb(NULL),
            controlReleasedCb(NULL)
        {
            connect(wrapped, SIGNAL(openSessionEnd(QString,QString,QString)),
                             SLOT(onOpenSessionEnd(QString,QString,QString)));
            connect(wrapped, SIGNAL(controlTaken()),    SLOT(onControlTaken()));
            connect(wrapped, SIGNAL(controlReleased()), SLOT(onControlReleased()));
        }

        NetHelpClient *wrapped;

        void (*openSessionEndCb)(const wchar_t *, const wchar_t *, const wchar_t *);

        void (*controlTakenCb)();

        void (*controlReleasedCb)();

    private slots:
        inline void onOpenSessionEnd(const QString &identifier,
                                     const QString &password,
                                     const QString &errorMsg)
        {
            if(openSessionEndCb)
            {
                wchar_t *identifierBuf = returnString(identifier);
                wchar_t *passwordBuf = returnString(password);
                wchar_t *errorMsgBuf = returnString(errorMsg);

                openSessionEndCb(identifierBuf, passwordBuf, errorMsgBuf);

                delete[] identifierBuf;
                delete[] passwordBuf;
                delete[] errorMsgBuf;
            }
        }

        inline void onControlTaken()
        {
            if(controlTakenCb)
            {
                controlTakenCb();
            }
        }

        inline void onControlReleased()
        {
            if(controlReleasedCb)
            {
                controlReleasedCb();
            }
        }
};

#endif // NETHELPCLIENTWRAPPER_H
