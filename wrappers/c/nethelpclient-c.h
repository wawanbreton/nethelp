/*! @file   nethelpclient-c.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#ifndef NETHELPCLIENTC_H
#define NETHELPCLIENTC_H

#ifdef LIBNETHELPCLIENTC
# include <QtCore/QtGlobal>
# define EXPORT Q_DECL_EXPORT
#else
// Set the correct import directives according to the OS
#endif

#ifdef __cplusplus 
extern "C" {
#endif
  
/*! @brief Instantiates a NetHelp client
 *  @return The NetHelp client instance to be given to all the following methods */
EXPORT void *NHNewClient();

/*! @brief Deletes a NetHelp client instance
 *  @param client The client instance to be deleted */
EXPORT void NHDeleteClient(void *client);

/*! @brief Sets the URL of the NetHelp server
 *  @param client The NetHelp client instance
 *  @param url The URL of the NetHelp server */
EXPORT void NHSetServerUrl(void *client, const wchar_t *url);

/*! @brief Sets the proxy to be used to access the NetHelp server
 *  @param client The NetHelp client instance
 *  @param hasProxy Null if no proxy is used, non-null if a proxy is used
 *  @param hostName The proxy host name
 *  @param port The proxy port
 *  @param user The proxy user login
 *  @param password The proxy password */
EXPORT void NHSetProxy(void *client,
                       int hasProxy,
                       const wchar_t *hostName,
                       const unsigned short port,
                       const wchar_t *user,
                       const wchar_t *password);

/*! @brief Starts a remote control session
 *  @param client The NetHelp client instance
 *  @param iconData Pointer to the PNG icon data
 *  @param iconDataSize Length of the PNG icon data
 *  @param desiredIdentifier The requested identifier */
EXPORT void NHStartSession(void *client,
                           const char *iconData,
                           int iconDataSize,
                           const wchar_t *desiredIdentifier);

/*! @brief Stops a remote control session
 *  @param client The NetHelp client instance */
EXPORT void NHStopSession(void *client);

/*! @brief Indicates whether a remote control session is running
 *  @param client The NetHelp client instance
 *  @return Non-null if a session is running, null if there is no session */
EXPORT int NHIsSessionRunning(void *client);

/*! @brief Gets the current session ID
 *  @param client The NetHelp client instance
 *  @return The current session ID */
EXPORT wchar_t *NHGetSessionId(void *client);

/*! @brief Gets the current session password
 *  @param client The NetHelp client instance
 *  @return The current session password */
EXPORT wchar_t *NHGetSessionPassword(void *client);

/*! @brief Indicates whether the client is currently under remote control
 *  @param client The NetHelp client instance
 *  @return Non-null if the client is under remote control, null otherwise */
EXPORT int NHIsRemoteControlTaken(void *client);

/*! @brief Starts the VNC client proxy
 *  @param client The NetHelp client instance */
EXPORT void NHStartVncClientProxy(void *client);

/*! @brief Stops the VNC client proxy
 *  @param client The NetHelp client instance */
EXPORT void NHStopVncClientProxy(void *client);

/*! @brief Sends a useful piece of information to the viewer
 *  @param client The NetHelp client instance
 *  @param information The information to be sent */
EXPORT void NHSendInformation(void *client, wchar_t *information);

/*! @brief Sends the GUI description to the viewer, see
 *         http://sourceforge.net/apps/mediawiki/nethelp/index.php?title=Client_description
 *         for more details
 *  @param client The NetHelp client instance
 *  @param information The information to be sent */
EXPORT void NHSendDescription(void *client, wchar_t *description);

/*! @brief Register interesting files to be sent to the viewer
 *  @param client The NetHelp client instance
 *  @param categoryName The files category name, NUL-terminated
 *  @param files The files to be registered for the category. The files have to be separeted by
 *               a \n and the string has to be NUL-terminated */
EXPORT void NHRegisterInterestingFiles(void *client,
                                       const wchar_t *categoryName,
                                       const wchar_t *files);

/*! @brief Registers a callback method to be called when the session opening process is over
 *  @param client The NetHelp client instance
 *  @param openSessionEndCb The callback method to be called. The first argument is the session ID,
 *                          the second one is the session password, and the last one is the error
 *                          message, in case something went wrong. */
EXPORT void NHRegisterOpenSessionEndCb(void *client,
                                       void (*openSessionEndCb)(const wchar_t *,
                                                                const wchar_t *,
                                                                const wchar_t *));

/*! @brief Registers a callback method to be called when a remote technician takes the control
 *  @param client The NetHelp client instance
 *  @param controlTakenCb The callback method to be called */
EXPORT void NHRegisterControlTakenCb(void *client, void (*controlTakenCb)());

/*! @brief Registers a callback method to be called when a remote technician releases the control
 *  @param client The NetHelp client instance
 *  @param controlReleasedCb The callback method to be called */
EXPORT void NHRegisterControlReleasedCb(void *client, void (*controlReleasedCb)());

/*! @brief If the calling application is not living in a Qt environment, you have to call this
 *         method once to instanciate the QApplication
 *  @sa NHKeepMeAlive()
 *  @sa NHExec()
 *  @sa NHExit() */
EXPORT void NHStandaloneInit();

/*! @brief If the calling application is not living in a Qt environment, you have to call this
 *         method very frequently to have the QApplication process its events
 *  @warning This call has to be made from the application main thread */
EXPORT void NHKeepMeAlive();

/*! @brief If the calling application is not living in a Qt environment, you may call this
 *         method to let the QApplication process its events
 *  @warning This call has to be made from the application main thread and will block it
 *           until NHExit() is called */
EXPORT int NHExec();

/*! @brief If you called the NHExec() method above, you will have to call this once to have Qt stop
 *         treating events */
EXPORT void NHExit();

#ifdef __cplusplus 
}
#endif

#endif // NETHELPCLIENTC_H
