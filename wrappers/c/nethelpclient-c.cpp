/*! @file   nethelpclient-c.c
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelpclient-c.h"

#include <QCoreApplication>

#include "nethelpclient.h"
#include "nethelpclientwrapper.h"

#define WRAPPED (((NetHelpClientWrapper *)client)->wrapped)


QCoreApplication *application = NULL;

void *NHNewClient()
{
    return new NetHelpClientWrapper();
}

void NHDeleteClient(void *client)
{
    delete ((NetHelpClientWrapper *)client);
}

void NHSetServerUrl(void *client, const wchar_t *url)
{
    WRAPPED->setServerUrl(wcharToString(url));
}

void NHSetProxy(void *client,
                int hasProxy,
                const wchar_t *hostName,
                const unsigned short port,
                const wchar_t *user,
                const wchar_t *password)
{
    if(hasProxy)
    {
        QNetworkProxy proxy;
        proxy.setHostName(wcharToString(hostName));
        proxy.setPort(port);
        proxy.setUser(wcharToString(user));
        proxy.setPassword(wcharToString(password));
        WRAPPED->setProxy(proxy);
    }
    else
    {
        WRAPPED->setProxy(QNetworkProxy::NoProxy);
    }
}

void NHStartSession(void *client,
                    const char *iconData,
                    int iconDataSize,
                    const wchar_t *desiredIdentifier)
{
    WRAPPED->startSession(QByteArray(iconData, iconDataSize),
                          wcharToString(desiredIdentifier));
}

void NHStopSession(void *client)
{
    WRAPPED->stopSession();
}

int NHIsSessionRunning(void *client)
{
    return WRAPPED->isSessionRunning();
}

wchar_t *NHGetSessionId(void *client)
{
    return returnString(WRAPPED->getSessionId());
}

wchar_t *NHGetSessionPassword(void *client)
{
    return returnString(WRAPPED->getSessionPassword());
}

int NHIsRemoteControlTaken(void *client)
{
    return WRAPPED->isRemoteControlTaken();
}

void NHStartVncClientProxy(void *client)
{
    WRAPPED->startVncClientProxy();
}

void NHStopVncClientProxy(void *client)
{
    WRAPPED->stopVncClientProxy();
}

void NHSendInformation(void *client, wchar_t *information)
{
    WRAPPED->sendInformation(wcharToString(information));
}

void NHSendDescription(void *client, wchar_t *description)
{
    WRAPPED->sendDescription(wcharToString(description));
}

void NHRegisterInterestingFiles(void *client,
                                const wchar_t *categoryName,
                                const wchar_t *files)
{
    size_t size = 0;
    size_t i=0;
    const wchar_t *currentFileName = files;
    QList<Path> paths;

    while(true)
    {
        i++;
        size++;

        if(files[i] == '\n' || files[i] == '\0')
        {
            paths << Path(wcharToString(currentFileName, size), false);

            if(files[i] == '\0')
            {
                break;
            }

            size = 0;
            currentFileName = files + i + 1;
            i++;

        }
    }

    WRAPPED->registerInterestingFiles(wcharToString(categoryName), paths);
}

void NHRegisterOpenSessionEndCb(void *client,
                                void (*openSessionEndCb)(const wchar_t *,
                                                         const wchar_t *,
                                                         const wchar_t *))
{
    ((NetHelpClientWrapper *)client)->openSessionEndCb = openSessionEndCb;
}

void NHRegisterControlTakenCb(void *client, void (*controlTakenCb)())
{
    ((NetHelpClientWrapper *)client)->controlTakenCb = controlTakenCb;
}

void NHRegisterControlReleasedCb(void *client, void (*controlReleasedCb)())
{
    ((NetHelpClientWrapper *)client)->controlReleasedCb = controlReleasedCb;
}

void NHStandaloneInit()
{
    // We are living outside a Qt application, so we have to create our own one
    int argc = 0;
    application = new QCoreApplication(argc, NULL);
}

void NHKeepMeAlive()
{
    application->processEvents();
}

int NHExec()
{
    return application->exec();
}

void NHExit()
{
    application->deleteLater();
    application->exit();
    application = NULL;
}
