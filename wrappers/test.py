# -*- coding: utf-8 -*-

import pynethelp
import pynethelp.nethelpclient
import time

def printAll():
  print '==='
  print 'Session running :  ', oClient.isSessionRunning()
  print 'Session ID :       ', oClient.getSessionId()
  print 'Session password : ', oClient.getSessionPassword()
  print 'Remote control:    ', oClient.isRemoteControlTaken()
  print '==='

def openSessionEndCb(uIdentifier, uPassword, uErrorMsg):
  print 'openSessionEndCb [%s] [%s] [%s]' %(uIdentifier, uPassword, uErrorMsg)
  printAll()
  
  
def controlTakenCb():
  print 'controlTakenCb'
  oClient.sendInformation(u'I am very nice, am I not ?')
  oClient.sendInformation(u'I am soooo cute')
  oClient.sendInformation(u'I can send UTF-8 chars too : êéûôâ')
  oClient.registerInterestingFiles(u'superfiles', [u'foo', u'bar'])
  
def controlReleasedCb():
  print 'controlReleasedCb'

pynethelp.standaloneInit()

oClient = pynethelp.nethelpclient.NetHelpClient()

printAll()

oClient.setServerUrl(u'http://localhost:8080')
oClient.registerOpenSessionEndCb(openSessionEndCb)
oClient.registerControlTakenCb(controlTakenCb)
oClient.registerControlReleasedCb(controlReleasedCb)

oClient.startSession(open('logo.png', 'rb').read(), u'supertoto')

printAll()

while True:
  pynethelp._exec()
