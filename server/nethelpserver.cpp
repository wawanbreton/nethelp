/*! @file   nethelpserver.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "nethelpserver.h"

#include <QStringList>
#include <QCoreApplication>

#include "common/constants.h"
#include "core/httptunnelcommand.h"
#include "server/httptunnelserver.h"
#include "server/requestanswerer.h"
#include "server/requestreader.h"


NetHelpServer::NetHelpServer(quint16 port, bool loopbackOnly):
    QTcpServer(),
    _clientsManager(new ClientsListManager(this))
{
    if(listen(loopbackOnly ? QHostAddress::LocalHost : QHostAddress::Any, port))
    {
        qInfo() << "Now listening on port" << port
                << (loopbackOnly ? "on loopback interface" : "on all interfaces");

        connect(this, &NetHelpServer::newConnection, this, &NetHelpServer::onNewConnection);
    }
    else
    {
        qWarning() << "Unable to listen on port"
                   << port << ", please check that an other server is not running";
    }
}

void NetHelpServer::onNewConnection()
{
    while(hasPendingConnections())
    {
        QTcpSocket *socket = nextPendingConnection();

        qDebug() << "###### New socket connected" << socket;

        connect(socket, &QTcpSocket::disconnected, socket, &QTcpSocket::deleteLater);
        setupReader(socket);
    }
}

void NetHelpServer::onRequestHeaderParsed(const RequestDesc &desc)
{
    RequestReader *reader = qobject_cast<RequestReader *>(sender());
    if(reader)
    {
        HttpTunnelServer *tunnel = nullptr;
        if(desc.command == HttpTunnelCommand::Hello)
        {
            QString identifier = _clientsManager->getClientUniqueIdentifier(
                                                         desc.arguments.value("desiredIdentifier"));
            bool controllable = desc.arguments.value("controllable") == "true";
            bool listClients = desc.arguments.value("listClients") == "true";

            qInfo() << "Client" << identifier << "is now connected"
                    << QString("(controllable: %1)").arg(controllable)
                    << QString("(listClients: %1)").arg(listClients);

            tunnel = new HttpTunnelServer(identifier,
                                          desc.arguments.value("desiredPassword"),
                                          _clientsManager,
                                          listClients,
                                          this);
            _clientsManager->addClient(identifier, tunnel, controllable);
        }
        else
        {
            QString identifier = desc.arguments.value("identifier");
            const QMap<QString, ClientParams> &clients = _clientsManager->getClients();
            auto iterator = clients.constFind(identifier);
            if(iterator != clients.constEnd())
            {
                tunnel = iterator.value().tunnel;
            }
            else
            {
                qWarning() << "Received command" << desc.command
                           << "for unknown client" << identifier;
            }
        }

        if(tunnel)
        {
            connect(tunnel, &HttpTunnelServer::deviceOver, this, &NetHelpServer::onDeviceOver);
            tunnel->processIncomingCommand(reader, desc);
        }
        else
        {
            RequestAnswerer::send(reader->accessDevice(), HttpStatusCode::BadRequest);
        }
    }
    else
    {
        qCritical() << "Sender is not a RequestReader";
    }
}

void NetHelpServer::onRequestError(QAbstractSocket::SocketError socketError, bool timeout)
{
    // Not much to do, because at this point we don't known which client causes the error, so
    // log it and wait to see if a tunnel times out
    qWarning() << "socket error" << socketError << timeout;
}

void NetHelpServer::onDeviceOver(QIODevice *device)
{
    // Device may be re-used for a later command (e.g. apache mod_proxy connection pool)
    if(device->bytesAvailable())
    {
        onDeviceReuseReadyRead();
    }
    else
    {
        connect(device, &QIODevice::readyRead, this, &NetHelpServer::onDeviceReuseReadyRead);
    }
}

void NetHelpServer::onDeviceReuseReadyRead()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if(socket)
    {
        disconnect(socket, &QIODevice::readyRead, this, &NetHelpServer::onDeviceReuseReadyRead);
        setupReader(socket);
    }
    else
    {
        qCritical() << "Sender is not a QIODevice";
    }
}

void NetHelpServer::setupReader(QTcpSocket *socket)
{
    RequestReader *reader = new RequestReader(Constants::requestTimeout, this);
    connect(reader, &RequestReader::headerParsed, this,   &NetHelpServer::onRequestHeaderParsed);
    connect(reader, &RequestReader::error,        this,   &NetHelpServer::onRequestError);
    connect(reader, &RequestReader::finished,     reader, &RequestReader::deleteLater);
    reader->setSocket(socket);
}
