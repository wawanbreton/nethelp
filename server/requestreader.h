#pragma once

#include <QObject>

#include <QTcpSocket>

#include "core/abstractcommandreader.h"

class RequestReader : public AbstractCommandReader
{
    Q_OBJECT

    public:
        explicit RequestReader(int timeout, QObject *parent = nullptr);

        void setSocket(QTcpSocket *socket);

    signals:
        void error(QAbstractSocket::SocketError socketError, bool timeout);

    protected:
        virtual bool parseHeader(QByteArray &buffer,
                                 HttpTunnelCommand::Enum &command,
                                 ArgsList &arguments,
                                 int &contentLength) override;

        virtual void onTimeout() override;

    private:
        void onSocketError(QAbstractSocket::SocketError socketError);
};
