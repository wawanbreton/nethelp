#pragma once

#include <QByteArray>

class HttpTunnelServer;

typedef struct
{
    HttpTunnelServer *tunnel{nullptr};
    bool controllable{false};
    bool controlled{false};
    QByteArray data{};
} ClientParams;
