#include "dataaggregator.h"

#include "core/abstractcommandreader.h"
#include "server/requestanswerer.h"


DataAggregator::DataAggregator(HttpTunnelCommand::Enum command,
                               const ArgsList &arguments,
                               QObject *parent) : QObject(parent),
    _command(command),
    _arguments(arguments)
{
}

void DataAggregator::appendReader(AbstractCommandReader *reader)
{
    if(reader->getRequestDesc().expectsData)
    {
        if(reader->getRequestDesc().command == _command &&
           reader->getRequestDesc().arguments == _arguments)
        {
            _readers.enqueue(reader);
            _readersAggregatedData.insert(reader, {QByteArray(), false});

            connect(reader, &AbstractCommandReader::dataAvailable,
                    this,   &DataAggregator::onReaderData);
        }
        else
        {
            qCritical() << "Reader is not compatible"
                        << reader->getRequestDesc().command << reader->getRequestDesc().arguments
                        << _command << _arguments;
        }
    }
    else
    {
        qCritical() << "Reader" << reader << "does not expect payload data";
    }
}

void DataAggregator::onReaderData(const QByteArray &data, bool complete)
{
    AbstractCommandReader *reader = qobject_cast<AbstractCommandReader *>(sender());
    if(reader)
    {
        auto iterator = _readersAggregatedData.find(reader);
        if(iterator != _readersAggregatedData.end())
        {
            iterator.value().data.append(data);
            iterator.value().complete = complete;

            pushAvailableData();
        }
        else
        {
            qCritical() << "Reader is not registered" << reader;
        }
    }
    else
    {
        qCritical() << "Sender is not a AbstractCommandReader";
    }
}

void DataAggregator::pushAvailableData()
{
    if(_readers.isEmpty())
    {
        qCritical() << "No reader to push data from";
        return;
    }

    QByteArray dataToPush;
    bool next = false;

    //qDebug() << "<===";
    do
    {
        AbstractCommandReader *firstReader = _readers.head();
        auto iterator = _readersAggregatedData.find(firstReader);
        dataToPush.append(iterator.value().data);
        iterator.value().data.clear();
        //qDebug() << "push data" << _command << _arguments << dataToPush.count() << iterator.value().complete;

        next = iterator.value().complete;
        if(next)
        {
            RequestAnswerer::send(firstReader->accessDevice(), HttpStatusCode::Ok);
            _readers.dequeue();
            _readersAggregatedData.erase(iterator);
            emit deviceOver(firstReader->accessDevice());
        }
    } while(next && !_readers.isEmpty());
    //qDebug() << ">===";

    if(!dataToPush.isEmpty())
    {
        emit sendCommand({_command, _arguments, dataToPush});
    }

    if(_readers.isEmpty())
    {
        emit finished();
    }
}
