#pragma once

#include <QObject>

#include <QQueue>

#include "core/nethelpcommand.h"

class AbstractCommandReader;

class DataAggregator : public QObject
{
    Q_OBJECT

    public:
        explicit DataAggregator(HttpTunnelCommand::Enum command,
                                const ArgsList &arguments,
                                QObject *parent = nullptr);

        HttpTunnelCommand::Enum getCommand() const { return _command; }

        const ArgsList &getArguments() const { return _arguments; }

        void appendReader(AbstractCommandReader *reader);

    signals:
        void sendCommand(const NetHelpCommand &command);

        void deviceOver(QIODevice *device);

        void finished();

    private:
        void onReaderData(const QByteArray &data, bool complete);

        void pushAvailableData();

    private:
        typedef struct
        {
            QByteArray data{};
            bool complete{false};
        } AggregatedReaderData;

    private:
        const HttpTunnelCommand::Enum _command;
        const ArgsList _arguments;
        QQueue<AbstractCommandReader *> _readers;
        QMap<AbstractCommandReader *, AggregatedReaderData> _readersAggregatedData;
};
