/*! @file   server/main.cpp
    @brief  This file is the entry point of the NetHelp server application
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QStringList>

#include "common/messagehandler.h"
#include "server/nethelpserver.h"


QString boolOptionTostring(const QString &message, bool option)
{
    return message.arg(option ? "true" : "false");
}

/*! @brief NetHelp server entry point
    @param argc The number of given arguments
    @param argv The list of given arguments
    @return The program exit code */
int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    // Initialize parser
    QCommandLineParser parser;
    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsCompactedShortOptions);
    parser.setApplicationDescription("This is the NetHelp routing server application, "
                                     "you can use the following options :");

    parser.addHelpOption();

    // Port option
    quint16 port = 8080;
    QCommandLineOption optionPort({"p", "port"},
                                  QString("Select the port to listen on (default %1)").arg(port),
                                  "port");
    parser.addOption(optionPort);

    // Loopback option
    bool loopbackOnly = false;
    QString optionLocalStr = boolOptionTostring("Listen only on the loopback interface (default %1)",
                                                loopbackOnly);
    QCommandLineOption optionLocal({"l", "local"}, optionLocalStr);
    parser.addOption(optionLocal);

    // Debug option
    bool displayDebug = false;
    QCommandLineOption optionDebug({"d", "debug"},
                                   boolOptionTostring("Display debug messages (default %1)",
                                                      displayDebug));
    parser.addOption(optionDebug);

    parser.process(app);

    if(parser.isSet(optionPort))
    {
        QString portStr = parser.value(optionPort);
        bool portOk;
        port = portStr.toUShort(&portOk);

        if(!portOk)
        {
            qInfo() << "ERROR: Invalid given port" << portStr;
            parser.showHelp();
        }
    }

    if(parser.isSet(optionLocal))
    {
        loopbackOnly = true;
    }

    MessageHandler handler(".");
    handler.setMsgType(parser.isSet(optionDebug) ? QtDebugMsg : QtWarningMsg);

    NetHelpServer server(port, loopbackOnly);

    if(server.isListening())
    {
        return app.exec();
    }
    else
    {
        return 1;
    }
}
