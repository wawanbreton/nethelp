#pragma once

#include <QIODevice>

#include "common/httpstatuscode.h"

namespace RequestAnswerer
{
    void send(QIODevice *device, const QByteArray &data);

    void send(QIODevice *device, HttpStatusCode::Enum code, const QByteArray &data = QByteArray());
}
