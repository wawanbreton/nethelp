#include "requestreader.h"

#include "common/argslist.h"

#include <QDebug>


RequestReader::RequestReader(int timeout, QObject *parent) :
    AbstractCommandReader(timeout, parent)
{
}

void RequestReader::setSocket(QTcpSocket *socket)
{
    connect(socket, qOverload<QAbstractSocket::SocketError>(&QTcpSocket::error),
            this,   &RequestReader::onSocketError);
    connect(socket, &QTcpSocket::disconnected,
            this,   &RequestReader::onTransferFinished);

    AbstractCommandReader::setDevice(socket);
}

bool RequestReader::parseHeader(QByteArray &buffer,
                                HttpTunnelCommand::Enum &command,
                                ArgsList &arguments,
                                int &contentLength)
{
    // Flush the HTTP header
    bool headerComplete = false;
    QStringList lines;
    int startLineIndex = 0;
    int endLineIndex = 0;
    while((endLineIndex = buffer.indexOf("\r\n", startLineIndex)) >= 0 && !headerComplete)
    {
        QString line = QString::fromUtf8(buffer.mid(startLineIndex, endLineIndex - startLineIndex));
        if(line.isEmpty())
        {
            // This is the break line indicating the start of the data
            headerComplete = true;
        }
        else
        {
            lines.append(line);
        }

        startLineIndex = endLineIndex + 2;
    }

    if(headerComplete)
    {
        buffer = buffer.mid(startLineIndex);

        for(const QString &line : lines)
        {
            if(line.startsWith("Content-Length"))
            {
                bool ok;
                int length = line.mid(16).toLongLong(&ok);
                if(ok)
                {
                    contentLength = length;
                }
                else
                {
                    qWarning() << "Error when parsing content length" << line;
                }
            }
            else if(line.startsWith("GET") || line.startsWith("POST"))
            {
                // Tokens are supposed to be something like "GET", "/command?arg=value", "HTTP/1.1"
                QStringList tokens = line.split(' ');

                if(tokens.count() == 3)
                {
                    parseUrl(tokens.at(1), command, arguments);
                }
                else
                {
                    qWarning() << "Invalid HTTP request :" << line;
                }
            }
        }
    }

    return headerComplete;
}

void RequestReader::onTimeout()
{
    emit error(QAbstractSocket::UnknownSocketError, true);
    onFatalError();
}

void RequestReader::onSocketError(QAbstractSocket::SocketError socketError)
{
    emit error(socketError, false);
    onFatalError();
}
