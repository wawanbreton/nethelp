QT       += core network
QT       -= gui

TARGET = server

TEMPLATE = app

NETHELP_ROOT = ..
NETHELP_COMMON = $${NETHELP_ROOT}/common
NETHELP_CORE   = $${NETHELP_ROOT}/core
NETHELP_SERVER = $${NETHELP_ROOT}/server

INCLUDEPATH += $${NETHELP_ROOT}

include($${NETHELP_COMMON}/common.pri)


SOURCES *= $${NETHELP_COMMON}/clientendpointparams.cpp
SOURCES *= $${NETHELP_COMMON}/categorizedfiles.cpp
SOURCES *= $${NETHELP_COMMON}/messagehandler.cpp
SOURCES *= $${NETHELP_COMMON}/path.cpp
SOURCES *= $${NETHELP_COMMON}/httpstatuscode.cpp
SOURCES *= $${NETHELP_CORE}/abstractcommandreader.cpp
SOURCES *= $${NETHELP_CORE}/abstracthttptunnel.cpp
SOURCES *= $${NETHELP_SERVER}/clientslistmanager.cpp
SOURCES *= $${NETHELP_SERVER}/dataaggregator.cpp
SOURCES *= $${NETHELP_SERVER}/main.cpp
SOURCES *= $${NETHELP_SERVER}/httptunnelserver.cpp
SOURCES *= $${NETHELP_SERVER}/nethelpserver.cpp
SOURCES *= $${NETHELP_SERVER}/requestanswerer.cpp
SOURCES *= $${NETHELP_SERVER}/requestreader.cpp

HEADERS *= $${NETHELP_COMMON}/clientendpointparams.h
HEADERS *= $${NETHELP_COMMON}/categorizedfiles.h
HEADERS *= $${NETHELP_COMMON}/constants.h
HEADERS *= $${NETHELP_COMMON}/messagehandler.h
HEADERS *= $${NETHELP_COMMON}/path.h
HEADERS *= $${NETHELP_COMMON}/httpstatuscode.h
HEADERS *= $${NETHELP_CORE}/abstractcommandreader.h
HEADERS *= $${NETHELP_CORE}/abstracthttptunnel.h
HEADERS *= $${NETHELP_CORE}/requestdesc.h
HEADERS *= $${NETHELP_CORE}/httptunnelcommand.h
HEADERS *= $${NETHELP_CORE}/nethelpcommand.h
HEADERS *= $${NETHELP_SERVER}/clientparams.h
HEADERS *= $${NETHELP_SERVER}/clientrequest.h
HEADERS *= $${NETHELP_SERVER}/clientslistmanager.h
HEADERS *= $${NETHELP_SERVER}/dataaggregator.h
HEADERS *= $${NETHELP_SERVER}/httptunnelserver.h
HEADERS *= $${NETHELP_SERVER}/nethelpserver.h
HEADERS *= $${NETHELP_SERVER}/requestanswerer.h
HEADERS *= $${NETHELP_SERVER}/requestreader.h
