#pragma once

#include <QByteArray>
#include <QIODevice>
#include <QString>

#include "core/httptunnelcommand.h"

typedef struct
{
    QIODevice *device;
    HttpTunnelCommand::Enum command{HttpTunnelCommand::None};
    QString clientId;
    QByteArray data;
    QString identifier;
    QString password;
    QString type;
} ClientRequest;
