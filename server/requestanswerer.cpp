#include "requestanswerer.h"


void RequestAnswerer::send(QIODevice *device, const QByteArray &data)
{
    send(device, HttpStatusCode::Ok, data);
}

void RequestAnswerer::send(QIODevice *device, HttpStatusCode::Enum code, const QByteArray &data)
{
    device->write(QString("HTTP/1.1 %1 %2\r\n").arg(code).arg(HttpStatusCode::text(code)).toUtf8());
    device->write(QString("Content-Length: %1\r\n\r\n").arg(data.count()).toUtf8());
    if(!data.isEmpty())
    {
        device->write(data);
    }
}
