/*! @file   nethelpserver.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include <QTcpServer>

#include "common/stringboolpair.h"
#include "core/requestdesc.h"
#include "server/clientrequest.h"
#include "server/clientslistmanager.h"

class HttpTunnelServer;

/*! @brief Base class of the NetHelp server which is a simple TCP server listening
           for incoming connections */
class NetHelpServer : public QTcpServer
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param port The TCP port to listen to
            @param loobbackOnly True if the server should liste on the loopback interface only,
                                false if all the network interfaces may be used */
        NetHelpServer(quint16 port, bool loopbackOnly);

    private:
        /*! @brief Slot called every time a client connects to the server */
        void onNewConnection();

        void onRequestHeaderParsed(const RequestDesc &desc);

        void onRequestError(QAbstractSocket::SocketError socketError, bool timeout);

        void onDeviceOver(QIODevice *device);

        void onDeviceReuseReadyRead();

        void setupReader(QTcpSocket *socket);

    private:
        ClientsListManager *_clientsManager{nullptr};
};
