#include "clientslistmanager.h"

#include <QDebug>

#include "server/httptunnelserver.h"


ClientsListManager::ClientsListManager(QObject *parent) : QObject(parent)
{
}

void ClientsListManager::addClient(const QString &client, HttpTunnelServer *tunnel, bool controllable)
{
    connect(tunnel, &HttpTunnelServer::disconnected,
            this,   &ClientsListManager::onTunnelDisconnected);

    _clients.insert(client, {tunnel, controllable});
    emit listChanged(controllable);
}

void ClientsListManager::setClientControlled(const QString &clientId, bool controlled)
{
    auto iterator = _clients.find(clientId);
    if(iterator != _clients.end() && iterator.value().controlled != controlled)
    {
        if(!iterator.value().controllable)
        {
            qCritical() << "Setting a client as controlled" << controlled
                        << "but it is not controllable";
        }

        iterator.value().controlled = controlled;
        emit listChanged(iterator.value().controllable);
    }
}

void ClientsListManager::setClientData(const QString &clientId, const QByteArray &data)
{
    auto iterator = _clients.find(clientId);
    if(iterator != _clients.end())
    {
        iterator.value().data = data;
    }
    else
    {
        qWarning() << "No client with ID" << clientId;
    }
}

QByteArray ClientsListManager::getClientData(const QString &clientId) const
{
    auto iterator = _clients.constFind(clientId);
    if(iterator != _clients.constEnd())
    {
        return iterator.value().data;
    }
    else
    {
        qWarning() << "No client with ID" << clientId;
    }

    return QByteArray();
}

QString ClientsListManager::getClientUniqueIdentifier(const QString &desiredIdentifier)
{
    if(desiredIdentifier.isEmpty())
    {
        // Client has not required a specific identifier, give it the first available
        return QString::number(_identifiers++);
    }
    else
    {
        // Client has required a specific identifier, give something very similar that
        // does not already exist
        QString identifier = desiredIdentifier;
        int count = 1;
        while(_clients.contains(identifier))
        {
            identifier = desiredIdentifier + "_" + QString::number(count++);
        }

        return identifier;
    }
}

void ClientsListManager::onTunnelDisconnected()
{
    HttpTunnelServer *tunnel = qobject_cast<HttpTunnelServer *>(sender());
    if(tunnel)
    {
        qInfo() << "Client" << tunnel->getIdentifier() << "has disconnected";

        auto iterator = _clients.find(tunnel->getIdentifier());
        if(iterator != _clients.end())
        {
            bool controllable = iterator.value().controllable;
            _clients.erase(iterator);
            emit listChanged(controllable);
        }
        else
        {
            qCritical() << "Disconnected tunnel is not registered ?!";
        }

        tunnel->deleteLater();
    }
    else
    {
        qCritical() << "Sender is not a HttpTunnelServer ?!";
    }
}
