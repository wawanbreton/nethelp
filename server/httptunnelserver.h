/*! @file   httptunnel.h
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#pragma once

#include "core/abstracthttptunnel.h"

#include <QTcpSocket>
#include <QPointer>
#include <QTimer>

#include "common/clientendpointparams.h"
#include "common/httpstatuscode.h"
#include "server/clientrequest.h"
#include "core/requestdesc.h"

class ClientsListManager;
class DataAggregator;
class RequestReader;

/*! @brief This class implements the server-side of the NetHelp communication protocol */
class HttpTunnelServer : public AbstractHttpTunnel
{
    Q_OBJECT

    public:
        /*! @brief Constructor
            @param identifier The assigned identifier of the remote client
            @param desiredPassword The desired password, or an empty string to generate a password
            @param clientsManager The manager holding the controllable clients lists, or null if
                                  this is not necessary
            @param parent The parent container */
        explicit HttpTunnelServer(const QString &identifier,
                                  const QString &desiredPassword,
                                  ClientsListManager *clientsManager,
                                  bool listClients,
                                  QObject *parent = nullptr);

        void processIncomingCommand(AbstractCommandReader *reader, const RequestDesc &desc);

        /*! @brief Gets the affected identifier
            @return The affected identifier */
        const QString &getIdentifier() const { return _identifier; }

        /*! @brief Gets the session password
            @return The session password */
        const QString &getPassword() const { return _password; }

        void sendControlTaken() { sendCommand({HttpTunnelCommand::ControlTaken}); }

    signals:
        void deviceOver(QIODevice *device);

        void disconnected();

    protected:
        virtual bool sendCommandImpl(const NetHelpCommand &command) override;

    private:
        typedef struct
        {
            QTcpSocket *socket{nullptr};
            qint64 bytesRead{0};
            bool headerComplete{false};
            QByteArray headerData;
            int contentLength{-1};
            HttpTunnelCommand::Enum command{HttpTunnelCommand::None};
        } HttpRequest;

    private:
        /*! @brief Slot called when data is incoming from the socket */
        void onSocketDataReady();

        /*! @brief Slot called when a socket is destroyed */
        void onSocketDestroyed();

        bool gentlyCloseListen();

        void onHelloDataAvailable(const QByteArray &data, bool complete);

        void onHelloComplete(QIODevice *device);

        void sendAnswer(QIODevice *device, const NetHelpCommand &command);

        void sendAnswer(QIODevice *device, HttpStatusCode::Enum code);

        QByteArray buildClientsListData();

        void onClientsListChanged(bool controllable);

        void onCommandForwardDataAvailable(const QByteArray &data);

        void onListenOver();

        bool takeControl(const QString &clientId, const QString &password);

        void disconnect();

        void onListenReconnectTimeout();

    private:
        static qint64 _timeDelta;

    private:
        const bool _listClients{false};
        const QString _identifier;
        QString _password;
        QTimer *_timerListenNoData{nullptr};
        QTimer *_timerListenReconnect{nullptr};
        QIODevice *_deviceListen{nullptr};
        ClientsListManager *_clientsManager{nullptr};
        HttpTunnelServer *_remote{nullptr};
        QList<DataAggregator *> _dataAggregators;
};
