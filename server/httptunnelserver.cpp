/*! @file   httptunnel.cpp
    @brief  This file is part of the NetHelp project
 */

// This file is part of NetHelp.
//
// NetHelp is free software: you can redistribute it and/or modify it under the terms of the GNU
// Lesser General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NetHelp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with NetHelp.
// If not, see <http://www.gnu.org/licenses/lgpl.html>

#include "httptunnelserver.h"

#include <QBuffer>
#include <QCryptographicHash>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStringList>
#include <QTime>
#include <QUrlQuery>

#include "server/clientslistmanager.h"
#include "server/dataaggregator.h"
#include "common/constants.h"
#include "server/requestanswerer.h"
#include "server/requestreader.h"


// Add a time delta to the password generation, so that if a hacker knows the exact server time,
// he won't know the exact time the delta was initialized, which is the application start time
qint64 HttpTunnelServer::_timeDelta = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();

HttpTunnelServer::HttpTunnelServer(const QString &identifier,
                                   const QString &desiredPassword,
                                   ClientsListManager *clientsManager,
                                   bool listClients,
                                   QObject *parent) :
    AbstractHttpTunnel(parent),
    _listClients(listClients),
    _identifier(identifier),
    _timerListenNoData(new QTimer(this)),
    _timerListenReconnect(new QTimer(this)),
    _clientsManager(clientsManager)
{
    if(desiredPassword.isEmpty())
    {
        // Generate a random-enough and irreversible-enough password
        qint64 timeMsec = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch() - _timeDelta;
        QByteArray timeStr = QString::number(timeMsec).toUtf8();
        QByteArray hash = QCryptographicHash::hash(timeStr, QCryptographicHash::Md5);
        QString hashed = QString(hash.toHex()).toLower();
        _password = QString(hashed[(int)(timeMsec % 32)]) +
                            hashed[(int)((timeMsec + 4) % 32)] +
                            hashed[(int)((timeMsec + 8) % 32)] +
                            hashed[(int)((timeMsec + 17) % 32)] +
                            hashed[(int)((timeMsec + 22) % 32)] +
                            hashed[(int)((timeMsec + 27) % 32)];
    }
    else
    {
        _password = desiredPassword;
    }

    // Triggering the timer means that the client has not given any news after some time,
    // so we consider that the connection is broken
    _timerListenNoData->setInterval(Constants::listenNoDataTimeout);
    _timerListenNoData->setSingleShot(true);
    connect(_timerListenNoData, &QTimer::timeout, this, &HttpTunnelServer::gentlyCloseListen);

    _timerListenReconnect->setInterval(Constants::listenReconnectTimeout);
    _timerListenReconnect->setSingleShot(true);
    connect(_timerListenReconnect, &QTimer::timeout,
            this,                  &HttpTunnelServer::onListenReconnectTimeout);
    _timerListenReconnect->start();

    if(_listClients)
    {
        connect(_clientsManager, &ClientsListManager::listChanged,
                this,            &HttpTunnelServer::onClientsListChanged);
    }
}

void HttpTunnelServer::processIncomingCommand(AbstractCommandReader *reader,
                                              const RequestDesc &desc)
{
    qDebug() << "Received command" << desc.command << desc.arguments << desc.expectsData
             << "for" << _identifier << "on device" << reader->accessDevice();

    switch(desc.command)
    {
        case HttpTunnelCommand::None:
        {
            qWarning() << "Unexpected command type" << desc.command << _identifier;
            sendAnswer(reader->accessDevice(), HttpStatusCode::BadRequest);
            break;
        }
        case HttpTunnelCommand::Listen:
        {
            if(gentlyCloseListen())
            {
                qWarning() << "Received listen request for client" << _identifier
                           << "as it already had a pending listen request";
            }

            _deviceListen = reader->accessDevice();
            connect(_deviceListen, &QIODevice::destroyed,
                    this,          &HttpTunnelServer::onListenOver);

            _timerListenReconnect->stop();
            _timerListenNoData->start();

            trySendNextCommand();

            break;
        }
        case HttpTunnelCommand::Hello:
        {
            if(desc.expectsData)
            {
                // Client wants to say more about who he is
                reader->setSendDataWhenComplete(true);
                connect(reader, &RequestReader::dataAvailable,
                        this,   &HttpTunnelServer::onHelloDataAvailable);
            }
            else
            {
                // Client has nothing more to say
                onHelloComplete(reader->accessDevice());
            }
            break;
        }
        case HttpTunnelCommand::Goodbye:
        {
            // Say goodbye in return
            qInfo() << "Received goodbye for client" << _identifier;
            sendAnswer(reader->accessDevice(), {HttpTunnelCommand::Goodbye});
            disconnect();
            break;
        }
        case HttpTunnelCommand::ClientData:
        {
            QString clientId = desc.arguments.value("clientId");
            QByteArray clientData = _clientsManager->getClientData(clientId);
            sendAnswer(reader->accessDevice(),
                       {HttpTunnelCommand::ClientData, {{"clientId", clientId}}, clientData});
            break;
        }
        case HttpTunnelCommand::TakeControl:
        {
            bool success = takeControl(desc.arguments.value("clientId"),
                                       desc.arguments.value("password"));
            sendAnswer(reader->accessDevice(),
                       {HttpTunnelCommand::TakeControl, {{"success", success ? "true" : "false"}}});
            break;
        }
        case HttpTunnelCommand::ReleaseControl:
        {
            if(_remote)
            {
                sendAnswer(reader->accessDevice(), {HttpTunnelCommand::ControlReleased});
                _remote->sendCommand({HttpTunnelCommand::ControlReleased});

                _clientsManager->setClientControlled(_remote->getIdentifier(), false);

                _remote->_remote = nullptr;
                _remote = nullptr;
            }
            else
            {
                sendAnswer(reader->accessDevice(), HttpStatusCode::NotFound);
            }
            break;
        }
        case HttpTunnelCommand::PleaseReleaseControl:
        {
            QString clientId = desc.arguments.value("clientId");
            QString user = desc.arguments.value("user");

            const QMap<QString, ClientParams> &clients = _clientsManager->getClients();
            auto iterator = clients.find(clientId);
            if(iterator != clients.end())
            {
                iterator.value().tunnel->sendCommand({HttpTunnelCommand::PleaseReleaseControl,
                                                      desc.arguments});
            }

            break;
        }

        // Commands that are basically forwarded to remote client
        case HttpTunnelCommand::ListDirectory:
        case HttpTunnelCommand::TcpConnected:
        case HttpTunnelCommand::TcpData:
        case HttpTunnelCommand::TcpDisconnected:
        case HttpTunnelCommand::DownloadFile:
        case HttpTunnelCommand::FileData:
        case HttpTunnelCommand::GetPicture:
        case HttpTunnelCommand::PictureData:
        {
            if(_remote)
            {
                if(desc.expectsData)
                {
                    if(HttpTunnelCommand::largeDataCommand(desc.command))
                    {
                        DataAggregator *actualAggregator = nullptr;

                        for(DataAggregator *aggregator : _dataAggregators)
                        {
                            if(desc.command == aggregator->getCommand() &&
                               desc.arguments == aggregator->getArguments())
                            {
                                actualAggregator = aggregator;
                                break;
                            }
                        }

                        if(!actualAggregator)
                        {
                            actualAggregator = new DataAggregator(desc.command,
                                                                  desc.arguments,
                                                                  this);
                            connect(actualAggregator, &DataAggregator::sendCommand,
                                    _remote,          &HttpTunnelServer::sendCommand);
                            connect(actualAggregator,
                                    &DataAggregator::finished,
                                    this,
                                    [this, actualAggregator]() { _dataAggregators.removeAll(actualAggregator); });
                            connect(actualAggregator, &DataAggregator::deviceOver,
                                    this,             &HttpTunnelServer::deviceOver);
                            connect(actualAggregator,  &DataAggregator::finished,
                                    actualAggregator,  &DataAggregator::deleteLater);
                            _dataAggregators.append(actualAggregator);
                        }

                        actualAggregator->appendReader(reader);
                    }
                    else
                    {
                        reader->setSendDataWhenComplete(true);
                        connect(reader, &RequestReader::dataAvailable,
                                this,   &HttpTunnelServer::onCommandForwardDataAvailable);
                    }
                }
                else
                {
                    _remote->sendCommand({desc.command, desc.arguments});
                    sendAnswer(reader->accessDevice(), HttpStatusCode::Ok);
                }
            }
            else
            {
                sendAnswer(reader->accessDevice(), HttpStatusCode::NotFound);
            }
            break;
        }

        // Unexpected commands
        case HttpTunnelCommand::ClientsList:
        case HttpTunnelCommand::ControlTaken:
        case HttpTunnelCommand::ControlReleased:
        {
            qWarning() << "Received unexpected command from client" << desc.command << _identifier;
            sendAnswer(reader->accessDevice(), HttpStatusCode::BadRequest);
            break;
        }
    }
}

bool HttpTunnelServer::sendCommandImpl(const NetHelpCommand &command)
{
    if(_deviceListen)
    {
        sendAnswer(_deviceListen, command);
        onListenOver();
        return true;
    }
    else
    {
        return false;
    }
}

bool HttpTunnelServer::gentlyCloseListen()
{
    if(_deviceListen)
    {
        qDebug() << "Gently closing listen request for client" << _identifier;
        sendAnswer(_deviceListen, HttpStatusCode::NoContent);
        onListenOver();
        return true;
    }

    return false;
}

void HttpTunnelServer::onListenOver()
{
    _deviceListen = nullptr;

    _timerListenNoData->stop();
    _timerListenReconnect->start();
}

void HttpTunnelServer::onHelloDataAvailable(const QByteArray &data, bool complete)
{
    RequestReader *reader = qobject_cast<RequestReader *>(sender());
    if(reader)
    {
        _clientsManager->setClientData(_identifier, data);

        if(complete)
        {
            onHelloComplete(reader->accessDevice());
        }
    }
    else
    {
        qCritical() << "Sender is not a RequestReader" << sender();
    }
}

void HttpTunnelServer::onHelloComplete(QIODevice *device)
{
    // Return the affected identifier, password, and clients list if relevant
    sendAnswer(device,
               {HttpTunnelCommand::Hello,
                {{"identifier", _identifier}, {"password", _password}},
                buildClientsListData()});
}

void HttpTunnelServer::sendAnswer(QIODevice *device, const NetHelpCommand &command)
{
    qDebug() << "Sending  command" << command.command << command.arguments << command.data.size()
             << "to" << _identifier
             << "using" << (device == _deviceListen ? "listen socket" : "request socket");

    QString url = makeUrl(QString(), command.command, command.arguments);
    QByteArray data = QString("%1\r\n").arg(url).toUtf8();
    data.append(command.data);

    RequestAnswerer::send(device, data);

    emit deviceOver(device);
}

void HttpTunnelServer::sendAnswer(QIODevice *device, HttpStatusCode::Enum code)
{
    qDebug() << "Sending  HTTP status code" << code
             << "to" << _identifier
             << "using" << (device == _deviceListen ? "listen socket" : "request socket");

    RequestAnswerer::send(device, code);
    emit deviceOver(device);
}

QByteArray HttpTunnelServer::buildClientsListData()
{
    if(_listClients)
    {
        QJsonArray clientsArray;
        const QMap<QString, ClientParams> &list = _clientsManager->getClients();
        for(auto iterator = list.constBegin() ; iterator != list.constEnd() ; ++iterator)
        {
            if(iterator.value().controllable)
            {
                QJsonObject jsonClient;
                jsonClient["identifier"] = iterator.key();
                jsonClient["controlled"] = iterator.value().controlled;

                clientsArray.append(jsonClient);
            }
        }

        QJsonDocument jsonDoc;
        jsonDoc.setArray(clientsArray);

        return jsonDoc.toJson();
    }
    else
    {
        return QByteArray();
    }
}

void HttpTunnelServer::onClientsListChanged(bool controllable)
{
    if(controllable) // We don't care about non-controllable clients
    {
        sendCommand({HttpTunnelCommand::ClientsList, {}, buildClientsListData()});
    }
}

void HttpTunnelServer::onCommandForwardDataAvailable(const QByteArray &data)
{
    RequestReader *reader = qobject_cast<RequestReader *>(sender());
    if(reader)
    {
        if(_remote)
        {
            _remote->sendCommand({reader->getRequestDesc().command,
                                  reader->getRequestDesc().arguments,
                                  data});
        }
        else
        {
            qCritical() << "There is no remote client ?!";
        }

        sendAnswer(reader->accessDevice(), HttpStatusCode::Ok);
    }
    else
    {
        qCritical() << "Sender is not a RequestReader" << sender();
    }
}

bool HttpTunnelServer::takeControl(const QString &clientId, const QString &password)
{
    const QMap<QString, ClientParams> &clients = _clientsManager->getClients();
    auto iterator = clients.constFind(clientId);
    if(iterator != clients.constEnd())
    {
        HttpTunnelServer *clientTunnel = iterator.value().tunnel;
        if(iterator.value().controllable && password == clientTunnel->getPassword())
        {
            _remote = clientTunnel;
            _remote->_remote = this;

            clientTunnel->sendControlTaken();

            _clientsManager->setClientControlled(clientId, true);

            return true;
        }
    }
    else
    {
        qWarning() << "No such client" << clientId;
    }

    return false;
}



void HttpTunnelServer::disconnect()
{
    if(_remote)
    {
        _remote->sendCommand({HttpTunnelCommand::ControlReleased});

        _clientsManager->setClientControlled(_remote->getIdentifier(), false);

        _remote->_remote = nullptr;
    }

    emit disconnected();
}

void HttpTunnelServer::onListenReconnectTimeout()
{
    qInfo() << "Listen reconnection timeout for client" << _identifier;
    disconnect();
}
