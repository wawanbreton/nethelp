#pragma once

#include <QObject>

#include <QMap>

#include "server/clientparams.h"

class ClientsListManager : public QObject
{
    Q_OBJECT

    public:
        explicit ClientsListManager(QObject *parent = nullptr);

        void addClient(const QString &client, HttpTunnelServer *tunnel, bool controllable);

        void setClientControlled(const QString &clientId, bool controlled);

        void setClientData(const QString &clientId, const QByteArray &data);

        QByteArray getClientData(const QString &clientId) const;

        const QMap<QString, ClientParams> &getClients() const { return _clients; }

        QString getClientUniqueIdentifier(const QString &desiredIdentifier);

    signals:
        void listChanged(bool controllableClient);

    private:
        void onTunnelDisconnected();

    private:
        QMap<QString, ClientParams> _clients;
        int _identifiers{1};
};

