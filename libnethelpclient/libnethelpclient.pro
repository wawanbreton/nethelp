TARGET = nethelpclient
TEMPLATE = lib
CONFIG += staticlib

NETHELP_ROOT = ..
NETHELP_COMMON = $${NETHELP_ROOT}/common

INCLUDEPATH += $${NETHELP_ROOT}

NETHELP_PACKAGES = tcp camera
include($${NETHELP_ROOT}/libnethelpclient/libnethelpclient.pri)

include($${NETHELP_COMMON}/common.pri)
