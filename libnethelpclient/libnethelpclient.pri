INCLUDEPATH += $${NETHELP_ROOT}

NETHELP_CORE_PACKAGES = endpoint-client

contains(NETHELP_PACKAGES, camera) {
    NETHELP_CORE_PACKAGES *= camera
}

contains(NETHELP_PACKAGES, tcp) {
    NETHELP_CORE_PACKAGES *= tcp
}

NETHELP_COMMON = $${NETHELP_ROOT}/common

include($${NETHELP_ROOT}/core/core.pri)
